import { Position } from '.';

export interface PCB {
    fileVersion: string;
    layers: ReadonlyArray<Layer>;
    nets: ReadonlyArray<Net>;
    segments: ReadonlyArray<Segment>;
    modules: ReadonlyArray<Module>;
    vias: ReadonlyArray<Via>;
    shapes: ReadonlyArray<FPShape>;
}

export type LayerType = 'user' | 'mixed' | 'power' | 'signal';
export interface Layer {
    index: number;
    name: string;
    type: LayerType;
}

export interface Net {
    index: number;
    name: string;
}

export interface Segment {
    start: Position;
    end: Position;
    width: number;
    layerName: string;
    netNumber: number;
}

export interface Via {
    position: Position,
    size: number,
    drill: number,
    layers: string[],
    net: number
}

export interface Module {
    moduleName: string;
    layer: string;
    position: Position;
    shapes: ReadonlyArray<FPShape>;
    pads: ReadonlyArray<Pad>;
}

interface TextPrimitive {
    value: string;
    position: Position;
    rotation: number;
    layer: string;
    visible: boolean;
    fontSize: [ number, number ]
    thickness: number;
}

interface CirclePrimitive {
    center: Position;
    end: Position;
    layer: string;
    width: number;
}

interface RectPrimitive {
    start: Position;
    end: Position;
    layer: string;
    width: number;
}

interface LinePrimitive {
    start: Position;
    end: Position;
    layer: string;
    width: number;
}

interface ArcPrimitive {
    start: Position;
    end: Position;
    angle: number;
    layer: string;
    width: number;
}

interface PolyPrimitive {
    points: ReadonlyArray<Position>;
    layer: string;
    width: number;
}

export type ShapeType = 'text' | 'circle' | 'rect' | 'line' | 'arc' | 'poly';
export interface FPShape {
    shape: ShapeType
}
export type GRShape = FPShape;

export type FPText = { shape: 'text' } & TextPrimitive & { fieldName: string };
export type FPCircle = { shape: 'circle' } & CirclePrimitive;
export type FPLine = { shape: 'line' } & LinePrimitive;
export type FPArc = { shape: 'arc' } & ArcPrimitive;
export type FPPoly = { shape: 'poly' } & PolyPrimitive;
export type FPRect = { shape: 'rect' } & RectPrimitive;

export type GRText = { shape: 'text' } & TextPrimitive;
export type GRCircle = { shape: 'circle' } & CirclePrimitive;
export type GRLine = { shape: 'line' } & LinePrimitive;
export type GRArc = { shape: 'arc' } & ArcPrimitive;
export type GRPoly = { shape: 'poly' } & PolyPrimitive;
export type GRRect = { shape: 'rect' } & RectPrimitive;


export type PadType = 'thru_hole'|'smd'|'connect'|'np_thru_hole';
export type PadShape = 'circle'|'rectangle'|'roundrect'|'oval'|'trapezoid'|'custom';
export interface Pad {
    padNumber: string;
    padType: PadType;
    padShape: PadShape;
    position: Position;
    size: { x: number, y: number };
    drill?: { x: number, y: number };
    layers: string[];
    netIndex?: number;
}
