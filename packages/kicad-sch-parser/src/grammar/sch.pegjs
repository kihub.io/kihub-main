// EESchema schematic Grammer
//
// Based on the official spec at: http://bazaar.launchpad.net/~stambaughw/kicad/doc-read-only/download/head:/1115%4016bec504-3128-0410-b3e8-8e38c2123bca:trunk%252Fkicad-doc%252Fdoc%252Fhelp%252Ffile_formats%252Ffile_formats.pdf/file_formats.pdf
// Additional reference: eeschema/sch_legacy_plugin.cpp from KiCAD source tree

{
  import { Schematic, DescriptionBlock, LibraryItem, DescriptionBlockInner,
    SchematicItem, ComponentItem, ItemType, Field, HorizontalAlignment, VerticalAlignment,
    FieldOrientation, SheetLabelType, TextItem, TextOrientation, TextItemType,
    JunctionItem, NoConnectionItem, ComponentTransform, LineItem, LineStyle, RGBColor,
    LineType, BitmapItem, Position, EntryItem, SheetItem, SheetPin,
    SheetPinSide, Size, PageInfo } from '../types';

  import { parseLibraryItem, parseRefDes, calcPageSize, replaceNewline } from '../helpers';

  let formatVersion: number = 0;

}


// Schematic
Schematic =
  formatVersion:SchematicHeader libs:Libs EELayer descr:DescrBlock? items:SchematicItem* EndSchematic
  <Schematic>{
    return { formatVersion, libs, descr: descr, items }
  }

SchematicHeader =
  "EESchema Schematic File Version " version:[1-9] NEWLINE
  <number>{
    // Set the format version globally, because we will need
    // that information later.
    formatVersion = parseInt(version);

    return formatVersion;
  }

// LIBS list
Libs = LibEntry*

LibEntry =
  "LIBS:" libName:[^\r\n]+ NEWLINE
  <string>{ return libName.join(''); }

// EELayer declaration is parsed, but no inforamation is extracted
EELayer = EELayerStart EELayerEnd

EELayerStart = "EELAYER" _ [0-9]+ _ [0-9]+ NEWLINE

EELayerEnd = "EELAYER END" NEWLINE

// Description Block
DescrBlock = pageInfo:DescrStart descrItems:(TextDescrAttr / SheetDescrAttr / EncodingDescrAttr)* DescrEnd
  <DescriptionBlock>{

    const descr: DescriptionBlock = { pageInfo };

    // Merge all description elements into one block
    for (const item of descrItems) {
      Object.assign(descr, item);
    }

    return descr;
  }

DescrStart = "$Descr" _ pageSize:UnquotedString _ pageWidth:Positive _ pageHeight:Positive portraitFlag:(_ "portrait")? NEWLINE
  <PageInfo>{
      return calcPageSize(pageSize, pageWidth, pageHeight, portraitFlag !== null); 
  }

DescrEnd = "$EndDescr" NEWLINE


TextDescrAttr = attrName:("Title" / "Date" / "Rev" / "Comp" / "Comment1" / "Comment2" / "Comment3" / "Comment4") _ value:QuotedString NEWLINE
  <DescriptionBlockInner>{
    let key: string;

    switch(attrName) {
    case 'Title': return { title: value };
    case 'Date': return { date: value };
    case 'Rev': return { rev: value };
    case 'Comp': return { company: value };
    case 'Comment1': return { comment1: value };
    case 'Comment2': return { comment2: value };
    case 'Comment3': return { comment3: value };
    case 'Comment4': return { comment4: value };
    }
  }

SheetDescrAttr = "Sheet" _ sheetNumber:Positive _ totalSheets:Positive NEWLINE
  <DescriptionBlockInner>{
    return {
      sheetNumber, totalSheets
    }
  }

EncodingDescrAttr = "encoding" _ encoding:UnquotedString NEWLINE
  <DescriptionBlockInner>{
    return { encoding };
  }

SchematicItem = item:(ComponentItem / TextItem / ConnectionItem / LineItem / BitmapItem / EntryItem / SheetItem)
  <SchematicItem>{
    return item;
  }

//////////////////////////////////////////////////////////////////
// Component Item
//////////////////////////////////////////////////////////////////

ComponentItem = CompStart compL:CompL compU:CompU compP:CompP hierReference:CompAR* fields:CompF+ 
  CompTransformLine1 transform:CompTransformLine2 CompEnd
  <ComponentItem>{
    const [ libraryItem, refDes ] = compL;
    const { unit, convert, timestamp } = compU;

    const r:ComponentItem = {
      type: ItemType.Component,
      libraryItem, refDes,
      unit, convert, timestamp,
      position: compP,
      fields, transform,
      hierReference
    };

    // Check that all mandatory fields are defined
    const m = fields.filter(f => f.index < 4);
    if (m.length !== 4)
      error("Not all mandatory fields are defined");

    if (fields.length > 99)
      error("Too many fields");

    return r;
  }

CompStart = "$Comp" NEWLINE

CompL = "L" _ libNameRaw:UnquotedString _ refDes:UnquotedString NEWLINE
  <[LibraryItem, string]>{
    return [
      parseLibraryItem(libNameRaw, formatVersion),
      parseRefDes(refDes)
    ];
  }

CompU = "U" _ unit:Positive _ convert:Positive _ timestamp:HexString NEWLINE
  {
    return { unit, convert, timestamp };
  }

CompAR = "AR" _ "Path=" rpath:QuotedString _ "Ref=" ref:QuotedString _ "Part=\"" part:Positive "\"" NEWLINE
  {
    return { rpath, ref, part };
  }

CompP = "P" _ x:Integer _ y:Integer NEWLINE
  {
    return { x, y }
  }

CompF = "F" _ index:Positive _ value:QuotedString _ orientation:[HV] _ x:Integer _ y:Integer _ size:Positive _ attr:HexString
        textAttr:(_ @HorizontalAlignment _ @VerticalAlignment @(Italic Bold)?)? nameRaw:(_ QuotedString)? NEWLINE
  <Field>{
    const [ halign, valign, extra ] = textAttr || [HorizontalAlignment.Center, VerticalAlignment.Center, null];
    const [ italic, bold ] = extra || [ false, false ];

    const r:Field = {
      index, value, 
      position: { x, y },
      size,
      orientation: orientation === 'H'? FieldOrientation.Horizontal : FieldOrientation.Vertical,
      visible: parseInt(attr, 16) > 0,
      halign, valign, italic, bold
    };

    // Source: template_fieldnames.cpp: TEMPLATE_FIELDNAME::GetDefaultFieldName
    if (index > 3) {
      // Non-mandatory field - If a name is specified use it. Otherwise
      // default to "FieldNN" (NN - index of the field)
      r.name = nameRaw? nameRaw[1] : `Field${index}`;
    }

    return r;
  }

// The data in this line is redudant, it is parsed but not used
// Note: The first number should always be "1", but in some files it
//       is not the case.
CompTransformLine1 = _ [0-9]+ _ Integer _ Integer NEWLINE

CompTransformLine2 = _ x1:Integer _ y1:Integer _ x2:Integer _ y2:Integer NEWLINE
  <ComponentTransform>{

    // Each transform component must be an integer between -1 .. 1
    const checkRange = (z) => {
      if ((z < -1) || (z > 1))
        error("Invalid transform value");
    }
    checkRange(x1);
    checkRange(y1);
    checkRange(x2);
    checkRange(y2);

    return { x1, y1, x2, y2 };
  }

HorizontalAlignment = v:[LRC]
  <HorizontalAlignment>{
    if (v === 'L')
      return HorizontalAlignment.Left;
    else if (v === 'R')
      return HorizontalAlignment.Right;
    else
      return HorizontalAlignment.Center;
  }

VerticalAlignment = v:[TBC]
  <VerticalAlignment>{
    if (v === 'T')
      return VerticalAlignment.Top;
    else if (v === 'B')
      return VerticalAlignment.Bottom;
    else
      return VerticalAlignment.Center;
  }

Italic = v:[IN]
  <boolean>{
    return (v === 'I');
  }

Bold = v:[BN]
  <boolean>{
    return (v === 'B');
  }

FieldTextAttr = horizJustify:[LRC]

CompEnd = "$EndComp" NEWLINE

//////////////////////////////////////////////////////////////////
// Text Item
//////////////////////////////////////////////////////////////////

TextItem = "Text" _ textType:TextItemType _ x:Integer _ y:Integer _ orientation:TextOrientation _ size:Positive
  shape:(_ SheetLabelType)? italic:(_ ("~" / "Italic"))? thickness:(_ Positive)? NEWLINE
  text:LineString
  <TextItem>{

    const r: TextItem = {
      type: ItemType.Text,
      textType,
      text: replaceNewline(text),
      position: { x, y },
      orientation,
      size,
      thickness: (thickness? thickness[1] : 0),
      italic: (italic? (italic[1] === 'Italic') : false)
    };

    if (textType === TextItemType.HierLabel || textType === TextItemType.GlobalLabel) {
      // For these text types, a shape must be specified.
      if (!shape)
        error('Shape must be specified for this type of text');

      r.shape = shape[1];
    }
    else {
      // Shape must NOT be specified
      if (shape)
        error('Shape must not be specified for this type of text');
    }

    return r;
  }

TextItemType = t:("Notes" / "Label" / "HLabel" / "GLabel" )
  <TextItemType>{
    switch(t) {
      case 'Notes': return TextItemType.Notes;
      case 'Label': return TextItemType.Label;
      case 'HLabel': return TextItemType.HierLabel;
      default: 
        if (formatVersion === 1)
          // Prior to version 2, the GlobalLabel object did not exist
          return TextItemType.HierLabel;
        else
          return TextItemType.GlobalLabel;
    }
  }

TextOrientation = v:[0123]
  <TextOrientation>{
    switch(v) {
      case '0': return TextOrientation.Horizontal;
      case '1': return TextOrientation.VerticalUp;
      case '2': return TextOrientation.HorizontalInvert;
      default: return TextOrientation.VerticalDown;
    }
  }

SheetLabelType = v:("Input" / "Output" / "BiDi" / "3State" / "UnSpc" / "???")
  <SheetLabelType>{
    switch(v) {
      case 'Input': return SheetLabelType.Input;
      case 'Output': return SheetLabelType.Output;
      case 'BiDi': return SheetLabelType.BiDi;
      case '3State': return SheetLabelType.ThreeState;
      case 'UnSpc': return SheetLabelType.Unspecified;
      default: return SheetLabelType.Unknown;
    }
  }

//////////////////////////////////////////////////////////////////
// Connection Item
//////////////////////////////////////////////////////////////////

ConnectionItem = type:("NoConn" / "Connection") _ UnquotedString _ x:Integer _ y:Integer NEWLINE
  <JunctionItem|NoConnectionItem>{
    return {
      type: type === 'Connection'? ItemType.Junction : ItemType.NoConnect,
      position: { x, y }
    }
  }

//////////////////////////////////////////////////////////////////
// Entry Item
//////////////////////////////////////////////////////////////////

EntryItem = "Entry" _ entryType:(("Wire" _ "Line") / ("Bus" _ "Bus")) NEWLINE
  _ xStart:Integer _ yStart:Integer _ xEnd:Integer _ yEnd:Integer NEWLINE
  <EntryItem>{

    const xSize = xEnd - xStart;
    const ySize = yEnd - yStart;

    return {
      type: ItemType.BusEntry,
      busEntry: entryType[0] === "Bus",
      position: { x:xStart, y:yStart },
      size: { x:xSize, y:ySize }
    };
  }

//////////////////////////////////////////////////////////////////
// Line Item
//////////////////////////////////////////////////////////////////

LineItem = "Wire" _ lineType:LineType _ "Line" width:(_ LineWidth)? style:(_ LineStyle)? color:(_ LineColor)?  NEWLINE
  _ xstart:Integer _ ystart:Integer _ xend:Integer _ yend:Integer NEWLINE
  <LineItem>{
    const r: LineItem = {
      type: ItemType.Line,
      lineType,
      start: { x:xstart, y:ystart },
      end: { x:xend, y:yend }
    }

    if (width)  r.width = width[1];
    if (style)  r.style = style[1];
    if (color)  r.color = color[1];

    return r;
  }

LineType = typeName:("Wire" / "Bus" / "Notes")
  <LineType>{

    switch(typeName) {
      case 'Wire': return LineType.Wire;
      case 'Bus': return LineType.Bus;
      default: return LineType.Notes;
    }

  }

LineWidth = "width" _ value:Positive
  <number>{
    return value;
  }

LineColor = color:(LineColorRGB / LineColorRGBA)
  <RGBColor>{
    // Validate the color ranges
    const checkRange = v => {
      if (v > 255)
        error("Invalid color value");
    }
    checkRange(color.r);
    checkRange(color.g);
    checkRange(color.b);
    if (color.alpha)
      checkRange(color.alpha);

    return color;
  }

LineColorRGB = "rgb(" _? r:Positive "," _? g:Positive _? "," _? b:Positive _? ")"
  <RGBColor>{
    return  { r, g, b };
  }

LineColorRGBA = "rgba(" _? r:Positive "," _? g:Positive _? "," _? b:Positive _? "," _? alpha:Positive ")"
  <RGBColor>{
    return { r, g, b, alpha };
  }

LineStyle = "style" _ styleName:[a-z_]+
  <LineStyle>{
    switch(styleName.join('')) {
      case 'solid': return LineStyle.Solid;
      case 'dashed': return LineStyle.Dashed;
      case 'dotted': return LineStyle.Dotted;
      case 'dash_dot': return LineStyle.DashDot;
      default: error('Invalid line style');
    }
  }

//////////////////////////////////////////////////////////////////
// Bitmap Item
//////////////////////////////////////////////////////////////////

BitmapItem = BitmapStart position:BitmapPosition scale:BitmapScale data:BitmapData BitmapEnd
  <BitmapItem>{
    return {
      type: ItemType.Bitmap,
      position, scale, data
    }
  }

BitmapStart = "$Bitmap" NEWLINE

BitmapPosition = "Pos" _ x:Integer _ y:Integer NEWLINE
  <Position>{ return { x, y }; }

BitmapScale = "Scale" _ scale:PositiveFloat NEWLINE
  <number>{ return scale; }

BitmapData = "Data" NEWLINE dataLines:BitmapDataLine+ "EndData" NEWLINE
  <string>{
    return Buffer.concat(dataLines).toString('base64');
  }

BitmapDataLine = dataByte:(@digit1:[0-9A-F] @digit2:[0-9A-F] _?)+ NEWLINE
  <Buffer>{
    const line = Buffer.alloc(dataByte.length);
    dataByte.forEach(([digit1, digit2], index) => line[index] = parseInt(digit1 + digit2, 16));

    return line;
  }

BitmapEnd = "$EndBitmap" NEWLINE

//////////////////////////////////////////////////////////////////
// Sheet Item
//////////////////////////////////////////////////////////////////

SheetItem = SheetStart posSize:SheetS timestamp:SheetU sheetFields:SheetFields SheetEnd
  <SheetItem>{

    const [ position, size ] = posSize;
    const [ name, nameSize, filename, filenameSize, pins ] = sheetFields;

    return {
      type: ItemType.Sheet,
      position, size,
      name, nameSize, filename, filenameSize,
      pins, timestamp
    };
  }

SheetStart = "$Sheet" NEWLINE

SheetS = "S" _ x:Integer _ y:Integer _ xSize:Integer _ ySize:Integer NEWLINE
  <[Position, Size]>{
    return [
      { x, y },
      { x: xSize, y: ySize }
    ];
  }

SheetU = "U" _ ts:HexString NEWLINE
  <string>{
    if (ts.length > 8)
      error('Timestamp string too long');

    //return parseInt(ts, 16);
    return ts;
  }

SheetFields = name:SheetTextField filename:SheetTextField pins:SheetPinField*
  <[string, number, string, number, SheetPin[]]>{
    return [ name[0], name[1], filename[0], filename[1], pins ];
  }

SheetTextField = "F" [01] _ text:QuotedString _ size:Positive NEWLINE
  <[string, number]>{
    return [ text, size ];
  }

SheetPinField = "F" number:Positive _ name:QuotedString _ shape:SheetPinShape _ side:SheetPinSide _ 
  x:Integer _ y:Integer _ size:Positive NEWLINE
  <SheetPin>{

    if (number === 0 || number === 1)
      error("Invalid sheet pin number");

    return { 
      number, name, shape, side, size,
      position: { x, y }
    };
  }

SheetPinShape = v:[IOBTU]
  <SheetLabelType>{
    switch(v) {
      case 'I': return SheetLabelType.Input;
      case 'O': return SheetLabelType.Output;
      case 'B': return SheetLabelType.BiDi;
      case 'T': return SheetLabelType.ThreeState;
      default: return SheetLabelType.Unspecified;
    }
  }

SheetPinSide = v:[RTBL]
  <SheetPinSide>{
    switch(v) {
      case 'R': return SheetPinSide.Right;
      case 'L': return SheetPinSide.Left;
      case 'T': return SheetPinSide.Top;
      default: return SheetPinSide.Bottom;
    };
  }

SheetEnd = "$EndSheet" NEWLINE

//////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////

EndSchematic = "$EndSCHEMATC" NEWLINE?

// Common
Name =
    [^ \t\n\r]+ <string>{ return text() }

Integer =
     "-"? [0-9]+ <number>{ return parseInt(text(), 10); }
  
Positive =
     [0-9]+ <number>{ return parseInt(text(), 10); }

PositiveFloat =
  intPart:[0-9]+ fracPart:([.,] [0-9]+)? exponent:([Ee] [+-]? [0-9]+)?
  <number>{
    let num = intPart.join('');

    if (fracPart)
      num += "." + fracPart[1];

    if (exponent) {
      num += "e";

      if (exponent[1]==='-')
        num += '-';

      num += exponent[2].join('');
    }

    return parseFloat(num);
  }
  
YN =
    [YN] <boolean>{ return text()==='Y' }

    
//QuotedString =
//    '"' inner:[^"]* '"' <string>{ return inner.join('') }
QuotedString = '"' inner:('\\"' / [^"])* '"' <string>{ return inner.join('') }

UnquotedString =
    head:[^"] tail:[^ \t\n\v\f\r]* <string>{ return head + tail.join(''); }

AlphaNumericString =
    chars:[^ \t]+ <string>{ return chars.join(''); }

EscapedString =
    UnquotedString
    <string>{
      return text().split('~').join(' ');
    }

QuotedOrEscapedString =
    QuotedString / EscapedString

LineString = text:[^\n\r]* STRICT_NEWLINE
  <string>{ return text.join(''); }

HexString = value:[0-9A-Z]+
   <string>{ return value.join(''); }

PinName =
    [^ \t]+ <string>{ return text(); }
  
PinNumber =
    AlphaNumericString
    <string|null>{
      if (text() === "~")
        return null
      else
        return text()
    }

_ "whitespace"
  = [ \t]+
  
NEWLINE = _? STRICT_NEWLINE

STRICT_NEWLINE = [\n\r]+

  


