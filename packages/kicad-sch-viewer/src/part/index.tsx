import * as React from 'react';
import { DrawPrimitiveType, DrawCircle, FillType,
    DrawRect, DrawPolyline, DrawText,  
    DrawPin, 
    DrawArc, 
    Convert, 
    ComponentTransform,
    Field,
    Part,
    FieldOrientation} from '@kihub/kicad-sch-parser';

import { SchematicTheme } from '../theme';
import { xor, applyTransform } from './utils';
import { Text } from './text';
import { Pin } from './pin';

export interface PartDrawProps {
    part: Part;
    convert: boolean;
    subPart: number;
    theme: SchematicTheme;
    transform?: ComponentTransform;
    fields?: Field[]
}

/**
 * Determine if arc angles should be swapped.
 * 
 * This function is a rewrite of the TRANSFORM::mapAngles method,
 * with TRANSFORM created using default constructor. Originally, it
 * mirrors two given angles about X axis and returns true whether
 * the angles should be swapped. In this implementation, only the
 * swap value is required.
 * This is cumbersome and probably over-complicated.
 * 
 * @param angle1 
 * @param angle2 
 */
function mapAngles(angle1: number, angle2: number) {

    const DECIDEG2RAD = (n: number) => (n / 1800) * Math.PI;
    const RAD2DECIDEG = (n: number) => (n / Math.PI) * 1800;
    const NORMALIZE_ANGLE_POS = (n: number) => {
        let angle = n;
        while( angle < 0 )
            angle += 3600;
        while( angle >= 3600 )
            angle -= 3600;
        return angle;
    }
    const flipAngle = (a: number) => {
        const x = Math.cos( DECIDEG2RAD( a ) );
        const y = Math.sin( DECIDEG2RAD( a ) );
        return RAD2DECIDEG( Math.atan2( -y, x ) );
    }

    let newAngle1 = NORMALIZE_ANGLE_POS(flipAngle(angle1));
    let newAngle2 = NORMALIZE_ANGLE_POS(flipAngle(angle2));

    if( newAngle2 < newAngle1 )
        newAngle2 += 3600;

    return (newAngle2 - newAngle1) > 1800;
}

export function PartDraw(props: PartDrawProps): JSX.Element {
    const { theme, subPart, convert, fields } = props;
    const { draw, showPinName, showPinNumber, offset, fields: partFields } = props.part;

    const fillSvg = (fillType: FillType) => 
        fillType === FillType.Background? theme.background :
        fillType === FillType.Foreground? theme.foreground :
        'none';

    const transform = props.transform || { x1: 1, y1: 0, x2: 0, y2: -1 };

    // Filter only the primitives in the current subpart & convert combinations
    const activeDraw = draw.details
        .filter(p => 
            (p.part === 0 || p.part === subPart) && 
            (p.convert === Convert.All || (convert && p.convert === Convert.Convert)  || (!convert && p.convert === Convert.Regular)));

    const drawElements = activeDraw.filter(p => p.type !== DrawPrimitiveType.Pin).map((primitive, index) => {
        const key = `draw${index}`;
        switch(primitive.type) {

        case DrawPrimitiveType.Circle:
            const { center: circleCenter, radius: circleRadius, 
                pen: circlePen, fill: circleFill } = primitive as DrawCircle;
            return <circle 
                key={key}
                cx={circleCenter.x} 
                cy={-circleCenter.y} 
                r={circleRadius}
                strokeWidth={circlePen || theme.defaultThickness}
                stroke={theme.foreground}
                fill={fillSvg(circleFill)}
                />

        case DrawPrimitiveType.Arc:
            const { radius: arcRadius, startAngle, endAngle,
                fill: arcFill, pen: arcPen, start, end } = (primitive as DrawArc);
           
            const swapFlag = mapAngles(startAngle, endAngle)? '0' : '1';

            const bigArcFlag = '0';            

            return <path key={key}
                d={`M${start.x},${-start.y} A${arcRadius},${arcRadius},0,${bigArcFlag},${swapFlag},${end.x},${-end.y}`}
                stroke={theme.foreground} 
                strokeWidth={arcPen || theme.defaultThickness}
                fill={fillSvg(arcFill)}/>

        case DrawPrimitiveType.Polyline:
            const { vertices, pen: polylinePen, fill: polylineFill } = primitive as DrawPolyline;
            return <polyline
                key={key}
                points={vertices.map(({x, y}) => `${x},${-y}`).join(' ')}
                strokeWidth={polylinePen || theme.defaultThickness}
                stroke={theme.foreground}
                fill={fillSvg(polylineFill)}
                />


        case DrawPrimitiveType.Rect:
            const rect = primitive as DrawRect;
            const { topLeft, bottomRight } = primitive as DrawRect;
            
            if ((topLeft.x === bottomRight.x) || (topLeft.y === bottomRight.y)) {
                // If the rect reduces to a line, use the <line> primitive, otherwise
                // it becomes invisible.
                return <line
                    key={key}
                    x1={topLeft.x}
                    y1={-topLeft.y}
                    x2={bottomRight.x}
                    y2={-bottomRight.y}
                    strokeWidth={10/*rect.pen || theme.defaultThickness*/}
                    stroke={theme.foreground}
                    strokeLinecap='square'
                />
            }
            else {
                const x1 = Math.min(topLeft.x, bottomRight.x);
                const y1 = Math.max(topLeft.y, bottomRight.y);
                const width = Math.abs(rect.bottomRight.x - rect.topLeft.x);
                const height = Math.abs(rect.bottomRight.y - rect.topLeft.y);
                return <rect
                    key={key} 
                    x={x1} 
                    y={-y1}
                    width={width}
                    height={height}
                    strokeWidth={rect.pen || theme.defaultThickness}
                    stroke={theme.foreground}
                    fill={fillSvg(rect.fill)}
                    strokeLinecap='square'
                    />
            }

        case DrawPrimitiveType.Text:
            return <Text key={key} fontName={theme.fontName} color={theme.notes} {...(primitive as DrawText)}/>

        case DrawPrimitiveType.Pin:
            return <Pin pinProps={primitive as DrawPin} partProps={{ showName: showPinName, showNumber: showPinNumber, textOffset: offset}} 
                transform={transform} theme={theme}/>
        
        default:
            return <></>
        }
    });

    const pins = activeDraw.filter(p => p.type === DrawPrimitiveType.Pin).map((primitive, index) => {
        const key = `pin${index}`;
        return <Pin key={key} pinProps={primitive as DrawPin} partProps={{  showName: showPinName, showNumber: showPinNumber, textOffset: offset }} 
            transform={transform} theme={theme}/>;
    });

    const svgTransform = `matrix(${transform.x1} ${transform.x2} ${-transform.y1} ${-transform.y2} 0 0)`;
    const effectiveFields = fields || partFields;

    const drawField = (f: Field|undefined|null) => 
        (f && f.visible)?
        <Text
            text={f.value}
            position={applyTransform(f.position, transform)}
            size={f.size}
            halign={f.halign}
            valign={f.valign}
            bold={f.bold}
            italic={f.italic}
            angle={xor(f.orientation == FieldOrientation.Horizontal, transform.y1 !== 0)? 0 : -900}
            color={
                f.index === 0? theme.partReference :
                f.index === 1? theme.partValue :
                theme.fields
            }
            fontName={theme.fontName}
        /> : null;
    return <g>
        <g transform={svgTransform}>
            {drawElements}
        </g>
        {effectiveFields && effectiveFields.map(f => f && drawField(f))}
        {pins}
    </g>;
}
