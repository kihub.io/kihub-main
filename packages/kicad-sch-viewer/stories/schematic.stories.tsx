import * as React from 'react';

import { storiesOf } from '@storybook/react';
import KiCADTheme from '../src/kicad-theme';

import { SchematicDocument, SchematicItem, ItemType, LineType, LineStyle, TextItemType, 
    TextOrientation, SheetLabelType, SheetPinSide, ComponentItem, PartDatabase, Part, Position } from '@kihub/kicad-sch-parser';
import { SchematicViewCore, SchematicView } from '../src/schematic';

import picProgrammerDesign from './resources/pic_programmer.json';
import complexHierarchyDesign from './resources/complex_hierarchy.json';
import singleComponentDesign from './resources/single.json';
import testPart from './resources/test_part.json';

console.log(singleComponentDesign)
const VIEW_HEIGHT = 600;
const VIEW_WIDTH = 1000;
const schematic = <T extends SchematicItem>(items: T[]): SchematicDocument => ({
    schematics: {
        root: {
            items,
            formatVersion: 2,
            libs: [],
            descr: {
                pageInfo: {
                    pageSize: 'A4',
                    width: 0,
                    height: 0,
                    portrait: false   
                }
            }
        }
    },
    root: 'root',
    parts: {}
});

const originCross = (lenFact: number = 100, widthFact: number = 5) => <>
    <line x1={-50*600/lenFact} y1={0} x2={50*600/lenFact} y2={0} style={{ strokeWidth: 1*widthFact, stroke: 'black' }}/>
    <line x1={0} y1={-50*600/lenFact} x2={0} y2={50*600/lenFact} style={{ strokeWidth: 1*widthFact, stroke: 'black' }}/>
</>

const StdSchematicViewer = (props: { schDoc: SchematicDocument, parts?: PartDatabase, zoom?: number, center?: Position  }) =>
    <div style={{border: '2px solid black', float: 'left'}}>
        <SchematicViewCore width={VIEW_WIDTH} height={VIEW_HEIGHT} extraDraw={originCross(props.zoom, props.zoom)} gridSpacing={100} zoom={props.zoom || 2.5} 
            theme={KiCADTheme} center={props.center || { x: 0, y: 0}}
            schDoc={{
                root: 'root',
                schematics: { root: props.schDoc.schematics['root'] },
                parts: props.parts || props.schDoc.parts
            }}
        />
    </div>;

///////////////////////////////////
// Junction
///////////////////////////////////
storiesOf('Items', module)
    .add('Junction', () => <StdSchematicViewer schDoc={schematic([
        {
            type: ItemType.Junction,
            position: { x: 100, y: -200 }
        },
    ])}/>);

///////////////////////////////////
// Line
///////////////////////////////////
storiesOf('Items/Line', module)
    .add('Wire (Default)', () => <StdSchematicViewer schDoc={schematic([
        {
            type: ItemType.Line,
            lineType: LineType.Wire,
            start: { x: -100, y: 200 },
            end: { x: 200, y: 200 }
        }
    ])}/>)
    .add('Bus (Default)', () => <StdSchematicViewer schDoc={schematic([
        {
            type: ItemType.Line,
            lineType: LineType.Bus,
            start: { x: -100, y: 200 },
            end: { x: 200, y: 200 }
        }
    ])}/>)
    .add('Notes (Default)', () => <StdSchematicViewer schDoc={schematic([
        {
            type: ItemType.Line,
            lineType: LineType.Notes,
            start: { x: -100, y: 200 },
            end: { x: 200, y: 200 }
        }
    ])}/>)
    .add('Line Width', () => <StdSchematicViewer schDoc={schematic([
        {
            type: ItemType.Line,
            lineType: LineType.Wire,
            start: { x: 100, y: 0 },
            end: { x: 100, y: 200 },
            width: 4
        },
        {
            type: ItemType.Line,
            lineType: LineType.Wire,
            start: { x: 200, y: 0 },
            end: { x: 200, y: 200 },
            width: 8
        },
        {
            type: ItemType.Line,
            lineType: LineType.Wire,
            start: { x: 300, y: 0 },
            end: { x: 300, y: 200 },
            width: 12
        },
        {
            type: ItemType.Line,
            lineType: LineType.Wire,
            start: { x: 400, y: 0 },
            end: { x: 400, y: 200 },
            width: 16
        }
    ])}/>)
    .add('Line Styles', () => <StdSchematicViewer schDoc={schematic([
        {
            type: ItemType.Line,
            lineType: LineType.Wire,
            start: { x: 100, y: 0 },
            end: { x: 100, y: 200 },
            style: LineStyle.Solid
        },
        {
            type: ItemType.Line,
            lineType: LineType.Wire,
            start: { x: 200, y: 0 },
            end: { x: 200, y: 200 },
            style: LineStyle.Dashed
        },
        {
            type: ItemType.Line,
            lineType: LineType.Wire,
            start: { x: 300, y: 0 },
            end: { x: 300, y: 200 },
            style: LineStyle.Dotted
        },
        {
            type: ItemType.Line,
            lineType: LineType.Wire,
            start: { x: 400, y: 0 },
            end: { x: 400, y: 200 },
            style: LineStyle.DashDot
        }
    ])}/>)
    .add('Line Colors', () => <StdSchematicViewer schDoc={schematic([
        {
            type: ItemType.Line,
            lineType: LineType.Wire,
            start: { x: 100, y: 0 },
            end: { x: 100, y: 200 },
            color: { r: 127, g: 0, b: 0 }
        },
        {
            type: ItemType.Line,
            lineType: LineType.Wire,
            start: { x: 200, y: 0 },
            end: { x: 200, y: 200 },
            color: { r: 0, g: 127, b: 0 }
        },
        {
            type: ItemType.Line,
            lineType: LineType.Wire,
            start: { x: 300, y: 0 },
            end: { x: 300, y: 200 },
            color: { r: 0, g: 0, b: 127 }
        },
    ])}/>)

///////////////////////////////////
// Entry
///////////////////////////////////
storiesOf('Items/Entry', module)
    .add('Bus', () => <StdSchematicViewer schDoc={schematic([
        {
            type: ItemType.BusEntry,
            busEntry: true,
            position: { x: 100, y: 100 },
            size: { x: 100, y: 100 }
        }
    ])}/>)
    .add('Wire', () => <StdSchematicViewer schDoc={schematic([
        {
            type: ItemType.BusEntry,
            busEntry: false,
            position: { x: 100, y: 100 },
            size: { x: 100, y: 100 }
        }
    ])} />)
    
///////////////////////////////////
// No Connection
///////////////////////////////////
storiesOf('Items', module)
    .add('No Connection', () => <StdSchematicViewer schDoc={schematic([
        {
            type: ItemType.NoConnect,
            position: { x: 100, y: -100 }
        },
    ])}/>)

///////////////////////////////////
// Text
///////////////////////////////////
const textItem = {
    type: ItemType.Text,
    textType: TextItemType.Notes,
    position: { x: 100, y: -100 },
    orientation: TextOrientation.Horizontal,
    size: 50,
    thickness: 0,
    italic: false
}

storiesOf('Items/Text', module)
    .add('Simple', () => <StdSchematicViewer schDoc={schematic([
        {
            ...textItem,
            text: 'This is a text'
        },
    ])} />)
    .add('Multiline', () => <StdSchematicViewer schDoc={schematic([
        {
            ...textItem,
            text: 'First Line\nSecond Line\nThird Line'
        },
    ])} />)

///////////////////////////////////
// Global Label
///////////////////////////////////

const globalLabelTypes = [
    SheetLabelType.Input,
    SheetLabelType.Output,
    SheetLabelType.BiDi,
    SheetLabelType.ThreeState,
    SheetLabelType.Unknown,
    SheetLabelType.Unspecified
];

storiesOf('Items/Global Label', module)
    .add('Left', () => <StdSchematicViewer schDoc={schematic(
        globalLabelTypes.map((shape, labelIndex) => ({
            ...textItem,
            textType: TextItemType.GlobalLabel,
            shape,
            position: { x: 100, y: -200 + labelIndex * 100 },
            text: shape
        }))
    )} />)
    .add('Right', () => <StdSchematicViewer schDoc={schematic(
        globalLabelTypes.map((shape, labelIndex) => ({
            ...textItem,
            textType: TextItemType.GlobalLabel,
            shape,
            position: { x: 100, y: -200 + labelIndex * 100 },
            text: shape,
            orientation: TextOrientation.HorizontalInvert
        }))
    )} />)
    .add('Up', () => <StdSchematicViewer schDoc={schematic(
        globalLabelTypes.map((shape, labelIndex) => ({
            ...textItem,
            textType: TextItemType.GlobalLabel,
            shape,
            position: { x: -200 + labelIndex * 100, y: -100 },
            text: shape,
            orientation: TextOrientation.VerticalUp
        }))
    )} />)
    .add('Down', () => <StdSchematicViewer schDoc={schematic(
        globalLabelTypes.map((shape, labelIndex) => ({
            ...textItem,
            textType: TextItemType.GlobalLabel,
            shape,
            position: { x: -200 + labelIndex * 100, y: -100 },
            text: shape,
            orientation: TextOrientation.VerticalDown
        }))
    )} />)


///////////////////////////////////
// Hierarchical Label
///////////////////////////////////
const hierLabelItem = {
    ...textItem,
    textType: TextItemType.HierLabel,
    text: 'HLABEL'
}

storiesOf('Items/Hierarchical Label', module)
    .add('Left', () => <StdSchematicViewer schDoc={schematic(
        globalLabelTypes.map((shape, labelIndex) => ({
            ...hierLabelItem,
            shape,
            position: { x: 100, y: -200 + labelIndex * 100 },
            text: shape
        }))
    )} />)
    .add('Right', () => <StdSchematicViewer schDoc={schematic(
        globalLabelTypes.map((shape, labelIndex) => ({
            ...hierLabelItem,
            shape,
            position: { x: 100, y: -200 + labelIndex * 100 },
            text: shape,
            orientation: TextOrientation.HorizontalInvert
        }))
    )} />)
    .add('Up', () => <StdSchematicViewer schDoc={schematic(
        globalLabelTypes.map((shape, labelIndex) => ({
            ...hierLabelItem,
            shape,
            position: { x: -200 + labelIndex * 100, y: -100 },
            text: shape,
            orientation: TextOrientation.VerticalUp
        }))
    )} />)
    .add('Down', () => <StdSchematicViewer schDoc={schematic(
        globalLabelTypes.map((shape, labelIndex) => ({
            ...hierLabelItem,
            shape,
            position: { x: -200 + labelIndex * 100, y: -100 },
            text: shape,
            orientation: TextOrientation.VerticalDown
        }))
    )} />)
    .add('Styles', () => <StdSchematicViewer schDoc={schematic(
        [{
            ...hierLabelItem,
            shape: SheetLabelType.Input,
            position: { x: -200, y: -300 },
            text: SheetLabelType.Input,
            orientation: TextOrientation.Horizontal,
            italic: true
        },
        {
            ...hierLabelItem,
            shape: SheetLabelType.Input,
            position: { x: -200, y: -200 },
            text: SheetLabelType.Input,
            orientation: TextOrientation.Horizontal,
            thickness: 100
        },
        {
            ...hierLabelItem,
            shape: SheetLabelType.Input,
            position: { x: -200, y: -100 },
            text: SheetLabelType.Input,
            orientation: TextOrientation.Horizontal,
            italic: true,
            thickness: 100
        }]
    )} />)
    .add('Sizes', () => <StdSchematicViewer schDoc={schematic(
        [{
            ...hierLabelItem,
            shape: SheetLabelType.Input,
            position: { x: -200, y: -300 },
            text: SheetLabelType.Input,
            orientation: TextOrientation.Horizontal,
        },
        {
            ...hierLabelItem,
            shape: SheetLabelType.Input,
            position: { x: -200, y: -200 },
            text: SheetLabelType.Input,
            orientation: TextOrientation.Horizontal,
        },
        {
            ...hierLabelItem,
            shape: SheetLabelType.Input,
            position: { x: -200, y: -100 },
            text: SheetLabelType.Input,
            orientation: TextOrientation.Horizontal,
            italic: true,
        }]
    )} />)
    .add('Sizes', () => <StdSchematicViewer schDoc={schematic([
            { ...hierLabelItem, shape: SheetLabelType.Input,  position: { x: -100, y: -400 }, size: 50 },
            { ...hierLabelItem, shape: SheetLabelType.Output,  position: { x: -100, y: -300 }, size: 50 },
            { ...hierLabelItem, shape: SheetLabelType.BiDi,  position: { x: -100, y: -200 }, size: 50 },
            { ...hierLabelItem, shape: SheetLabelType.Unknown,  position: { x: -100, y: -100 }, size: 50 },
            { ...hierLabelItem, shape: SheetLabelType.Input,  position: { x: -100, y: 100 }, size: 100 },
            { ...hierLabelItem, shape: SheetLabelType.Output,  position: { x: -100, y: 200 }, size: 100 },
            { ...hierLabelItem, shape: SheetLabelType.BiDi,  position: { x: -100, y: 300 }, size: 100 },
            { ...hierLabelItem, shape: SheetLabelType.Unknown,  position: { x: -100, y: 400 }, size: 100 },

            { ...hierLabelItem, shape: SheetLabelType.Input,  position: { x: 400, y: 100 }, size: 100, orientation: TextOrientation.Horizontal },
            { ...hierLabelItem, shape: SheetLabelType.Input,  position: { x: 400, y: 100 }, size: 100, orientation: TextOrientation.HorizontalInvert },
            { ...hierLabelItem, shape: SheetLabelType.Input,  position: { x: 400, y: 100 }, size: 100, orientation: TextOrientation.VerticalUp },
            { ...hierLabelItem, shape: SheetLabelType.Input,  position: { x: 400, y: 100 }, size: 100, orientation: TextOrientation.VerticalDown },

            { ...hierLabelItem, shape: SheetLabelType.BiDi,  position: { x: 0, y: 600 }, size: 25, orientation: TextOrientation.Horizontal }
        ])
    } />)


    const hierarchicalSheet = {
        type: ItemType.Sheet,
        position: { x: 0, y: 0 },
        size: { x: 605, y: 540 },
        name: "Sheet5DCF2049",
        nameSize: 50,
        filename: "file5DCF2049.sch",
        filenameSize: 50,
        pins: [
            {
                number: 2,
                name: "IN1",
                shape: SheetLabelType.Input,
                side: SheetPinSide.Right,
                size: 50,
                position: { x: 605, y: 100 }
            },
            {
                number: 3,
                name: "OUT1",
                shape: SheetLabelType.Output,
                side: SheetPinSide.Right,
                size: 50,
                position: { x: 605, y: 200 }
            },
            {
                number: 4,
                name: "BIDI1",
                shape: SheetLabelType.BiDi,
                side: SheetPinSide.Right,
                size: 50,
                position: { x: 605, y: 300 }
            },
            {
                number: 5,
                name: "TRIS1",
                shape: SheetLabelType.ThreeState,
                side: SheetPinSide.Right,
                size: 50,
                position: { x: 605, y: 400 }
            },
            {
                number: 6,
                name: "PSV1",
                shape: SheetLabelType.Unspecified,
                side: SheetPinSide.Right,
                size: 50,
                position: { x: 605, y: 500 }
            },
            {
                number: 7,
                name: "IN2",
                shape: SheetLabelType.Input,
                side: SheetPinSide.Left,
                size: 50,
                position: { x: 0, y: 100 }
            },
            {
                number: 8,
                name: "OUT2",
                shape: SheetLabelType.Output,
                side: SheetPinSide.Left,
                size: 50,
                position: { x: 0, y: 200 }
            },
            {
                number: 9,
                name: "BIDI2",
                shape: SheetLabelType.BiDi,
                side: SheetPinSide.Left,
                size: 50,
                position: { x: 0, y: 300 }
            },
            {
                number: 10,
                name: "TRIS2",
                shape: SheetLabelType.ThreeState,
                side: SheetPinSide.Left,
                size: 50,
                position: { x: 0, y: 400 }
            },
            {
                number: 11,
                name: "PSV2",
                shape: SheetLabelType.Unspecified,
                side: SheetPinSide.Left,
                size: 50,
                position: { x: 0, y: 500 }
            }
        ],
        "timestamp": "5DCF204A"
    }

    // S 250  -100 150  500 
    // U 5DDC41EE
    // F0 "Sheet5DDC41ED" 50
    // F1 "file5DDC41ED.sch" 50
    // F2 "IN" I B 345 400 50 
    // F3 "OUT" O T 345 -100 50 
    //$EndSheet
    const hierarchicalSheetRotated = {
        type: ItemType.Sheet,
        position: { x: 250, y: -100 },
        size: { x: 150, y: 500 },
        name: "Sheet5DDC41ED",
        nameSize: 50,
        filename: "file5DDC41ED.sch",
        filenameSize: 50,
        pins: [
            {
                number: 2,
                name: "IN",
                shape: SheetLabelType.Input,
                side: SheetPinSide.Bottom,
                size: 50,
                position: { x: 345, y: 400 }
            },
            {
                number: 3,
                name: "OUT",
                shape: SheetLabelType.Output,
                side: SheetPinSide.Top,
                size: 50,
                position: { x: 345, y: -100 }
            }
        ]
    }

    const hierarchicalSheetLabelSize = {
        type: ItemType.Sheet,
        position: { x: 0, y: 0 },
        size: { x: 200, y: 300 },
        name: "Sheet5DDC41ED",
        nameSize: 100,
        filename: "file5DDC41ED.sch",
        filenameSize: 100,
        pins: []
    };

    const hierarchicalSheetPortTextSize = {
        type: ItemType.Sheet,
        position: { x: 0, y: 0 },
        size: { x: 500, y: 400 },
        name: "Sheet5DDC41ED",
        nameSize: 50,
        filename: "file5DDC41ED.sch",
        filenameSize: 50,
        pins: [
            {
                number: 2,
                name: "IN",
                shape: SheetLabelType.Input,
                side: SheetPinSide.Left,
                size: 100,
                position: { x: 0, y: 100 }
            },
            {
                number: 3,
                name: "OUT",
                shape: SheetLabelType.Output,
                side: SheetPinSide.Left,
                size: 100,
                position: { x: 0, y: 300 }
            }

        ]
    };

    storiesOf('Items/Hierarchical Sheet', module)
        .add('Straight', () => <StdSchematicViewer schDoc={schematic([hierarchicalSheet])} />)
        .add('Rotated', () => <StdSchematicViewer schDoc={schematic([hierarchicalSheetRotated])} />)
        .add('Label size', () => <StdSchematicViewer schDoc={schematic([hierarchicalSheetLabelSize])} />)
        .add('Port text size', () => <StdSchematicViewer schDoc={schematic([hierarchicalSheetPortTextSize])} />);

    ///////////////////////////////////
    // Parts
    ///////////////////////////////////
    const componentItem: ComponentItem = {
        type: ItemType.Component,
        libraryItem: {
            name: 'T1',
            libNickname: null,
            revision: null
        },
        refDes: 'Q1',
        unit: 0,
        convert: false,
        timestamp: '',
        position: { x: 0, y: 0 },
        fields: [],
        hierReference: [],
        transform: { x1: 1, y1: 0, x2: 0, y2: -1 },
        partUid: '0000'
    };
    const testLib: PartDatabase = { '0000': (testPart as Part) };

    storiesOf('Items/Part', module)
        .add('Straight', () => <StdSchematicViewer schDoc={schematic([{...componentItem, transform: { x1: 1, y1: 0, x2: 0, y2: -1 }}])} parts={testLib} />)
        .add('90 CW', () => <StdSchematicViewer schDoc={schematic([{...componentItem, transform: { x1: 0, y1: 1, x2: 1, y2: 0} }])} parts={testLib} />)
        .add('180 CW', () => <StdSchematicViewer schDoc={schematic([{...componentItem, transform: { x1: -1, y1: 0, x2: 0, y2: 1 }}])} parts={testLib} />)
        .add('270 CW', () => <StdSchematicViewer schDoc={schematic([{...componentItem, transform: { x1: 0, y1: -1, x2: -1, y2: 0 }}])} parts={testLib} />)
        .add('Horizontal Flip', () => <StdSchematicViewer schDoc={schematic([{...componentItem, transform: { x1: -1, y1: 0, x2: 0, y2: -1 }}])} parts={testLib} />)
        .add('Vertical Flip', () => <StdSchematicViewer schDoc={schematic([{...componentItem, transform: { x1: 1, y1: 0, x2: 0, y2: 1 }}])} parts={testLib} />)
        .add('Position', () => <StdSchematicViewer schDoc={schematic([{...componentItem, position: {x: 100, y: -100 }}])} parts={testLib} />)
        .add('Position+Transform', () => <StdSchematicViewer schDoc={schematic([
            {...componentItem, 
                position: {x: 100, y: -100 },
                transform: { x1: 0, y1: 1, x2: 1, y2: 0}
            },
            {...componentItem, 
                position: {x: 100, y: -100 },
                transform: { x1: 1, y1: 0, x2: 0, y2: -1}
            }

        ])} parts={testLib} />)

    ///////////////////////////////////
    // SchematicViewCore
    ///////////////////////////////////
    storiesOf('SchematicViewCore/Single Component', module)
        //@ts-ignore
        .add('Single', () => <StdSchematicViewer schDoc={singleComponentDesign as SchematicDocument} />)

    storiesOf('SchematicViewCore/PIC Programmer', module)      
        //@ts-ignore
        .add('pos=0,0 zoom=30', () => <StdSchematicViewer center={{x: 0, y: 0}} zoom={30} schDoc={picProgrammerDesign as SchematicDocument} />)
        //@ts-ignore
        .add('pos=100,100 zoom=15', () => <StdSchematicViewer center={{x: 3450, y: 1460}} zoom={10} schDoc={picProgrammerDesign as SchematicDocument} />)

        
    storiesOf('SchematicView', module)
        .add('PIC Programmer', () => 
            <SchematicView 
                width={VIEW_WIDTH} 
                height={VIEW_HEIGHT} 
                gridSpacing={100} 
                initialZoom={20}
                theme={KiCADTheme} 
                initialCenter={{ x: 0, y: 0 }}
                //@ts-ignore
                schDoc={picProgrammerDesign as SchematicDocument}
            />)
        .add('Complex Hierarchy', () =>
            <SchematicView
            width={VIEW_WIDTH} 
            height={VIEW_HEIGHT} 
            gridSpacing={100} 
            initialZoom={20}
            theme={KiCADTheme} 
            initialCenter={{ x: 0, y: 0 }}
            //@ts-ignore
            schDoc={complexHierarchyDesign as SchematicDocument}
        />);

//stories.addDecorator(withKnobs);
