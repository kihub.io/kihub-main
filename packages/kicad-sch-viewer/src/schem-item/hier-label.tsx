import * as React from 'react';
import { TextItem, TextOrientation, SheetLabelType } from '@kihub/kicad-sch-parser';
import { SchematicTheme } from 'theme';


export function HierLabel(props: { item: TextItem, theme: SchematicTheme }): JSX.Element {

    const { text, orientation, size, thickness, italic, shape } = props.item;
    const { x, y } = props.item.position;
    const { theme } = props;

    const invert = (orientation === TextOrientation.HorizontalInvert || orientation === TextOrientation.VerticalUp);
    const rotate = (orientation === TextOrientation.VerticalDown || orientation === TextOrientation.VerticalUp);;

    const textElement = <text
        x={invert? x + size + 25 : x - size - 25}
        y={y}  
        fontSize={size}
        fontWeight={thickness > 0 ? 'bold' : 'normal'}
        fontStyle={italic ? 'italic' : 'normal'}
        textAnchor={invert? 'start' : 'end'}
        dominantBaseline='middle'
        style={{
            fontFamily: theme.fontName,
            stroke: theme.hierLabel,
            fill: theme.hierLabel,
            strokeWidth: 0,
            
        }}>{text}</text>;

    const step = size / 2;
    const step2 = size;

    const symbolPath = 
        shape === SheetLabelType.Input? `l-${step} ${step} l-${step} 0 l0 -${step2} l${step} 0 l${step} ${step}` :
        shape === SheetLabelType.Output? `l0 -${step} l-${step} 0 l-${step} ${step} l${step} ${step} l${step}, 0 l0 -${step}` :
        shape === SheetLabelType.BiDi || shape === SheetLabelType.ThreeState? `l-${step} -${step} l-${step} ${step} l${step} ${step} l${step} -${step}` :
     /* shape === SheetLabelType.Unknown || Unspecified */ `l0 -${step} l-${step2} 0 l0 ${step2} l${step2} 0 l0 -${step}`
    
    const symbol = <path 
        d={`M${x} ${y} ${symbolPath}`} 
        transform={invert? `rotate(180 ${x} ${y})`: undefined}
        stroke={theme.hierLabel} 
        strokeWidth='6' 
        fill='none'/>

    return  <g transform={rotate? `rotate(-90 ${x} ${y})` : undefined}>
        {/* <circle cx={x} cy={y} r={10} stroke='none' fill='red'/> */}
        { symbol }
        { textElement }
        </g>
}

