import * as React from 'react';

import { SchematicTheme } from './theme';
import { ItemType, Position, SchematicDocument, Schematic, SheetItem } from '@kihub/kicad-sch-parser';
import { DrawSchematicItem, HierPath, EnterSheetHandler } from './schem-item';

export interface SchematiViewCoreProps {
    schDoc: SchematicDocument;
    width: number;
    height: number;
    theme: SchematicTheme;
    hierPath?: string;
    center?: Position;
    zoom?: number;
    gridSpacing?: number;
    extraDraw?: JSX.Element;
    drawPageBorder?: boolean;
    onZoom?: (ev: ZoomEvent) => void;
    onPan?: (ev: PanEvent) => void;
    onEnterSheet?: EnterSheetHandler;
}

export interface ZoomEvent {
    point: Position;
    amount: number;
}

export interface PanEvent {
    amountX: number;
    amountY: number;
}


export function resolveHierarchicalPath(hierPathString: string | undefined, schDoc: SchematicDocument): HierPath | undefined {
    if (!hierPathString)
        return [];

    const parts = hierPathString.split('/');
    const hierPath: HierPath = [];

    let sch: Schematic | undefined = schDoc.schematics[schDoc.root];
    parts.forEach(pathElement => {
        if (!sch)
            return;
        const sheetItem = sch.items
            .find(item => (item.type === ItemType.Sheet) && ((item as SheetItem).name === pathElement));

        if (sheetItem) {
            const item = sheetItem as SheetItem;
            sch = schDoc.schematics[item.filename];
            hierPath.push({
                name: item.name,
                filename: item.filename,
                timestamp: item.timestamp
            })
        }
        else
            sch = undefined;
    });

    if (sch)
        return hierPath;
    else
        return undefined;
}

export function SchematicViewCore(props: SchematiViewCoreProps): JSX.Element {

    const { schDoc, width, height, center, zoom, gridSpacing } = props;

    // Calculate the viewbox, such that the center
    // coordinate will be at the center of the svg
    const zoomFactor = (zoom || 1);
    const centerX = center ? center.x : 0;
    const centerY = center ? center.y : 0;
    const viewBoxMinX = centerX - width * zoomFactor / 2;
    const viewBoxMinY = centerY - height * zoomFactor / 2;
    const viewBox = `${viewBoxMinX} ${viewBoxMinY} ${width * zoomFactor} ${height * zoomFactor}`;

    // Find the schematic through the hierarchical path
    const hierPath = resolveHierarchicalPath(props.hierPath, schDoc);
    if (hierPath) {
        let schematic: Schematic | undefined = (hierPath && hierPath.length > 0) ?
            schDoc.schematics[(hierPath.slice(-1)[0]).filename] :
            schDoc.schematics[schDoc.root];

        const pageInfo = schematic?.descr?.pageInfo;

        let svgRef = React.useRef<SVGSVGElement>(null);
        /*
        let clientToSchemTransform: ((x: number, y: number) => Position)|undefined;
    
        React.useEffect(() => {
            if (svgRef.current) {
                const t = svgRef.current.getScreenCTM();
                if (t) {
                    const inverseCTM = t.inverse();
                    clientToSchemTransform = (x, y) => (new DOMPoint(x, y)).matrixTransform(inverseCTM);
                }
            }
        }, [ svgRef ]);
        */

        React.useEffect(() => {
            const cancelWheel = (event: any) => event.preventDefault();

            document.body.addEventListener('wheel', cancelWheel, { passive: false });

            return () => {
                document.body.removeEventListener('wheel', cancelWheel);
            }
        });

        const [draggingOrigin, setDraggingOrigin] = React.useState<Position | null>(null);

        return <svg ref={svgRef} width={width} height={height} viewBox={viewBox}

            onWheelCapture={(ev) => {
                if (svgRef.current && props.onZoom) {
                    const t = svgRef.current.getScreenCTM();
                    if (t) {
                        ev.stopPropagation();

                        const inverseCTM = t.inverse();
                        const clientToSchemTransform = (x: number, y: number) => (new DOMPoint(x, y)).matrixTransform(inverseCTM);

                        const point = clientToSchemTransform(ev.clientX, ev.clientY);
                        const amount = ev.deltaY;
                        props.onZoom({ point, amount });
                    }
                }
            }}

            onMouseDown={(ev) => {
                if (svgRef.current && props.onPan) {
                    const t = svgRef.current.getScreenCTM();

                    if (t) {
                        const origin = (new DOMPoint(ev.clientX, ev.clientY)).matrixTransform(t.inverse());
                        setDraggingOrigin(origin);
                    }
                }
            }}

            onMouseUp={(ev) => {
                if (draggingOrigin)
                    setDraggingOrigin(null);
            }}

            onMouseOut={(ev) => {
                if (draggingOrigin)
                    setDraggingOrigin(null);
            }}

            onMouseMove={(ev) => {
                const t = svgRef.current?.getScreenCTM()
                if (t) {
                    const point = (new DOMPoint(ev.clientX, ev.clientY)).matrixTransform(t.inverse());
                    if (draggingOrigin) {
                        props.onPan!({ amountX: point.x - draggingOrigin.x, amountY: point.y - draggingOrigin.y });
                    }
                }
            }}

        >
            {gridSpacing &&
                <defs>
                    <pattern id="grid" width={gridSpacing} height={gridSpacing} patternUnits="userSpaceOnUse" patternTransform="translate(-2.5 -2.5)">
                        <rect x="0" y="0" width="5" height="5" stroke="none" fill="gray" />
                    </pattern>
                </defs>}

            {props.gridSpacing &&
                <rect x={viewBoxMinX} y={viewBoxMinY} width='100%' height='100%' fill="url(#grid)" />}
            {schematic.items.map((schItem, index) =>
                <DrawSchematicItem
                    key={index}
                    item={schItem}
                    partDb={props.schDoc.parts}
                    theme={props.theme}
                    onEnterSheet={props.onEnterSheet}
                    hierPath={hierPath}
                />)}
            {props.extraDraw}
            {props.drawPageBorder? 
                <rect x={0} y={0} 
                    width={pageInfo.portrait? pageInfo.width : pageInfo.height} 
                    height={pageInfo.portrait? pageInfo.height : pageInfo.width} 
                    strokeWidth={8} stroke={props.theme.pageBorderColor} fill='none'/> : null}
        </svg>;
    }
    else {
        // Failed to find hierarchical path
        return <svg width={width} height={height}>
            <text x='100' y='100'>Not Found</text>
        </svg>
    }

}

export interface ViewerParameters {
    minZoom: number;  // The minimal zoom factor
    maxZoom: number;  // The maximal zoom factor
    zoomStep: number; // The factor from a whell step to zoom step
}

export interface SchematicViewProps {
    schDoc: SchematicDocument;
    width: number;
    height: number;
    theme: SchematicTheme;
    viewerParameters?: Partial<ViewerParameters>;
    initialHierPath?: string;
    initialCenter?: Position;
    initialZoom?: number;
    gridSpacing?: number;
    extraDraw?: JSX.Element;
}

const defaultViewerParameters: ViewerParameters = {
    minZoom: 2,
    maxZoom: 60,
    zoomStep: 10
};

const clamp = (min: number, max: number, v: number) => Math.min(Math.max(v, min), max);


export function SchematicView(props: SchematicViewProps) {

    const { initialHierPath, schDoc } = props;

    const { minZoom, maxZoom, zoomStep } = { ...defaultViewerParameters, ...props.viewerParameters };
    const [zoom, setZoom] = React.useState(props.initialZoom);
    const [center, setCenter] = React.useState(props.initialCenter);
    const [hierPath, setHierPath] = React.useState(initialHierPath);

    return <SchematicViewCore
        width={props.width}
        height={props.height}
        schDoc={schDoc}
        theme={props.theme}
        hierPath={hierPath}
        center={center}
        zoom={zoom}
        gridSpacing={props.gridSpacing}
        extraDraw={props.extraDraw}
        drawPageBorder={true}
        onZoom={(ev) => {
            const { point, amount } = ev;
            const newZoom = clamp(minZoom, maxZoom, zoom! * (1 + amount / zoomStep));
            const newCenter = {
                x: point.x - (point.x - center!.x) * newZoom / zoom!,
                y: point.y - (point.y - center!.y) * newZoom / zoom!
            }
            setZoom(newZoom);
            setCenter(newCenter);
        }}
        onPan={(ev) => {
            if (center) {
                setCenter({
                    x: center.x - ev.amountX,
                    y: center.y - ev.amountY
                });
            }
        }}
        onEnterSheet={(sheetHierPath) => {
            setHierPath(sheetHierPath);
        }}
    />
}