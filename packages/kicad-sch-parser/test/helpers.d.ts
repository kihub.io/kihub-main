
declare module Chai {
    interface Assertion {
        parseAs( schExpected: any ): Assertion;
        failParsingWith( schExpected: any ): Assertion;
        parseWithoutError(): Assertion;
    }
}
