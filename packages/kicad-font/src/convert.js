
const fs = require('fs');

const unicode = require('unicode/category');
const svg2ttf = require('svg2ttf');
const chalk = require('chalk');
const ttf2woff = require('ttf2woff');

const fontSource = require('./font-source');
const generatePreview = require('./preview');

const EMPTY_GLYPH = "F^K[KFYFY[K[";
const WEIGHT_NORMAL = 100;
const WEIGHT_BOLD = 130;
const EXTRA_SPACING = 300;

const EM = 2048;
const ASCENT = 1638;
const DESCENT = -410;
const CAP_HEIGHT_FACTOR = 0.8;


/**
 * Convert a glyph from source representation
 * 
 * 
 * @param {*} source 
 * @param {*} code 
 */
function convertGlyph(source, code) {
    /**
     * Gets the name of a Unicode character
     * 
     * @param {*} v 
     */
    function unicodeName(v) {
        for(const cat of Object.keys(unicode)) {
            if (unicode[cat][v])
                return unicode[cat][v].name;
        }
    }

    /**
     * Convert an integer to a 4-digit hex string
     * 
     * @param {*} n 
     */
    function toHex4(n) {
        let h = n.toString(16);
        while (h.length < 4)
            h = "0" + h
        return h;
    }


    /**
     * Convert a letter to numrical value
     * 
     * The value is computed by taking the ASCII value of the letter
     * and substracting the ASCII value of the letter R out of it.
     * 
     * @param {*} c 
     */
    function unR(c) {
        return c.charCodeAt(0) - 82;
    }

    // The size is encoded in the first two letters
    const startX = unR(source[0]);
    const endX = unR(source[1]);

    // Scan the rest of the source string. Every pair of letter
    // encodes a point coordinate. The glyph comprises of a line
    // that connects all the points. A " R" (space-R) combination
    // denotes "pen up" - that is, a beginning of a new line.
    let index = 2;
    // A Glyph is a list of lines and character width
    const glyph = {
        code,
        codeHex: toHex4(code),
        name: unicodeName(code),
        charWidth: endX - startX,
        segments: []
    }; 
    let lastPoint = null; 
    const points = []
    while (index < source.length) {
        if (source[index] === ' ' && source[index + 1] === 'R') {
            // Insert the current line into the Glyph
            lastPoint = null;
        }
        else {
            const p = {
                x: unR(source[index]),
                y: unR(source[index + 1]) - 10
            };
            points.push(p);

            if (lastPoint)
                glyph.segments.push([ lastPoint, p ])

            lastPoint = p;
            }

        index += 2;
    }

    const minX = Math.min(...points.map(p => p.x));
    const maxX = Math.max(...points.map(p => p.x));
    const minY = Math.min(...points.map(p => p.y));
    const maxY = Math.max(...points.map(p => p.y));
    glyph.bbox = { minX, maxX, minY, maxY }

    return glyph;
}

/**
 * Translate the coordinate system of the glyph to match the font metrics
 * 
 * @param {*} glyph 
 * @param {*} fontMetrics 
 */
async function translateGlyph(glyph, translate, weight, spacing) {
    const segments2 = glyph.segments.map(([p0, p1]) => [
        translate(0)(p0),
        translate(0)(p1)
    ]);

    outlines = segments2.map(seg => createOutline(seg[0], seg[1], weight));

    // Find the minimum and maximum X values in the glyph
    const glyphX = outlines.map(ol => {
        const xs = ol.map(p => p.x);
        return [Math.min(...xs), Math.max(...xs)];
    });
    const minX = Math.min(...glyphX.map(e => e[0]));
    const maxX = Math.max(...glyphX.map(e => e[1]));
    glyph.advanceX = maxX-minX+spacing;
    offsetX = -minX;
    glyph.offsetX = offsetX;

    // Apply the calculated offset to the strokes and the outlines
    glyph.segments2 = segments2.map(seg => [
        { x: seg[0].x + offsetX, y: seg[0].y },
        { x: seg[1].x + offsetX, y: seg[1].y }
    ]);
    glyph.outlines = outlines.map(ol =>
        ol.map(p => ({ x: p.x + glyph.offsetX, y: p.y }))
        );

    glyph.path = glyphToPath(glyph.outlines);

    // Special treatment: SPACE
    if (glyph.code === 32)
        glyph.advanceX = 200 + spacing;
}

/**
 * Calculate the outline stroke of a line defined by
 * the points p1 and p2. The width of the outline is d.
 * 
 * @param {*} p1 
 * @param {*} p2 
 * @param {*} d 
 */
function createOutline(p1, p2, d) {
    const alpha = Math.atan2(p2.y-p1.y, p2.x-p1.x);
    const cosa = Math.cos(alpha);
    const sina = Math.sin(alpha);

    /**
     * Calculate the corrdinates of a point, which has distance
     * d from the point p, perpendicular to the line.
     * 
     * @param {} p 
     * @param {*} d 
     */
    const dperp = (p, d) => ({
        x: p.x + d*sina,
        y: p.y - d*cosa
    });

        /**
     * Calculate the corrdinates of a point, which has distance
     * d from the point p, colinear with the line.
     * 
     * @param {} p 
     * @param {*} d 
     */
    const dcol = (p, d) => ({
        x: p.x + d*cosa,
        y: p.y + d*sina
    });

    return [
        dperp(p1, d/2),
        dperp(p2, d/2),
        dcol(p2, d/2),
        dperp(p2, -d/2),
        dperp(p1, -d/2),
        dcol(p1, -d/2)
    ];
}

/**
 * Convert a list of points into an SVG path
 * 
 * @param {*} outlines 
 */
function glyphToPath(outlines) {
    return outlines.map(ol => [
        //`M${ol[0].x} ${ol[0].y}`, 
        `M${ol[1].x} ${ol[1].y}`,
        `Q${ol[2].x} ${ol[2].y} ${ol[3].x} ${ol[3].y}`,
        `L${ol[4].x} ${ol[4].y}`,
        `Q${ol[5].x} ${ol[5].y} ${ol[0].x} ${ol[0].y}z`
    ].join(' ')).join('');
}



/**
 * Calculates font metrics.
 * 
 * See reference here: https://i.stack.imgur.com/ntg5f.png
 * @param {} glyphs 
 */
function calcFontMetrics(glyphs) {

    // Find the CAP height, from the height of the letter "A"
    const glyphCapA = glyphs.find(g => g.code==='A'.charCodeAt(0));
    const capHeightNative = glyphCapA.bbox.minY;
    const baselineNative = glyphCapA.bbox.maxY;

    // Calculate a scale factor, such that the entire character would fit
    // into a height of 1000 units.
    const ascentNative = capHeightNative/CAP_HEIGHT_FACTOR;
    const scale = ASCENT / ascentNative;  // Note the minus. The coordinate system
                                      // of the glyph has Y direction up!
    const n2f = h => (h - baselineNative) * scale;
    const translate = (xOffset) => ({x, y}) => ({x: -(x + xOffset)*scale, y: n2f(y)});

    return translate;
}



function generateSVGFont(glyphs, family, weight, stretch) {
    const glyphsSVG = glyphs.map(glyph =>
        `<glyph glyph-name="&#x${glyph.codeHex};" unicode="&#x${glyph.codeHex};" horiz-adv-x="${glyph.advanceX}" d="${glyph.path}"/>`);

    return svg = [
        '<?xml version="1.0" standalone="no"?>',
        '<!DOCTYPE svg PUBLIC "-//W3C//DTD SVG 1.1//EN" "http://www.w3.org/Graphics/SVG/1.1/DTD/svg11.dtd">',
        '<svg xmlns="http://www.w3.org/2000/svg">',
        '<defs>',
        '<font id="kicad-font" horiz-adv-x="1000">',
        `<font-face font-family="${family}" font-weight="${weight}" units-per-em="${EM}" ascent="${ASCENT}" descent="${DESCENT}" />`,
        '<missing-glyph horiz-adv-x="1000"/>',
        ...glyphsSVG,
        '</font>',
        '</defs>',
        '</svg>'
    ].join('\n');
}

function convertFont() {
    console.log('Converting font source');
    const glyphs = fontSource.map((glyphSource, index) => {
        const unum = 32 + index;
        s = `${unum}(${String.fromCharCode(unum)}) `;

        if (glyphSource !== EMPTY_GLYPH) {
            process.stdout.write(chalk.green(s));
            //console.log(`Converting Glyph ${unum}`);
            return convertGlyph(glyphSource, unum);
        }
        else
            process.stdout.write(chalk.red(s));
            //console.log(`Skipping Glyph ${unum}`)
    }).filter(glyph => !!glyph)
    console.log('');

    console.log('Calculating font metrics');
    const translate = calcFontMetrics(glyphs);
    console.log(`  em:         ${EM}`);
    console.log(`  ascent:     ${ASCENT}`);
    console.log(`  descent:    ${DESCENT}`);
    console.log('');

    return { glyphs, translate };
}

async function translateFont(font, fontName, weight, spacing, withPreview = false, withSVG = false, withTTF = false) {
    const { glyphs, translate } = font;

    console.log(`Translating and creating outlines for weight ${weight}`);
    await Promise.all(glyphs.map(glyph => translateGlyph(glyph, translate, weight, spacing)));


    if (withPreview) {
        // Generate the preview
        console.log('Creating glyph preview');
        generatePreview(glyphs, fontMetrics, fontName);
    }

    // Generate the SVG font source
    console.log('Creating SVG font');
    const svgFont = generateSVGFont(glyphs, fontName, 400);
    if (withSVG) {
        fs.writeFileSync(`${fontName}.svg`, svgFont);
    }

    // Generate the TTF font
    console.log('Creating TTF font');
    const ttf = svg2ttf(svgFont);
    if (withTTF) {
        fs.writeFileSync(`${fontName}.ttf`, ttf.buffer);
    }

    // Convert TTF to WOFF
    console.log('Converting TTF to WOFF');
    const woff = ttf2woff(ttf.buffer);
    fs.writeFileSync(`${fontName}.woff`, woff.buffer);
}

const genPreview = process.argv[2] == '--preview';

async function main() {
    font = convertFont();
    translateFont(font, 'kicad-newstroke', WEIGHT_NORMAL, EXTRA_SPACING, genPreview, genPreview, genPreview);
    translateFont(font, 'kicad-newstroke-bold', WEIGHT_BOLD, EXTRA_SPACING, genPreview, genPreview, genPreview);
    console.log('Done.');
}

main()


