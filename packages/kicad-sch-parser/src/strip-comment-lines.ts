'use strict'

const COMMENT_CHAR = '#'

interface StripReturnType {
    stripped: string;
    translateLineNumber: (n: number) => number;
}

/**
 * Strips comment lines (starting with '#') from the input text.
 * 
 * The function returns both the stripped text, and a mapping function
 * between the line number in the stripped text to the line number in
 * the original text.
 * 
 * @param text The text (CR delimited) to be stripped.
 * @returns An object containing two fields:
 *   - stripped - A string containing the stripped text
 *   - translateLineNumber - A function, that translate a (1-based) line number in
 *     the stripped text into the line number in the original text.
 */
export default function(text: string): StripReturnType {
	const d = [];
	const lineMap = [ ];
	let sourceLine = 0;
	text
		.split('\n')
		.forEach(line => {
			sourceLine += 1;
			const sl = line.trim()
			if (!sl.startsWith(COMMENT_CHAR)) {
				d.push(sl);
				lineMap.push(sourceLine);
			}
		});

	return {
		stripped: d.join('\n') + '\n',
		translateLineNumber: (n: number) => lineMap[n-1]
	}
}
