// EESchema library Grammer
//
// Based on the official spec at: http://bazaar.launchpad.net/~stambaughw/kicad/doc-read-only/download/head:/1115%4016bec504-3128-0410-b3e8-8e38c2123bca:trunk%252Fkicad-doc%252Fdoc%252Fhelp%252Ffile_formats%252Ffile_formats.pdf/file_formats.pdf
// Additional reference: eeschema/sch_legacy_plugin.cpp from KiCAD source tree

{
  const crypto = require('crypto')
  import {
    PartLibrary,
    PartLibraryVersion,
    PartDef,
    Part,
    FieldOrientation,
    Field,
    DrawSection,
    DrawList,
    DrawPrimitiveType,
    FillType,
    HorizontalAlignment,
    VerticalAlignment,
    DrawOrientationType,
    PinShapeType,
    DrawPrimitive,
    DrawArc,
    DrawCircle,
    DrawPolyline,
    DrawRect,
    DrawText,
    PinType,
    DrawPin,
    Convert,
    Position
  } from '../types';

  interface FieldDetails {
    name?: string;
    value: string;
    position: Position;
    size: number;
    orientation: FieldOrientation;
    visible: boolean;
    halign: HorizontalAlignment;
    valign: VerticalAlignment;
    italic: boolean;
    bold: boolean;
  }

}


// Part Library
PartLibrary =
  version:LibraryHeader parts:Part* 
  <PartLibrary>{
    // Create a part index
    const index = {};
    parts.forEach((part, partNum) => index[part.partUid] = partNum);

    return { version, parts, index };
  }

// Library Header
LibraryHeader =
  "EESchema-LIBRARY Version " major:[0-9]+ "." minor:[0-9]+ [^\n]* NEWLINE
  <PartLibraryVersion>{ 
  	return <PartLibraryVersion>{ 
    	major: parseInt(major.join('')),
    	minor: parseInt(minor.join('')),
    }
  }


// Single Part
Part =
  partdef:DEFLine fields:Field+ aliases1:Aliases? fpFilter:FPLIST? aliases2: Aliases? draw:DRAWSection ENDDEFLine
  <Part>{
      // It's rare, but some libraries have the ALIAS section *after* FPLIST
      if (aliases1 && aliases2)
        error("Only one ALIAS section is allowed");

      // Calculate the part UID using SHA256 digest
      const hash = new crypto.Hash('sha256');
      hash.update(text());
      const partUid: string = hash.digest('hex');


      // Helper function: retrieve a field with a given
      // number and remove it from the list
      const retrieveField = (l: Field[], index: number): (Field|undefined) => {
        const listIndex = l.findIndex(f => f.index === index);
        if (listIndex === -1)
          return undefined;
        else 
          return l.splice(listIndex, 1)[0];
      }

      // Find the reference field
      const referenceField = retrieveField(fields, 0);
      const valueField = retrieveField(fields, 1);
      const footprintField = retrieveField(fields, 2);
      const documentField = retrieveField(fields, 3);

      // Ref and value are mandatory
      if (!referenceField)
        error("Missing reference field in component");
      if (!valueField)
        error("Missing value field in component");

      // Determine if part has convert
      const hasConvert = draw.details.reduce((hasConvert, d) => hasConvert || d.convert === Convert.Convert, false);

      const part: Part = { ...partdef, draw, fields, referenceField, valueField, footprintField, documentField, partUid, hasConvert };
      if (aliases1)
        part.aliases = aliases1;
      if (aliases2)
        part.aliases = aliases2;
      if (fpFilter)
        part.fpFilter = fpFilter;


      return part;
  }

// Part definition line
DEFLine =
  "DEF" _ name:Name _ ref:Name _ "0" _ offset:Integer _ showPinNumber:YN _ showPinName:YN _ numOfParts:Positive _ locked:FL _ power:PN NEWLINE
  <PartDef>{
    return { name, ref, offset, showPinNumber, showPinName, numOfParts: numOfParts || 1, locked, power }
  }

ENDDEFLine
  = "ENDDEF" NEWLINE

// Fields
FieldDetails =
  value:QuotedString  _ x:Integer _ y:Integer _ size:Positive _ orientation:HV _ visible:VI attr:( _ @halign:CLR _ @valign:CTB @(italic:IN bold:BN)? )?
  <FieldDetails>{
    const attributes = attr?
      {
        halign: attr[0],
        valign: attr[1],
        italic: attr[2]? attr[2][0] : false,
        bold: attr[2]? attr[2][1]: false
      }
      :
      // Default field attributes, for older libraries
      {
        halign: HorizontalAlignment.Center,
        valign: VerticalAlignment.Center,
        italic: false,
        bold: false
      }
    return { value, position: {x, y}, size, orientation, visible, ...attributes }
  }


Field = "F" index:[0-9]+ _ details:FieldDetails fname:(_ QuotedString)? NEWLINE
  <Field>{
    const n = parseInt(index.join(''));

    // User field numbering starts at 4
    if (n > 99)
      error("Illegal field index");

    return { index: n, name: fname? fname[1] : undefined, ...details };
  }
  
// Footprint filter (FPLIST)
FPLIST =
    FPLISTStart fp:FPPattern* FPLISTEnd
    <string[]>{ return fp }

FPLISTStart
  = "$FPLIST" NEWLINE

FPLISTEnd
  = "$ENDFPLIST" NEWLINE
  
FPPattern =
    _ [^ \t\n\r$]* NEWLINE
    <string>{ return text().trim() }

// Part Aliases
Aliases = "ALIAS" _ names:( Name _ )+ NEWLINE
  <string[]>{
    return names.map(v => v[0])
  }
  
// Draw Section
DRAWSection =
  DRAWStart details:DrawPrimitive* DRAWEnd
    <DrawSection>{
      // Calculate the has of the DRAW section
      const hash = new crypto.Hash('sha256')
      hash.update(text())
      const digest: string = hash.digest('hex')
      
      return {
        details, hash: digest
      } 
    }
  
DRAWStart =
  "DRAW" NEWLINE
  
DRAWEnd =
  "ENDDRAW" NEWLINE
  
DrawPrimitive =
    DrawArc / DrawCircle / DrawPolyline / DrawRect / DrawText / DrawPin

DrawArc =
    "A" _ x:Integer _ y:Integer _ radius:Positive _ startAngle:Integer _ endAngle:Integer _ part:Positive _ convert:Convert _ pen:Integer extra:( _ @FILL @( _ @Integer _ @Integer _ @Integer _ @Integer )? )? NEWLINE
    <DrawArc>{
          // Code reference: kicad/common/geom.cpp, RotatePoint function
          const rotate = (center: { x: number, y: number }, radius: number, angle: number) => {
            const fangle = angle / 3600.0 * 2 * Math.PI
            const dx = radius * Math.cos(fangle)
            const dy = -radius * Math.sin(fangle)
            return {
              x: Math.round(center.x + dx),
              y: Math.round(center.y + dy)
            }
          }
      
          const arc = { type: DrawPrimitiveType.Arc, 
            center: { x, y },
            radius, startAngle, endAngle, part, convert, pen, fill: FillType.None }

          if (extra) {
            arc.fill = extra[0];
          }
          else
            arc.fill = FillType.None

          // If start and end point are specified, set them. Otherwise,
          // calculate them
          let start;
          let end;
          if (extra && extra[1]) {
              //const v = (<any>(<any>extra)[2]);
              const v = extra[1];
              start = { x: v[0], y: v[1] };
              end = { x: v[2], y: v[3] };
          }
          else {
            // The minus before the angle appears in loadArc method
            // of SCH_LEGACY_PLUGIN_CACHE, in sch_legacy_plugin.cpp
            start = rotate({ x, y }, radius, -startAngle);
            end = rotate({ x, y }, radius, -endAngle);
          }

          return { ...arc, start, end };
    }

DrawCircle =
    "C" _ x:Integer _ y:Integer _ radius:Positive _ part:Positive _ convert:Convert _ pen:Integer _ fill:FILL NEWLINE
    <DrawCircle>{
        return { type: DrawPrimitiveType.Circle, center: { x, y }, radius, part, convert, pen, fill }
    }
  
DrawPolyline =
    "P" _ count:Positive _ part:Positive _ convert:Convert _ pen:Integer _ vertices:( @Integer _ @Integer _ )+ fill:FILL? NEWLINE
    <DrawPolyline>{
        const vx = vertices.map(v => ({ x: v[0], y: v[1]}));

        // In KiCAD implementation, points beyond count are ignored, but
        // not consideled error.
        // Reference SCH_LEGACY_PLUGIN_CACHE::loadPolyLine method in sch_legacy_plugin.cpp
        return { type: DrawPrimitiveType.Polyline, convert, part, pen, 
          fill: fill || FillType.None, 
          vertices: vx.slice(0, count)
        }
    }
    
DrawRect =
    "S" _ x1:Integer _ y1:Integer _ x2:Integer _ y2:Integer _ part:Positive _ convert:Convert _ pen:Integer _ fill:FILL? NEWLINE
    <DrawRect>{
        return { 
          type: DrawPrimitiveType.Rect,
          topLeft: { x: x1, y: y1 },
          bottomRight: { x: x2, y: y2 },
          part, convert, pen, 
          fill: fill || FillType.None
        }
      }
    
DrawText =
    "T" _ angle:Integer _ x:Integer _ y:Integer _ size:Positive _ hidden:ZeroOne _ part:Positive _ convert:Convert _ text:QuotedOrEscapedString modifiers:(_ @italic:Italic _ @bold:ZeroOne @(_ @halign:CLR _ @valign:CTB)?)? NEWLINE
    <DrawText>{
      const t: DrawText = {
        type: DrawPrimitiveType.Text,
        position: { x, y }, 
        size, part, convert,
        angle, 
        text: text.replace("''", '"'),  // from loadText function in  
        visible: !hidden,
        italic: false,
        bold: false, 
        halign: HorizontalAlignment.Left,
        valign: VerticalAlignment.Center
      }

      if (modifiers) {
        t.italic = modifiers[0]
        t.bold = modifiers[1]

        // Alignment is optional
        if (modifiers[2]) {
          t.halign = modifiers[2]![0]
          t.valign = modifiers[2]![1]
        }
      }

      return t
    }

DrawPin =
    "X" _ name:PinName _ pinNumber:PinNumber _ x:Integer _ y:Integer _ pinLength:Positive _ orientation:Orientation _ pinNumberSize:Positive _ pinNameSize:Positive _ part:Positive _ convert:Convert _ pinType:PinType _ shape:PinShape? NEWLINE
    <DrawPin>{
        return {
          type: DrawPrimitiveType.Pin,
          position: { x, y },
          name, pinNumber, pinLength, orientation, pinNumberSize, pinNameSize, 
          part, convert, pinType, 
          shape: shape? shape[0] : PinShapeType.Line,
          visible: shape? shape[1] : true
        }
    }

// Common
Name =
    [^ \t\n\r]+ <string>{ return text() }

Integer =
     "-"? [0-9]+ <number>{ return parseInt(text(), 10); }
  
Positive =
     [0-9]+ <number>{ return parseInt(text(), 10); }
  
YN =
    [YN] <boolean>{ return text()==='Y' }

FL =
    [FL0] <boolean>{ return text()==='L' }

PN =
    [PN] <boolean>{ return text()==='P' }

HV =
    [HV] <FieldOrientation>{ return text()==='H'? FieldOrientation.Horizontal : FieldOrientation.Vertical }

VI =
    [VI] <boolean>{ return text()==='V' }

ZeroOne =
    [01] <boolean>{ return text()==='1' }
  
CLR =
    [CLR] <HorizontalAlignment>{
    switch(text()) {
    case 'C': return HorizontalAlignment.Center;
    case 'L': return HorizontalAlignment.Left;
    case 'R': return HorizontalAlignment.Right;
    default: throw error("Internal error");
    }
  }

CTB =
    [CTB] <VerticalAlignment>{
    switch(text()) {
    case 'T': return VerticalAlignment.Top;
    case 'C': return VerticalAlignment.Center;
    case 'B': return VerticalAlignment.Bottom
    default: throw error("Internal error");
    }
  }

Italic =
    v:("Italic" / "Normal")
    <boolean>{
      return v==='Italic'
    }

IN =
    [IN] <boolean>{ return text()==='I' }
  
BN =
    [BN] <boolean>{ return text()==='B' }

FILL =
    [fFN]? <FillType>{
    const c = text()
    switch(c) {
    case 'f':
      return FillType.Background;
    case 'F':
      return FillType.Foreground;
    default:
      return FillType.None;
    }
  }

Orientation =
    [UDLR]
    <DrawOrientationType>{
      const c = text()
      switch(c) {
        case 'U': return DrawOrientationType.Up
        case 'D': return DrawOrientationType.Down
        case 'L': return DrawOrientationType.Left
        case 'R': return DrawOrientationType.Right
        default: throw error("Internal error");
      }
    }
    
PinType =
    [IOBTPCENUWw]
    <PinType>{
      const c = text()
      switch(c) {
        case 'I': return PinType.Input;
        case 'O': return PinType.Output;
        case 'B': return PinType.Bidir;
        case 'T': return PinType.Tristate;
        case 'P': return PinType.Passive;
        case 'C': return PinType.OpenCollector;
        case 'E': return PinType.OpenEmitter;
        case 'N': return PinType.NotConnected;
        case 'U': return PinType.Unspecified;
        case 'W': return PinType.PowerInput;
        case 'w': return PinType.PowerOutput;
        default: throw error("Internal error");
      }
    }
  
// Pin shape can have some combinations, but not all are valid
PinShape =
    visible:"N"? shape:("IC" / "CI" / "LC" / "CL" / "L" / "C" / "I" / "V" / "F" / "X")?
    <[PinShapeType,boolean]>{
      const shapes = {
        I: PinShapeType.Inverted,
        C: PinShapeType.Clock,
        L: PinShapeType.InputLow,
        V: PinShapeType.OutputLow,
        F: PinShapeType.FallingEdge,
        X: PinShapeType.NonLogic,
        LC: PinShapeType.ClockLow,
        CL: PinShapeType.ClockLow,
        IC: PinShapeType.ClockInverted,
        CI: PinShapeType.ClockInverted
      }

      return [
        shape? shapes[shape] : PinShapeType.Line,
        (visible !== 'N')
      ]
    }

Convert = c:[0-9]
  <Convert>{
    if (c === '0')
      return Convert.All;
    else if (c === '1')
      return Convert.Regular;
    else if (c === '2')
      return Convert.Convert;
    else
      return Convert.None;
  }    
QuotedString =
    '"' inner:[^"]* '"' <string>{ return inner.join('') }

UnquotedString =
    head:[^"] tail:[^ \t\n\v\f\r]* <string>{ return head + tail.join(''); }

AlphaNumericString =
    chars:[^ \t]+ <string>{ return chars.join(''); }

EscapedString =
    UnquotedString
    <string>{
      return text().split('~').join(' ');
    }

QuotedOrEscapedString =
    QuotedString / EscapedString
  
PinName =
    [^ \t\n\r]+ <string>{ return text(); }
  
PinNumber =
    AlphaNumericString
    <string|null>{
      if (text() === "~")
        return null
      else
        return text()
    }

_ "whitespace"
  = [ \t]*
  
NEWLINE = [\n\r]+



