
// KiCAD source reference: eeschema/eeschema.cpp
export interface SchematicTheme {
    wire: string;         // LAYER_WIRE
    bus: string;          // LAYER_BUS
    junction: string;     // LAYER_JUNCTION
    localLabel: string;   // LAYER_LOCLABEL
    hierLabel: string;    // LAYER_HIERLABEL
    globalLabel: string;  // LAYER_GLOBLABEL
    pinNumber: string;    // LAYER_PINNUM
    pinName: string;      // LAYER_PINNAM
    fields: string;       // LAYER_FIELDS
    partReference: string;// LAYER_REFERNCEPART
    partValue: string;    // LAYER_VALUEPART
    notes: string;        // LAYER_NOTES
    foreground: string;   // LAYER_DEVICE
    background: string;   // LAYER_DEVICE_BACKGROUND
    netName: string;      // LAYER_NETNAM
    pin: string;          // LAYER_PIN
    sheet: string;        // LAYER_SHEET
    sheetFileName: string;// LAYER_SHEETFILENAME
    sheetName: string;    // LAYER_SHEETNAME
    sheetLabel: string;   // LAYER_SHEETLABEL
    noConnect: string;    // LAYER_NOCONNECT
    grid: string;         // LAYER_SCHEMATIC_GRID
    schBackground: string;// LAYER_SCHEMATIC_BACKGROUND
    cursor: string;       // LAYER_SCHEMATIC_CURSOR
    brightened: string;   // LAYER_BRIGHTENED
    hidden: string;       // LAYER_HIDDDEN
    worksheet: string;    // LAYER_WORKSHEET
    fontName: string;
    defaultThickness: number // Default line thickness
    pageBorderColor: string; // Page border color
}