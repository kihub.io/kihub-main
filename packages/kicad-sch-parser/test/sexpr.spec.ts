import { parse, NumberAtom, StringAtom, ParseError, ListAtom } from '../src/sexpr';

describe('S-Expression Parser', () => {

    it('Number', () => {
        expect(parse('(10 20.3)')).toEqual({
            value: [
                { value: 10, pos: 1 },
                { value: 20.3, pos: 4 }
            ],
            pos: 0
        });
    })

    it('Quoted with escape', () => {
        expect(parse('("\\n \\\\ \\r \\t x")')).toEqual({
            value: [ {
                value: '\n \\ \r \t x',
                pos: 1
            }],
            pos: 0
        });
    })

    it('Whitespace', () => {
        expect(parse('( \t  qwerty\n)')).toEqual({
            value: [ { value: 'qwerty', pos: 5 } ],
            pos: 0
        })
    });

    it('List', () => {
        expect(parse('(abc def\t\n "g x")')).toEqual({
            value: [
                { value: 'abc', pos: 1 },
                { value: 'def', pos: 5 },
                { value: 'g x', pos: 11 }
            ],
            pos: 0
        });
    });

    it('Nested Lists', () => {
        expect(parse('(abc(hello "world") qqq)')).toEqual({
            value: [
                { value: 'abc', pos: 1 },
                { value: [
                    { value: 'hello', pos: 5 },
                    { value: 'world', pos: 11}
                    ], pos: 4
                },
                { value: 'qqq', pos: 20 }
            ],
            pos: 0
        });
    });

    it('Unterminated quoted string', () => {
        expect(() => parse('("unterminated)')).toThrowError('Unterminated quoted string at 1');
    });

    it('Invalid escape character', () => {
        expect(() => parse('("\\q")')).toThrowError('Invalid escaped character at 3');
    });

    it('Unterminated list', () => {
        expect(() => parse('(((abc)def)')).toThrowError('Unterminated list');
    });

    it('Max depth', () => {
        expect(() => parse('(a(b(c)))', { maxDepth: 2 })).toThrowError('Maximum list depth reached');
    });

    it('Validate Number', () => {
        const v = parse('(10.1)').value[0] as NumberAtom;
        expect(v.value).toEqual(10.1);
        expect(v.validate({ min: 1, max: 20, integer: false })).toEqual(10.1);
        expect(() => v.validate({ min: 100 })).toThrow(ParseError);
        expect(() => v.validate({ max: 1 })).toThrow(ParseError);
        expect(() => v.validate({ integer: true })).toThrow(ParseError);
    });

    it('Validate String', () => {
        const v = parse('(abcd)').value[0] as StringAtom;
        expect(v.value).toEqual('abcd');
        expect(v.validate({ minLength: 2, maxLength: 10, match: /^[a-z]*$/})).toEqual('abcd');
        expect(() => v.validate({ minLength: 6 })).toThrow(ParseError);
        expect(() => v.validate({ maxLength: 2 })).toThrow(ParseError);
        expect(() => v.validate({ match: /^[b-z]*$/ })).toThrow(ParseError);
    });

    it('List Traversal', () => {
        const v = parse('(10 "qq" (key1 30) (key2 40) ("key3" "abc"))');
        expect(v.getNumberByIndex(0)).toEqual(10);
        expect(v.getStringByIndex(0)).toEqual('10');
        expect(v.getStringByIndex(1)).toEqual('qq');
        expect(v.getNumberByKey('key1')).toEqual(30);
        expect(v.getNumberByKey('key2')).toEqual(40);
        expect(v.getStringByKey('key2')).toEqual('40');
        expect(v.getStringByKey('key3')).toEqual('abc');

        expect(() => v.getNumberByIndex(30)).toThrow();
        expect(v.getNumberByIndex(30, { optional: true })).toBeUndefined();
        expect(() => v.getNumberByKey('xxx')).toThrow();
        expect(v.getNumberByKey('xxx', { optional: true })).toBeUndefined();

        expect(() => v.getNumberByIndex(1)).toThrow();

        expect(() => v.getStringByIndex(30)).toThrow();
        expect(v.getStringByIndex(30, { optional: true })).toBeUndefined();
        expect(() => v.getStringByKey('xxx')).toThrow();
        expect(v.getStringByKey('xxx', { optional: true })).toBeUndefined();

        expect(() => v.getNumberByIndex(1)).toThrow();
    });

});


const v = parse('(10 "qq" (key1 30) (key2 40) ("key3" "abc"))');
const zz  = v.getNumberByIndex(0);
