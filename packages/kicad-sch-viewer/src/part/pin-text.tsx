

// Reference: LIB_PIN::printPinTexts function from eeschema/lib_pin.cpp

import * as React from 'react';

import { Position, DrawOrientationType } from "@kihub/kicad-sch-parser";
import { SchematicTheme } from 'theme';

export interface PinTextProps {
    pinName?: string|null;
    pinNumber?: string|null;
    position: Position;
    pinLength: number;
    orientation: DrawOrientationType;
    textInside: number;
    showNumber: boolean;
    showName: boolean;
    pinNameSize: number;
    pinNumberSize: number;
    theme: SchematicTheme;
}

export function PinText(props: PinTextProps): JSX.Element {

    const { position, orientation, textInside, showNumber, showName, pinLength, pinName, pinNumber,
        pinNumberSize } = props;
    const { pinName: pinNameColor, pinNumber: pinNumberColor } = props.theme;
    let pinNumberElement: JSX.Element|undefined;
    let pinNameElement: JSX.Element|undefined;

    // TODO: Theme & constants
    const namePenWidth = 5;
    const numPenWidth = 5;

    const nameOffset = 4 /* PIN_TEXT_MARGIN */ + namePenWidth;
    const numOffset = 4 /* PIN_TEXT_MARGIN */ + numPenWidth;

    let x1 = position.x;
    let y1 = position.y;

    switch(orientation) {
        case DrawOrientationType.Up:
            y1 -= pinLength;
            break;
        case DrawOrientationType.Down:
            y1 += pinLength;
            break;
        case DrawOrientationType.Left:
            x1 -= pinLength;
            break;
        default: /* DrawOrientationType.Right */
            x1 += pinLength;
    }

    const commonTextProps = {
        dominantBaseline: 'middle',
        style: {
            fontFamily: /*theme.fontName */ 'kicad-newstroke',
            stroke: /* theme.foreground */ 'black',
            strokeWidth: 0,
            fontSize: pinNumberSize*1.2
        }
    }

    const grText = (txt: string, x: number, y: number, halign: 'start'|'end'|'middle', valign: 'hanging'|'bottom'|'middle', color: string, rotate: number = 0) =>
        <text {...commonTextProps} x={x} y={y} textAnchor={halign} dominantBaseline={valign} transform={rotate? `rotate(${rotate} ${x} ${y})` : undefined}>{txt}</text>

    if (textInside) {
        if ((orientation == DrawOrientationType.Left) || (orientation == DrawOrientationType.Right)) {
            if (showName && pinName) {
                if (orientation == DrawOrientationType.Right)
                    pinNameElement = grText(pinName, x1 + textInside, y1, 'start', 'middle', pinNameColor);
                else // orientation == DrawOrientationType.Left
                    pinNameElement = grText(pinName, x1 - textInside, y1, 'end', 'middle', pinNameColor);
            }
            if (showNumber && pinNumber) {
                pinNumberElement = grText(pinNumber, (x1 + position.x)/2, y1 - numOffset, 'middle', 'bottom', pinNumberColor);
            }
        }
        else /* Vertical */ {
            if (orientation == DrawOrientationType.Down) {
                if (showName && pinName) {
                    pinNameElement = grText(pinName, x1, y1 + textInside, 'end', 'middle', pinNameColor, -90);
                }
                if (showNumber && pinNumber) {
                    pinNumberElement = grText(pinNumber, x1 - numOffset, (y1 + position.y)/2, 'middle', 'bottom', pinNumberColor, -90);
                }
    
            }
            else /* Up */ {
                if (showName && pinName) {
                    pinNameElement = grText(pinName, x1, y1 - textInside, 'start', 'middle', pinNameColor, -90);
                }
                if (showNumber && pinNumber) {
                    pinNumberElement = grText(pinNumber, x1 - numOffset, (y1 + position.y)/2, 'middle', 'bottom', pinNumberColor, -90);
                }
            }
        }

     
    }
    else /* !textInside */ {
        if (orientation === DrawOrientationType.Right || orientation === DrawOrientationType.Left) {
            const x = (x1 + position.x) / 2;

            if (showName && pinName) {
                pinNameElement = grText(pinName, x, y1 - nameOffset, 'middle', 'bottom', pinNameColor);
            }

            if (showNumber && pinNumber) {
                pinNumberElement = grText(pinNumber, x, y1 + numOffset, 'middle', 'hanging', pinNumberColor);
            }
        }
        else /* Vertical */ {
            const y = (y1 + position.y) / 2;

            if (showName && pinName) {
                pinNameElement = grText(pinName, x1 - nameOffset, y, 'middle', 'bottom', pinNameColor, -90);
            }

            if (showNumber && pinNumber) {
                pinNumberElement = grText(pinNumber, x1 + numOffset, y, 'middle', 'hanging', pinNumberColor, -90);
            }
        }
    }

    return <>{pinNameElement}{pinNumberElement}</>
}