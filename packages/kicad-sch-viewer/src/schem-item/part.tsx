import * as React from 'react';
import { ComponentItem, Part, Field } from '@kihub/kicad-sch-parser';
import { PartDraw } from '../part';
import kicadTheme from '../kicad-theme';
import { SchematicTheme } from 'theme';
import { HierPath } from './sheet';

export interface PartItemProps {
    item: ComponentItem;
    part: Part;
    theme: SchematicTheme;
    hierPath: HierPath;
}

export function PartItem(props:PartItemProps) {
    const { item, part, hierPath } = props;

    const fields: Field[] = [];
    if (part.referenceField) fields.push(part.referenceField);
    if (part.valueField) fields.push(part.valueField);
    if (part.footprintField) fields.push(part.footprintField);
    if (part.documentField) fields.push(part.documentField);

    // Replace the fields from the ComponentItem (coming from the schematic)
    // with the fields in the part (coming from the library).
    item.fields.forEach(f => {
        const existingFieldIndex = fields.findIndex(f1 => f.index === f1.index);
        if (existingFieldIndex === -1) {
            // A field with this index does not exist - add it
            fields.push({
                ...f,
                position: {
                    x: f.position.x - item.position.x,
                    y: f.position.y - item.position.y  
                }
            });
        }
        else {
            fields[existingFieldIndex] =  { ...f,
                position: {
                    x: f.position.x - item.position.x,
                    y: f.position.y - item.position.y  
                }
            };
            // The field position in the schematic is given in schematic coordinates,
            // so we have to translate them into part coordinates to be drawn at
            // the required position.
        }
    });
    fields.forEach(f => {
        f.visible = true;
    });

    // If hierarchical prefix is given modify the reference
    // and the part number accordingly.
    const hierPrefix = '/' + hierPath.map(e => e.timestamp).concat(item.timestamp)?.join('/');
    let altUnit: number | undefined;
    if (hierPrefix && item.hierReference) {
        for (const hr of item.hierReference) {
            // @ts-ignore
            if (hr.rpath === hierPrefix) {
                // Find the reference field and modify it
                const refFieldIndex = fields.findIndex(f => f.index === 0);
                fields[refFieldIndex] = { ...fields[refFieldIndex], value: hr.ref }

                // Set an alternat unit number
                altUnit = hr.part;
            }
        }
    }

    return <g transform={`translate(${item.position.x} ${item.position.y})`}>
    <PartDraw 
        part={part}
        convert={false}
        subPart={altUnit || item.unit}
        theme={kicadTheme}
        transform={item.transform}
        fields={fields}
         />
    </g>;
}

