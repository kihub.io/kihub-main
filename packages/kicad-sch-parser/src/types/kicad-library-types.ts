import { type } from 'os';
import { HorizontalAlignment, VerticalAlignment, Field, Position } from './common';

export interface PartLibrary {
    version?: PartLibraryVersion;
    parts: Part[];
    index: {
        [key: string]: number;
    }
}

export interface PartLibraryVersion {
    major: number;
    minor: number;
}

export interface PartDef {
    name: string;
    ref: string;
    offset: number;
    showPinNumber: boolean;
    showPinName: boolean;
    numOfParts: number;
    locked: boolean;
    power: boolean;
}

export interface Part extends PartDef {
    referenceField: Field;
    valueField: Field;
    footprintField?: Field;
    documentField?: Field;
    fields: Field[];
    aliases?: string[];
    fpFilter?: string[];
    draw: DrawSection;
    partUid: string;
    hasConvert: boolean;
}

export interface DrawSection {
    hash: string;
    details: DrawList;
}

export type DrawList = DrawPrimitive[];

export enum DrawPrimitiveType {
    Arc = 'arc',
    Circle = 'circle',
    Polyline = 'polyline',
    Rect = 'rect',
    Text = 'text',
    Pin = 'pin'
}

export enum FillType {
    Background = 'bg',
    Foreground = 'fg',
    None = 'none'
}

export enum DrawOrientationType {
    Up = 'up',
    Down = 'down',
    Left = 'left',
    Right = 'right'
}

export enum PinShapeType {
    Line = 'line',
    Inverted = 'inverted',
    Clock = 'clock',
    InputLow = 'input-low',
    OutputLow = 'output-low',
    FallingEdge = 'falling-edge',
    NonLogic = 'non-logic',
    ClockLow = 'clock-low',
    ClockInverted = 'clock-inverted'
}

export type DrawPrimitive = DrawArc | DrawCircle | DrawPolyline | DrawRect | DrawText | DrawPin;

interface DrawPrimitiveBase {
    type: DrawPrimitiveType;
    part: number;
    convert: Convert;
}

export interface DrawArc extends DrawPrimitiveBase{
    center: Position;
    radius: number;
    startAngle: number;
    endAngle: number;
    pen: number;
    fill: FillType;
    start: Position,
    end: Position
}

export interface DrawCircle extends DrawPrimitiveBase {
    center: Position;
    radius: number;
    pen: number;
    fill: FillType;
}

export interface DrawPolyline extends DrawPrimitiveBase {
    part: number;
    convert: Convert;
    pen: number;
    fill: FillType;
    vertices: Position[];
}

export interface DrawRect extends DrawPrimitiveBase {
    type: DrawPrimitiveType;
    topLeft: Position;
    bottomRight: Position;
    pen: number;
    fill: FillType;
}

export interface DrawText {
    type: DrawPrimitiveType;
    text: string;
    position: Position,
    size: number;
    angle: number;
    part: number;
    convert: Convert;
    visible: boolean;
    italic: boolean;
    bold: boolean;
    halign: HorizontalAlignment;
    valign: VerticalAlignment;
}

export enum PinType {
    Input = 'input',
    Output = 'output',
    Bidir = 'bidirectional',
    Tristate = 'tristate',
    Passive =  'passive',
    OpenCollector = 'open collector',
    OpenEmitter = 'open emitter',
    NotConnected = 'not connected',
    Unspecified = 'unspecified',
    PowerInput = 'power input',
    PowerOutput = 'power output'
}

export interface DrawPin {
    type: DrawPrimitiveType;
    name: string;
    pinNumber: string|null;
    position: Position;
    pinLength: number;
    orientation: DrawOrientationType;
    pinNumberSize: number;
    pinNameSize: number;
    part: number;
    convert: Convert;
    pinType: PinType;
    shape: PinShapeType;
    visible: boolean;
};

export enum Convert {
    All = 'all',
    Regular = 'reg',
    Convert = 'cvt',
    None = 'none'
}


