import { Position, ComponentTransform } from '@kihub/kicad-sch-parser';

export function xor(a: boolean, b: boolean) { 
    return ( a || b ) && (!a || !b) 
};

export function applyTransform(p: Position, t: ComponentTransform) {
    return {
        x: t.x1*p.x + t.y1*p.y,
        y: t.x2*p.x + t.y2*p.y
    }
}
