import * as React from 'react';
import { IconButton } from './icon-button';
import zoomInSvg from './resources/zoom_in_black_24dp.svg';

const toolbarContainerStyle: React.CSSProperties = {
    position: 'absolute',
    width: '100%',
    display: 'flex',
    justifyContent: 'center' 
}

const toolbarStyle: React.CSSProperties = {
    position: 'absolute',
    width: '350px',
    display: 'flex',
    flexDirection: 'column',
    background: 'khaki',
    filter: 'drop-shadow(5px 5px 2px #101010)',
    padding: '10px',
    borderRadius: '5px',
    fontFamily: 'Roboto, sans-serif'
}

const topLineStyle: React.CSSProperties = {
    display: 'flex',
    flexDirection: 'row'
}

const searchInputStyle: React.CSSProperties = {
    outline: 'none',
    fontSize: '100%',
    borderRadius: '5px',
    width: '250px',
    padding: '9px 10px 9px 10px',
    background: '#ededed',
    marginRight: '10px'
}

const iconButtonStyle: React.CSSProperties = {
    padding: '7px',
    borderRadius: '4px',
    /*
    '&:hover': {
        background: '#e0ffff',
        cursor: 'pointer'
    }
    */
}

export function SchToolbar() {
    console.log('xxxxxxxxxxxxxxxxxxxx')
    console.log(zoomInSvg)
    return <div style={toolbarContainerStyle}>
        <div style={toolbarStyle}>
            <div style={topLineStyle}>
                <input type='text' placeholder='Search' style={searchInputStyle}/>
                <img src={zoomInSvg} alt="X"/>
            </div>

        </div>
    </div>
}