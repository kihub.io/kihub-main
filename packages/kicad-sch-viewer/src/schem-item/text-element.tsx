import * as React from 'react';
import { Position, TextOrientation } from "@kihub/kicad-sch-parser";

export interface TextElementProps {
    text: string;
    position: Position;
    size: number;
    orientation: TextOrientation;
    italic?: boolean;
    bold?: boolean;
    multiline?: boolean;
    lineSpacing?: number;
    color: string;
    fontName: string;
}

const defaultLineSpacing = 1.2;

export function TextElement(props: TextElementProps): JSX.Element {

    const { text, position, orientation, size, bold, italic, multiline, lineSpacing, fontName, color } = props;

    const [ rotate, anchor ] =
        orientation === TextOrientation.Horizontal? [ false, 'start' ] :
        orientation === TextOrientation.HorizontalInvert? [ false, 'end' ] :
        orientation === TextOrientation.VerticalUp? [ true, 'start' ] :
     /* orientation === TextOrientation.VerticalDown */ [ true, 'end' ]

    const lines = multiline? text.split('\n').reverse() : [ text.split('\n').join('') ];

    return <g>
        {lines.map((lineText, lineIndex) =>
            <text
                key={lineIndex}
                x={position.x}
                y={position.y}
                dy={-lineIndex * size * (lineSpacing || defaultLineSpacing)}
                fontSize={size}
                fontWeight={bold ? 'bold' : 'normal'}
                fontStyle={italic ? 'italic' : 'normal'}
                textAnchor={anchor}
                transform={rotate? 'rotate(90)' : undefined }
                style={{
                    fontFamily: fontName,
                    stroke: color,
                    fill: color,
                    strokeWidth: 0,
               }}>
                {lineText}
            </text> )}
        </g>
    }