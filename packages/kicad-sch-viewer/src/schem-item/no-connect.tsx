import * as React from 'react';
import { NoConnectionItem } from '@kihub/kicad-sch-parser';
import { SchematicTheme } from 'theme';

const NC_SIZE = 25;

export function NoConnection(props: { item: NoConnectionItem, theme: SchematicTheme }): JSX.Element {

    const { x, y } = props.item.position;

    return <path d={`M${x-NC_SIZE} ${y-NC_SIZE} L${x+NC_SIZE} ${y+NC_SIZE} M${x+NC_SIZE} ${y-NC_SIZE} L${x-NC_SIZE} ${y+NC_SIZE}`} 
        stroke={props.theme.noConnect} strokeWidth='6'/>
}
