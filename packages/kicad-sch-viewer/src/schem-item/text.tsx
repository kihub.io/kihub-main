import * as React from 'react';
import { TextItem, TextOrientation, TextItemType, Position } from '@kihub/kicad-sch-parser';

import { GlobalLabel } from './global-label';
import { HierLabel } from './hier-label';
import { SchematicTheme } from 'theme';

const LINE_SPACING = 1.2

export interface TextElementProps {
    text: string;
    position: Position;
    orientation: TextOrientation;
    size: number;
    bold: boolean;
    italic: boolean;
    multiline?: boolean;
    color: string;
    fontName: string;
}

export function TextElement(props: TextElementProps): JSX.Element {

    const { text, position, orientation, size, bold, italic, multiline, color, fontName } = props;

    const [ rotate, anchor ] =
        orientation === TextOrientation.Horizontal? [ false, 'start' ] :
        orientation === TextOrientation.HorizontalInvert? [ false, 'end' ] :
        orientation === TextOrientation.VerticalUp? [ true, 'start' ] :
     /* orientation === TextOrientation.VerticalDown */ [ true, 'end' ]

    const lines = multiline? text.split('\n').reverse() : [ text.split('\n').join('') ];

    return <g>
        {lines.map((lineText, lineIndex) =>
            <text
                key={lineIndex}
                x={position.x}
                y={position.y}
                dy={-lineIndex * size * LINE_SPACING}
                fontSize={size}
                fontWeight={bold? 'bold' : 'normal'}
                fontStyle={italic ? 'italic' : 'normal'}
                textAnchor={anchor}
                transform={rotate? 'rotate(90)' : undefined }
                style={{
                    fontFamily: fontName,
                    stroke: color,
                    fill: color,
                    strokeWidth: 0,
                }}>
                {lineText}
            </text> )}
        </g>
}

export function Text(props: { item: TextItem, theme: SchematicTheme }): JSX.Element {
    const { textType, text, position, orientation, size, thickness, italic } = props.item;
    const { theme } = props;

    switch(textType) {
        case TextItemType.Notes: 
            return <TextElement text={text} position={position} orientation={orientation} size={size} 
                bold={thickness > 0} italic={italic} multiline={true} color={theme.notes} fontName={theme.fontName}/>
        case TextItemType.GlobalLabel: 
            return <GlobalLabel {...props}/>
        case TextItemType.HierLabel:
            return <HierLabel {...props}/>
        case TextItemType.Label:
            return <TextElement text={text} position={position} orientation={orientation} size={size}
                bold={thickness > 0} italic={italic} multiline={false} color={theme.localLabel} fontName={theme.fontName}/>
        default: 
            return <></>
    }

}
