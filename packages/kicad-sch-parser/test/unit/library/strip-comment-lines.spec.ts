
import { expect } from 'chai';

import stripCommentLines from '../../../src/strip-comment-lines';

describe('strip-comment-lines', function() {

    it('Basic operation CR', function() {
        const text = [
            '# Line 1',
            'Line 2',
            ''
        ].join('\n');

        const r = stripCommentLines(text).stripped.split('\n');

        expect(r[0]).to.equal("Line 2");
        expect(r[1]).to.equal("");
    });

    it('Basic operation CR+LF', function() {
        const text = [
            '# Line 1',
            'Line 2',
            ''
        ].join('\r\n');

        const r = stripCommentLines(text).stripped.split('\n');

        expect(r[0]).to.equal("Line 2");
        expect(r[1]).to.equal("");
    });

    it('Line number mapping', function() {
        const text = [
            '# Line 1',
            'Line 2',
            '# Line 3',
            'Line 4',
            'Line 5'
        ].join('\n');
        const r = stripCommentLines(text).translateLineNumber;
        
        expect(r(1)).to.equal(2);
        expect(r(2)).to.equal(4);
        expect(r(3)).to.equal(5);
    })

})