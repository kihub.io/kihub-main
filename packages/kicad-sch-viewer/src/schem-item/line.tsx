import * as React from 'react';
import { LineItem, LineType, LineStyle, RGBColor } from '@kihub/kicad-sch-parser';
import { SchematicTheme } from 'theme';


// Convert KiCAD line style to SVG dash array
function styleToDash(style?: LineStyle): string|undefined  {
    switch(style) {
        case LineStyle.Solid: return undefined;
        case LineStyle.Dashed: return '20';
        case LineStyle.Dotted: return '10';
        case LineStyle.DashDot: return '20 10 10 10';
        default: return;
    }
};

// Convert RGB tuple to CSS style color string
function RGBtoColor(color: RGBColor): string {

    const toHex2 = (n: number): string => {
        const s = n.toString(16);
        return (s.length < 2)? '0' + s : s;
    }

    return `#${toHex2(color.r)}${toHex2(color.g)}${toHex2(color.b)}`;
}


export function Line(props: {item: LineItem, theme: SchematicTheme }) {

    const { lineType, start, end, width, color, style } = props.item;
    const { theme } = props;

    const wireDefaultStyle = {
        stroke: theme.wire,
        strokeWidth: 6
    }

    const busDefaultStyle = {
        stroke: theme.bus,
        strokeWidth: 12
    }

    const notesDefaultStyle = {
        stroke: theme.notes,
        strokeWidth: 6,
        strokeDasharray: styleToDash(LineStyle.Dashed)
    }

    const lineStyle: React.CSSProperties = 
        lineType === LineType.Wire? wireDefaultStyle :
        lineType === LineType.Bus? busDefaultStyle :
     /* lineType === LineType.Notes */ notesDefaultStyle;
    
    if (width)
        lineStyle.strokeWidth = width;
    if (style)
        lineStyle.strokeDasharray = styleToDash(style);
    if (color)
        lineStyle.stroke = RGBtoColor(color);

    return <line 
        x1={start.x}
        y1={start.y}
        x2={end.x}
        y2={end.y}
        r={15}
        style={lineStyle}
        fill='none'
        />

}
