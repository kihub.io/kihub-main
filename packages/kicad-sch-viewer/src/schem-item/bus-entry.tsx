import * as React from 'react';
import { EntryItem } from '@kihub/kicad-sch-parser';
import { SchematicTheme } from 'theme';

export function BusEntry(props: { item: EntryItem, theme: SchematicTheme }) {

    const { position, size, busEntry } = props.item;

    return <line 
        x1={position.x}
        y1={position.y}
        x2={position.x + size.x}
        y2={position.y + size.y}
        stroke={busEntry? props.theme.bus : props.theme.wire}
        strokeWidth={busEntry? 12 : 6}
        />

}
