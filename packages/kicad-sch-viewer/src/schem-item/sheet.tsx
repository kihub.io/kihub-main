import * as React from 'react';
import { SheetItem, TextItemType, TextOrientation, ItemType, SheetPinSide,/* SheetLabelType */ 
SheetLabelType,
Position} from '@kihub/kicad-sch-parser';
import { HierLabel } from './hier-label';
import { SchematicTheme } from 'theme';

/// Hierarchical Sheet Descriptor
///
/// An object implementing this interface describes an
/// instance of an hierarchical sheet.
export interface HierPathElement {
    name: string;
    timestamp: string;
    filename: string;
}

export type HierPath = HierPathElement[];

export type EnterSheetHandler = (desc: string) => void;

/**
 * Translate pin side to hierarchichal symbol orientation.
 * 
 * @param side 
 */
function translatePinSide(side: SheetPinSide): TextOrientation {
    switch(side) {
        case SheetPinSide.Left: return TextOrientation.HorizontalInvert;
        case SheetPinSide.Right: return TextOrientation.Horizontal;
        case SheetPinSide.Top: return TextOrientation.VerticalDown;
        default /* SheetPinSide.Bottom */: return TextOrientation.VerticalUp;
    }
}

/**
 * Change the direction of pin type
 * 
 * The shape of the arrow in the pin graphics is different between
 * hierarchical sheet labels and ports. This function converts input
 * to output to compensate for that. Other shapes are symmetrical and
 * left unchanged.
 * 
 * @param shape 
 */
function translatePinShape(shape: SheetLabelType): SheetLabelType {
    if (shape === SheetLabelType.Input)
        return SheetLabelType.Output;
    else if (shape === SheetLabelType.Output)
        return SheetLabelType.Input;
    else
        return shape;
}

function EnterSheetIcon(props: { position: Position, onClick?: () => void }): JSX.Element {
    const { x, y } = props.position;
    const scale = 8;

    return <g 
        transform={`scale(${scale}) translate(${x/scale},${y/scale})`} 
        style={{color: '#000000', strokeWidth: 3, strokeLinecap: 'butt', strokeLinejoin: 'round', strokeMiterlimit: 4, pointerEvents: 'all', cursor: 'pointer' }}
        pointerEvents='all'
        onClick={props.onClick}
        >
    <path
       d="m 9.1679688,1.7792969 c -3.0309468,0 -5.5136719,2.4827247 -5.5136719,5.5136719 V 9.5127897 H 6.6503906 V 7.2929688 c 0,-1.420839 1.09674,-2.5175782 2.5175782,-2.5175782 H 25.474609 c 1.420839,0 2.517579,1.0967392 2.517579,2.5175782 V 24.847656 c 0,1.420839 -1.09674,2.517578 -2.517579,2.517578 H 9.1679688 c -1.4208382,0 -2.5175782,-1.096739 -2.5175782,-2.517578 V 22.03931 H 3.6542969 v 2.808346 c 0,3.030947 2.4827251,5.521485 5.5136719,5.521485 H 25.474609 c 3.030947,0 5.513672,-2.490538 5.513672,-5.521485 V 7.2929688 c 0,-3.0309472 -2.482725,-5.5136719 -5.513672,-5.5136719 z"
       transform="scale(0.26458333)" />
    <path
       d="M 2.5040922,1.9570992 V 3.241606 H 0.01036917 V 5.1683642 H 2.5040922 V 6.4528697 L 4.7519771,4.2049851 Z"
    />

    </g>

}

export interface SheetProps {
    item: SheetItem;
    theme: SchematicTheme;
    hierPath: HierPath;
    onEnterSheet?: EnterSheetHandler;
}

export function Sheet(props: SheetProps) {

    const { size, pins, name, timestamp, nameSize, filename, filenameSize } = props.item;
    const { x, y } = props.item.position;
    const { sheet, sheetFileName, sheetLabel, fontName } = props.theme;
    const { onEnterSheet, hierPath } = props;

    // There is no direct indication whether the sheet is rotated, so
    // we check the first pin. If it has Top or Bottom side - the sheet is rotated.
    const rotated = (pins.length > 0) && (pins[0].side === SheetPinSide.Top || pins[0].side === SheetPinSide.Bottom);

    // Position of text labels
    const sheetNamePos = rotated?{ x: x - 25, y: y + size.y } : { x, y: y - 25 };
    const filenamePos = rotated? { x: x + size.x +  25, y: y + size.y } : { x, y: y + size.y + 25};

    return <g>
        <rect x={x} y={y} width={size.x} height={size.y} stroke={sheet} strokeWidth='6' fill='none'/>
        { pins.map(pin => <HierLabel 
            item={{
                type: ItemType.Text,
                textType: TextItemType.HierLabel,
                text: pin.name,
                position: pin.position,
                shape: translatePinShape(pin.shape),
                size: pin.size,
                thickness: 0,
                orientation: translatePinSide(pin.side),
                italic: false
            }}
            theme={props.theme}
            />) 
        }
        <text
            x={sheetNamePos.x}
            y={sheetNamePos.y}
            fontSize={nameSize}
            fontWeight='normal'
            fontStyle='normal'
            textAnchor='start'
            dominantBaseline='baseline'
            transform={rotated? `rotate(270 ${sheetNamePos.x} ${sheetNamePos.y})` : undefined}
            style={{
                fontFamily: fontName,
                stroke: sheetLabel,
                fill: sheetLabel,
                strokeWidth: 0,
            }}>{`Sheet: ${name}`}</text>
        <text
            x={filenamePos.x}
            y={filenamePos.y}
            fontSize={filenameSize}
            fontWeight='normal'
            fontStyle='normal'
            textAnchor='start'
            dominantBaseline='hanging'
            transform={rotated? `rotate(270 ${filenamePos.x} ${filenamePos.y})` : undefined}
            style={{
                fontFamily: fontName,
                stroke: sheetFileName,
                fill: sheetFileName,
                strokeWidth: 0,
            }}>{`File: ${filename}`}</text>
        <EnterSheetIcon position={{x: x + size.x - 80, y: y - 80}} 
            onClick={onEnterSheet && (() => onEnterSheet(hierPath.map(e => e.name).concat(name).join('/')))}/>

    </g>

}
