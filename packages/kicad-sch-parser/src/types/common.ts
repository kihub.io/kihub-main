
export interface Position {
    x: number;
    y: number;
}

export interface Size {
    x: number;
    y: number;
}

export enum FieldOrientation {
    Horizontal = 'horizontal',
    Vertical = 'vertical'
}

export interface Field {
    name?: string;
    index: number;
    value: string;
    position: Position;
    size: number;
    orientation: FieldOrientation;
    visible: boolean;
    halign: HorizontalAlignment;
    valign: VerticalAlignment;
    italic: boolean;
    bold: boolean;
}

export enum HorizontalAlignment {
    Left = 'left',
    Center = 'center',
    Right = 'right'
}

export enum VerticalAlignment {
    Top = 'top',
    Center = 'center',
    Bottom = 'bottom'
}
