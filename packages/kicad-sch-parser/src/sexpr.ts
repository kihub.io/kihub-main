
export interface NumberValidationOptions {
    min?: number;
    max?: number;
    integer?: boolean;
};

export interface StringValidationOptions {
    minLength?: number;
    maxLength?: number;
    match?: RegExp;
};
export interface Atom {
    readonly pos: number;
    readonly value: string | number | ReadonlyArray<Atom>;
};

export class ParseError extends Error {
    trace: number[];

    constructor(msg: string, pos: number) {
        super(msg);
        this.trace = [ pos ];

        // Set the prototype explicitly.
        Object.setPrototypeOf(this, ParseError.prototype);
    }

    pushTracePosition(pos: number) {
        this.trace = [ pos, ...this.trace ];
    }
}

export class NumberAtom implements Atom {

    readonly pos: number;
    readonly value: number;

    constructor(value: number, pos: number) {
        this.value = value;
        this.pos = pos;
    }

    validate(options: NumberValidationOptions = {}) {
        const { min, max, integer } = options;
        const { value, pos } = this;
   
        if (integer && !Number.isInteger(value))
            throw new ParseError('Expected an integer', pos);
    
        if (min !== undefined && value < min)
            throw new ParseError('Number too small', pos);
    
        if (max !== undefined && value > max)
            throw new ParseError('Number too big', pos);
    
        return value;
    }

    convertToString() {
        return new StringAtom(this.value.toString(), this.pos);
    }
}

export class StringAtom implements Atom {

    readonly pos: number;
    readonly value: string;

    constructor(value: string, pos: number) {
        this.value = value;
        this.pos = pos;
    }

    validate(options: StringValidationOptions = {}) {
        const { minLength, maxLength, match } = options;
        const { value, pos } = this;
   
        if (minLength !== undefined && value.length < minLength)
            throw new ParseError(`String too short at ${pos}`, pos);
    
        if (maxLength && value.length > maxLength)
            throw new ParseError(`String too long at ${pos}`, pos);
    
        if (match && !match.test(value))
            throw new ParseError(`String failed to validate at ${pos}`, pos);
    
        return value;
    }
}

export class ListAtom implements Atom {

    readonly pos: number;
    readonly value: ReadonlyArray<Atom>;

    constructor(value: ReadonlyArray<Atom>, pos: number) {
        this.value = value;
        this.pos = pos;
    }

    getByIndex<T>(index: number, elementParser: (el: Atom) => T, options: { optional: true }): T|undefined;
    getByIndex<T>(index: number, elementParser: (el: Atom) => T, options?: { optional?: boolean }): T;
    getByIndex<T>(index: number, elementParser: (el: Atom) => T, options?: { optional?: boolean }): T|undefined {
        const el = this.value[index];
    
        if (el === undefined) {
            if (!options?.optional) {
                throw new ParseError(`Expected to find an element with index ${index}`, this.pos);
            }
            else {
                return undefined;
            }
        }
        const z = el instanceof NumberAtom;
    
        try {
            return elementParser(el);
        }
        catch(e) {
            if (e instanceof ParseError) {
                e.pushTracePosition(el.pos);
            }
            throw e;
        }
        
    }

    getByKey<T>(key: string, elementParser: (el: ListAtom) => T, options: { optional: true }): T|undefined;
    getByKey<T>(key: string, elementParser: (el: ListAtom) => T, options?: { optional?: boolean }): T;
    getByKey<T>(key: string, elementParser: (el: ListAtom) => T, options?: { optional?: boolean }): T|undefined {
        const el = this.value.find(el => (el instanceof ListAtom) && ((el as ListAtom).value[0]?.value) === key);

        if (el === undefined) {
            if (!options?.optional) {
                throw new ParseError(`Expected to find an element with key ${key}`, this.pos);
            }
            else {
                return undefined;
            }
        }
      
        try {
            return elementParser(el as ListAtom);
        }
        catch(e) {
            if (e instanceof ParseError) {
                e.pushTracePosition(el.pos);
            }
            throw e;
        }
    };

    getMany<T>(key: string, elementParser: (el: ListAtom) => T) {
        return this.value
            .filter(el => (el instanceof ListAtom) && (el as ListAtom).value[0]?.value === key)
            .map(el => { 
                try {
                    return elementParser(el as ListAtom);
                }
                catch(e) {
                    if (e instanceof ParseError) {
                        e.pushTracePosition(el.pos);
                    }
                    throw e;
                }        
            });
    }


    //getNumberByIndex(index: number): number;
    getNumberByIndex(index: number, options: { optional: true } & NumberValidationOptions): number|undefined;
    getNumberByIndex(index: number, options?: { optional?: boolean } & NumberValidationOptions): number;
    getNumberByIndex(index: number, options?: { optional?: boolean } & NumberValidationOptions): number | undefined {
        const r = this.getByIndex(index, (el) => { 
            if (!(el instanceof NumberAtom))
                throw new ParseError('Expected a number but found something else', this.pos);

            return el.validate(options)
        }, options);
        return r;
    }

    getNumberByKey(key: string, options: { optional: true } & NumberValidationOptions): number|undefined;
    getNumberByKey(key: string, options?: { optional?: boolean } & NumberValidationOptions): number;
    getNumberByKey(key: string, options?: { optional?: boolean } & NumberValidationOptions) {
        return this.getByKey(key, (el) => { 
            const v = el.value[1];
            if (!v)
                throw new ParseError('Expected to find a value after the key', this.pos);
            if (!(v instanceof NumberAtom))
                throw new ParseError('Expected a number but found something else', this.pos);

            return v.validate(options);
        }, options);
    }

    getStringByIndex(index: number, options: { optioanl: true } & StringValidationOptions): string|undefined;
    getStringByIndex(index: number, options?: { optional?: boolean } & StringValidationOptions): string;
    getStringByIndex(index: number, options?: { optional?: boolean } & StringValidationOptions): string|undefined {
        return this.getByIndex(index, (el) => {
            if (el instanceof StringAtom) {
                return el.validate(options);
            }
            else if (el instanceof NumberAtom) {
                return el.convertToString().validate(options);
            }
            else {
                throw new ParseError('Expected a string but found a list', this.pos);
            }
        },
        options
        );
    }

    getStringByKey(key: string, options: { optioanl: true } & StringValidationOptions): string|undefined;
    getStringByKey(key: string, options?: { optional?: boolean } & StringValidationOptions): string;
    getStringByKey(key: string, options?: { optional?: boolean } & StringValidationOptions) {
        return this.getByKey(key, (el) => {
            const v = el.value[1];
            if (v instanceof StringAtom) {
                return v.validate(options);
            }
            else if (v instanceof NumberAtom) {
                return v.convertToString().validate(options);
            }
            else {
                throw new ParseError('Expected a string but found a list', this.pos);
            }
        },
        options
        );
    }

}

export interface ParseOptions {
    maxDepth?: number;
}

const DEFAULT_PARSE_OPTIONS = {
    maxDepth: 100
}


const whitespace = /^[ \t\n\r\b\f\v]/;

function parseQuotedString(input: string, pos: number): [ StringAtom, number ] {
    let inc = 1;
    let value = '';
    let escape = false;

    while(true) {

        const c = input[pos + inc];
        inc += 1;

        if (!c)
            throw Error(`Unterminated quoted string at ${pos}`);

        if (escape) {
            if (c === '"')
                value += '"';
            else if (c === 'n')
                value += '\n';
            else if (c === 'r')
                value += '\r';
            else if (c === 't')
                value += '\t';
            else if (c === '\\')
                value += '\\';
            else
                throw Error(`Invalid escaped character at ${pos + inc - 1}`);

            escape = false;
        }
        else {
            if (c === '\\')
                escape = true;
            else if (c === '"')
                return [ new StringAtom(value, pos), inc ];
            else
                value += c;
        }

    }

}

function parseUnquoted(input: string, pos: number): [ StringAtom|NumberAtom, number ] {
    let inc = 0;
    let value = '';

    while(true) {
        const c = input[pos + inc];
        inc += 1;

        if (!c)
            break;

        if (whitespace.test(c) || c === '(' || c === ')')
            break;

        value += c;
    }

    const numValue = parseFloat(value);
    return ([ Number.isNaN(numValue)? new StringAtom(value, pos) : new NumberAtom(numValue, pos), inc - 1 ]);
}

function parseList(input: string, pos: number, depth: number): [ ListAtom, number ] {
    if (depth <= 0)
        throw new ParseError('Maximum list depth reached', pos);

    let inc = 1;
    let value: Atom[] = [];

    while(true) {

        const c = input[pos + inc];

        if (!c)
            throw new ParseError('Unterminated list', pos);

        if (whitespace.test(c)) {
            inc += 1;
            continue;
        }

        else if (c === '(') {
            const [ elementValue, elementInc ] = parseList(input, pos + inc, depth - 1);
            value.push(elementValue); 
            inc += elementInc;
        }

        else if (c === ')') {
            return [ new ListAtom(value, pos), inc + 1 ];
        }

        else if (c === '"') {
            const [ elementValue, elementInc ] = parseQuotedString(input, pos + inc);
            value.push(elementValue);
            inc += elementInc;
        }

        else {
            const [ elementValue, elementInc ] = parseUnquoted(input, pos + inc);
            value.push(elementValue);
            inc += elementInc;
        }
    }
}

export function parse(input: string, options?: ParseOptions): ListAtom {
    const { maxDepth } = { ...DEFAULT_PARSE_OPTIONS, ...options }

    if (input[0] === '(') {
        return parseList(input, 0, maxDepth)[0];
    }
    else {
        throw new ParseError("Top level element must be a list", 0);
    }
}
