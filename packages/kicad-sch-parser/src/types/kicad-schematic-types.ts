import { Size, Position, Field } from './common';
import { Part } from './kicad-library-types';

export type PartDatabase = { [ uid: string ]: Part };

export interface SchematicDocument {
    schematics: { [name: string]: Schematic };
    root: string;
    parts: PartDatabase;
}

export interface Schematic {
    formatVersion: number,
    libs: string[],
    descr: DescriptionBlock,
    items: SchematicItem[],
}

export interface DescriptionBlockInner {
    sheetNumber?: number;
    totalSheets?: number;
    title?: string;
    date?: string;
    rev?: string;
    company?: string;
    comment1?: string;
    comment2?: string;
    comment3?: string;
    comment4?: string;
    encoding?: string;
}

export interface DescriptionBlock extends DescriptionBlockInner {
    pageInfo: PageInfo;
}

export interface PageInfo {
    pageSize: string;
    width: number;
    height: number;
    portrait?: boolean;
}

/*
type SchematicItem = ComponentItem | NoConnectItem | SheetItem | TextItem | LabelItem |
  JunctionItem | LineItem | BitmapItem;
*/

export enum ItemType {
    Component = 'component',
    NoConnect = 'noconn',
    Sheet = 'sheet',
    Text = 'text',
    Junction = 'junction',
    Line = 'line',
    BusEntry = 'busentry',
    Bitmap = 'bitmap'
}

export interface SchematicItem {
    type: ItemType;
}

export interface ComponentItem extends SchematicItem {
    libraryItem: LibraryItem;
    refDes: string;
    unit: number,
    convert: boolean,
    timestamp: string,
    position: Position;
    fields: Field[];
    hierReference: ComponentHierReference[];
    transform: ComponentTransform;

    partUid?: string;
}

export interface ComponentHierReference {
    path: string;
    ref: string;
    part: number;
}

export interface ComponentTransform {
    x1: number;
    y1: number;
    x2: number;
    y2: number;
}

export interface LibraryItem {
    name: string;
    libNickname: string | null;
    revision: string | null;
}

export interface NoConnectionItem extends SchematicItem {
    position: Position;
}

// Note, Label
export interface TextItem extends SchematicItem {
    textType: TextItemType;
    text: string;
    position: Position;
    orientation: TextOrientation;
    size: number;
    thickness: number;
    italic: boolean;
    shape?: SheetLabelType;
}

export enum TextItemType {
    Notes = 'notes',
    Label = 'label',
    HierLabel = 'hier_label',
    GlobalLabel = 'global_label'
}

export interface JunctionItem extends SchematicItem {
    position: Position;
}

// Wire, Bus, Dotted, Bus Entry
export interface LineItem extends SchematicItem {
    lineType: LineType,
    start: Position;
    end: Position;
    width?: number;
    color?: RGBColor;
    style?: LineStyle;
}

export enum LineType {
    Wire = 'wire',
    Bus = 'bus',
    Notes = 'notes'
}

export enum LineStyle {
    Solid = 'solid',
    Dashed = 'dashed',
    Dotted = 'dotted',
    DashDot = 'dash_dot'    
}

export interface RGBColor {
    r: number;
    g: number;
    b: number;
    alpha?: number;
}

export interface BitmapItem extends SchematicItem {
    position: Position;
    scale: number;
    data: string;
}

export enum TextOrientation {
    Horizontal = 'horiz_normal',       // 0 Left
    VerticalUp = 'vert_up',            // 1 Up
    HorizontalInvert = 'horiz_invert', // 2 Right
    VerticalDown = 'vert_down'         // 3 Down
}

// sch_text.cpp, definition of SheetLabelType
export enum SheetLabelType {
    Input = 'input',
    Output = 'output',
    BiDi = 'bidi',
    ThreeState = '3state',
    Unspecified = 'unspc',
    Unknown = '???'
}

export interface EntryItem extends SchematicItem {
    busEntry: boolean;
    position: Position;
    size: Size;
}

export interface SheetItem extends SchematicItem {
    position: Position;
    size: Size;
    timestamp: string;
    name: string;
    filename: string;
    nameSize: number;
    filenameSize: number;
    pins: SheetPin[];
}

export interface SheetPin {
    number: number;
    name: string;
    shape: SheetLabelType;
    side: SheetPinSide;
    position: Position;
    size: number;
}

export enum SheetPinSide {
    Right = 'right',
    Left = 'left',
    Top = 'top',
    Bottom = 'bottom'
}
