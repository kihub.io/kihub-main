import * as React from 'react';
import { useRef, useEffect, useState } from 'react';
import { TextItem, TextOrientation, SheetLabelType } from '@kihub/kicad-sch-parser';
import { SchematicTheme } from 'theme';

const LABEL_PADDING_TOP = 5;

export function GlobalLabel(props: {item: TextItem, theme: SchematicTheme }): JSX.Element {

    const { text, orientation, size, thickness, italic, shape } = props.item;
    const { x, y } = props.item.position;
    const { theme } = props;

    const invert = (orientation === TextOrientation.HorizontalInvert || orientation === TextOrientation.VerticalUp);
    const rotate = (orientation === TextOrientation.VerticalDown || orientation === TextOrientation.VerticalUp);;

    const textRef = useRef<SVGTextElement>(null);
    const [ textSize, setTextSize ] = useState<DOMRect|undefined>(undefined);

    useEffect(() => {
        const clientRect = textRef.current!.getBBox();
        setTextSize(clientRect);
    }, [ textRef.current ])

    const textElement = <text
        ref={textRef}
        x={invert? x + 25 : x - 25}
        y={y}  
        fontSize={size}
        fontWeight={thickness > 0 ? 'bold' : 'normal'}
        fontStyle={italic ? 'italic' : 'normal'}
        textAnchor={invert? 'start' : 'end'}
        dominantBaseline='middle'
        style={{
            fontFamily: theme.fontName,
            stroke: theme.globalLabel,
            fill: theme.globalLabel,
            strokeWidth: 0,
        }}>{text}</text>;

    let frame;
    if (textSize) {
        const { height, width } = textSize;
        const top = y + height/2 + LABEL_PADDING_TOP;
        const bottom = y - height/2 - LABEL_PADDING_TOP;
        const side = x - 50 - width;
        const offset1 = (shape === SheetLabelType.BiDi || shape === SheetLabelType.Input || shape === SheetLabelType.ThreeState)? 25 : 0;
        const offset2 = (shape === SheetLabelType.BiDi || shape === SheetLabelType.Output || shape === SheetLabelType.ThreeState)? 25 : 0;

        const points = [
            `M${x} ${y}`,
            `L${x - offset1} ${top}`,
            `L${side + offset2} ${top}`,
            `L${side} ${y}`,
            `L${side + offset2} ${bottom}`,
            `L${x - offset1} ${bottom}`,
            `L${x} ${y}`
        ];

        frame = <path 
            d={points.join(' ')} 
            transform={invert? `rotate(180 ${x} ${y})` : undefined}
            stroke={theme.globalLabel} 
            strokeWidth='6' 
            fill='none'/>
    }

    return  <g transform={rotate? `rotate(-90 ${x} ${y})` : undefined}>
        {/* <circle cx={x} cy={y} r={10} stroke='none' fill='red'/> */}
        { frame }
        { textElement }
        </g>
}

