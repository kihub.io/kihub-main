import * as React from 'react';

import { storiesOf } from '@storybook/react';

import { SchToolbar } from '../src/toolbar';
import { SchematicViewCore } from '../src/schematic';
import picProgrammerDesign from './resources/pic_programmer.json';
import KiCADTheme from '../src/kicad-theme';
import { SchematicDocument } from '@kihub/kicad-sch-parser';


storiesOf('Toolbar', module)
    .add('Nav Mode', () => <div>
        <SchToolbar/>
        //@ts-ignore
        <SchematicViewCore schDoc={picProgrammerDesign as SchematicDocument} width={800} height={800} theme={KiCADTheme} center={{ x: 6000, y: 6000}} zoom={10}/>
        </div>)
