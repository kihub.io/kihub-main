'use strict'

const path = require('path');
const fs = require('fs');

const previewHeight = 200;
const midY = previewHeight / 2;
const baselineOffset = previewHeight - 15;
const strokeOffset = 250;
const outlineOffset = 400;
const filledOffset = 550;

function drawStrokes(glyph, offsetX, offsetY, scale) {
    const { segments2 } = glyph;
    return segments2.map(seg => 
        `<line x1="${seg[0].x}" y1="${seg[0].y}" x2="${seg[1].x}" y2="${seg[1].y}" stroke="black" stroke-width="30px"/>`
    )
}

function drawOutline(glyph, filled) {
    return [
        `<path d="${glyph.path}" stroke="black" stroke-width="1" fill="${filled? 'black' : 'none'}" />`,
    ]
}

function axes(x, y, size) {
    const color = 'red';
    return [
        `<line x1="${x-size}" y1="${y}" x2="${x+size}" y2="${y}" stroke="${color}" stroke-width="0.5"/>`,
        `<line x1="${x}" y1="${y-size}" x2="${x}" y2="${y+size}" stroke="${color}" stroke-width="0.5"/>`,
    ];
}

function glyphBox(offsetX, fontMetrics, advanceX, content) {
    const width = advanceX;
    const vspace = 20;

    const scale = (previewHeight - 2*vspace) / 1000;
    const translateY = -(previewHeight - vspace)/scale + fontMetrics.descend;
    const translateX = offsetX/scale;
    return [
        `<g transform="scale(${scale},${-scale}) translate(${translateX},${translateY})">`,
        `<rect x="0" y="${-fontMetrics.descend}" height="1000" width="${width}" stroke="red" stroke-width="2px" fill="none"/>`,
        `<line x1="0" y1="0" x2="${width}" y2="0" stroke="red" stroke-width="10px" stroke-dasharray="4"/>`,
        `<line x1="0" y1="${fontMetrics.xline}" x2="${width}" y2="${fontMetrics.xline}" stroke="blue" stroke-width="5px" stroke-dasharray="4"/>`,
        `<line x1="0" y1="${fontMetrics.capHeight}" x2="${width}" y2="${fontMetrics.capHeight}" stroke="green" stroke-width="5px" stroke-dasharray="4"/>`,
        ...content,
        `</g>`
    ]
}

    
/**
 * Create a preview SVG for a single Glyph.
 * 
 * A preview SVG includes the following elements:
 * - The glyph Unicode value
 * - The glyph Unicode name
 * - The Unicode character
 * - The Glyph, represented as a set of lines (stroke) - directly from the original source
 * - The Glyph, represented as a set of outlines (countours)
 * - The Glyph, represented as a set of outlines (countours) - filled
 * 
 * @param {*} glyph The Glyph to which the preview is requested
 * @returns An SVG image of the preview
 */
function previewGlyph(glyph, fontMetrics) {
    return [
        `<svg xmlns="http://www.w3.org/2000/svg" width="800px" height="${previewHeight}">`,
        '<style>.txt { font: bold 40px monospace; fill: black; stroke: none }',
        '.stxt { font: bold 20px monospace; fill: black; stroke: none } </style>',
        `<text x="10" y="${midY}" class="txt">${glyph.codeHex}</text>`,
        `<text x="10" y="20" class="stxt" text-anchor="left">${glyph.name}</text>`,
        `<text x="130" y="${midY}" class="txt">${String.fromCharCode(glyph.code)}</text>`,
        ...glyphBox(strokeOffset, fontMetrics, glyph.advanceX, drawStrokes(glyph)),
        ...glyphBox(outlineOffset, fontMetrics, glyph.advanceX, drawOutline(glyph, false)),
        ...glyphBox(filledOffset, fontMetrics, glyph.advanceX, drawOutline(glyph, true)),
        '</svg>'
    ].join('');
}

function preview(glyphs, fontMetrics, fontName) {
    const previewHtmlFile = `preview-${fontName}.html`;
    const previewDir = `preview-${fontName}`;

    // Create a directory to hold all the SVGs
    if (!fs.existsSync(previewDir))
        fs.mkdirSync(previewDir);

    // Create individual SVG for each glyph
    const bodyHtml = [];
    const indexHtml = [];
    glyphs.forEach((glyph, index) => {
        const svg = previewGlyph(glyph, fontMetrics);

        const { code } = glyph;
        const ucode = `u${code}`;
        const previewFilename = path.resolve(previewDir, `${ucode}.svg`)
        fs.writeFileSync(previewFilename, svg);

        // Body & index
        bodyHtml.push(`<a id="${ucode}"/><img src="preview/${ucode}.svg"/>`);
        indexHtml.push(`<li>&#x${glyph.codeHex};<a href="#${ucode}"/></li>`);
    });

    // Finally, create the preview HTML
    const previewHtml=`
<!DOCTYPE html>
<html>
<head>
<title>KiCAD Font Preview</title>
<style>
li {
    display: inline;
    padding: 10px;
    line-height: 45px;
    border: 1px solid black;
    font-size: 18;
}
img {
    display: block;
    border: 1px solid blue;
}
</style>
</head>
<body>
<ul>${indexHtml.join('')}</ul>
${bodyHtml.join('')}
</body>
</html>`

    fs.writeFileSync(previewHtmlFile, previewHtml);
}

module.exports = preview
