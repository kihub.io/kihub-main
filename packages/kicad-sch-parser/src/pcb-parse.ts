import { parse, ParseError, Atom, ListAtom, StringAtom } from './sexpr';
import { Position } from '.';
import { PCB, LayerType, Layer, Net, Segment, Via, Module, FPArc, FPCircle, FPLine, FPPoly, 
         FPText, FPRect, PadShape, PadType, Pad, GRText, GRArc, GRCircle, 
         GRLine, GRPoly, GRRect } from './pcb-types';


const LAYER_VALIDATION = { match: /^[a-zA-Z0-9\._]+$/, maxLength: 20, minLength: 1 }

function parseLayers(el: ListAtom): Layer[] {
    return el.value.slice(1).map(el => {
        if (!(el instanceof ListAtom))
            throw new ParseError('Unexpected value in layers', el.pos);

        return {
            index: el.getNumberByIndex(0, { min: 0, max: 63, integer: true }),
            name: el.getStringByIndex(1, LAYER_VALIDATION),
            type: el.getStringByIndex(2, { match: /^user|mixed|power|signal$/ }) as LayerType 
        }
    });
}

function parseNet(el: ListAtom): Net {
    return {
        index: el.getNumberByIndex(1, { min: 0, integer: true }),
        name: el.getStringByIndex(2, { maxLength: 150 })
    }
}

function parseSegment(el: ListAtom): Segment {
    return {
        start: el.getByKey('start', parsePosition),
        end: el.getByKey('end', parsePosition),
        width: el.getNumberByKey('width', { min: 0, max: 100 }),
        layerName: el.getStringByKey('layer', LAYER_VALIDATION),
        netNumber: el.getNumberByKey('net', { min: 0, integer: true })
    }
}

function parsePosition(el: ListAtom): Position {
    return {
        x: el.getNumberByIndex(1, { min: -1000000, max: 1000000 }),
        y: el.getNumberByIndex(2, { min: -1000000, max: 1000000 })
    }
}

function parseText(gr: true): (el: ListAtom) => GRText;
function parseText(gr: false): (el: ListAtom) => FPText;
function parseText(gr: boolean): (el: ListAtom) => FPText|GRText {
    return (el: ListAtom) => {
        const value = el.getStringByIndex(2, { maxLength: 150 });
        const position = el.getByKey('at',  parsePosition);
        const rotation = el.getByKey('at', el => el.getNumberByIndex(3, { min: -360, max: 360, optional: true })) || 0;
        const layer = el.getStringByKey('layer', LAYER_VALIDATION);
        const visible = !!(el.value.find(v => ((v instanceof StringAtom) && (v.value === 'hide'))));
    
        const fontEffect = el.getByKey('effects', (el) => el.getByKey('font', (el => ({
            size: el.getByKey('size', (el) => [ 
                el.getNumberByIndex(1, { min: 0, max: 10 }), 
                el.getNumberByIndex(2, { min: 0, max: 10 }) 
            ]),
            thickness: el.getNumberByKey('thickness', { min: 0, max: 10 })
        }))));
    
        const r: GRText = { 
            shape: 'text',
            value, position, rotation, layer, visible,
            fontSize: [ fontEffect.size[0], fontEffect.size[1] ],
            thickness: fontEffect.thickness
        };

        if (gr) {
            return r;
        }
        else {
            return { 
                ...r, 
                fieldName: el.getStringByIndex(1, { minLength: 1, maxLength: 150 })
            }
        }
    }
}

function parseCircle(el: ListAtom): FPCircle|GRCircle {
    return {
        shape: 'circle',
        center: el.getByKey('center', parsePosition),
        end: el.getByKey('end', parsePosition),
        layer: el.getStringByKey('layer', LAYER_VALIDATION),
        width: el.getNumberByKey('width', { min: 0, max: 100 })
    }
}

function parseLine(el: ListAtom): FPLine|GRLine {
    return {
        shape: 'line',
        start: el.getByKey('start', parsePosition),
        end: el.getByKey('end', parsePosition),
        layer: el.getStringByKey('layer', LAYER_VALIDATION),
        width: el.getNumberByKey('width', { min: 0 })
    }
}

function parseRect(el: ListAtom): FPRect|GRRect {
    return {
        shape: 'rect',
        start: el.getByKey('start', parsePosition),
        end: el.getByKey('end', parsePosition),
        layer: el.getStringByKey('layer', LAYER_VALIDATION),
        width: el.getNumberByKey('width', { min: 0 })
    }
}

function parseArc(el: ListAtom): FPArc|GRArc {
    return {
        shape: 'arc',
        start: el.getByKey('start', parsePosition),
        end: el.getByKey('end', parsePosition),
        angle: el.getNumberByKey('angle', { min: -360, max: 360 }),
        layer: el.getStringByKey('layer', LAYER_VALIDATION),
        width: el.getNumberByKey('width', { min: 0 })
    }
}

function parsePoly(el: ListAtom): FPPoly|GRPoly {
    return {
        shape: 'poly',
        points: el.getByKey('pts', el => el.getMany('xy', parsePosition)),
        layer: el.getStringByKey('layer', LAYER_VALIDATION),
        width: el.getNumberByKey('width', { min: 0 })
    }
}

function parsePad(el: ListAtom): Pad {
    const padNumber = el.getStringByIndex(1, { maxLength: 30 });
    const padType = el.getStringByIndex(2, { match: /thru_hole|smd|connect|np_thru_hole/ }) as PadType;
    const padShape = el.getStringByIndex(3, { match: /circle|rect|roundrect|oval|trapezoid|custom/ }) as PadShape;
    const position = el.getByKey('at', parsePosition);
    const size = el.getByKey('size', (el) => ({
        x: el.getNumberByIndex(1, { min: 0, max: 100 }),
        y: el.getNumberByIndex(1, { min: 0, max: 100 })
    }));
    const layers = el.getByKey('layers', (el) => 
        el.value.slice(1).map(ly => {
            if (!(ly instanceof StringAtom)) {
                throw new ParseError('Expected to find a layer name', el.pos);
            }
            return ly.value;
        }) // TODO: Parse layer names
    )
    const drill = el.getByKey('drill', (el) => {
        const ovalFlag = el.getStringByIndex(1);
        if (ovalFlag === 'oval') {
            return { 
                x: el.getNumberByIndex(2, { min: 0, max: 30 }),
                y: el.getNumberByIndex(3, { min: 0, max: 30 })
            }
        }
        else {
            // Circular drill
            const r = el.getNumberByIndex(1, { min: 0, max: 30 });
            return { x: r, y: r}
        }
    }, { optional: true });
    const netIndex = el.getByKey('net', (el) => {
        return el.getNumberByIndex(1, { integer: true, min: 0 });
    }, { optional: true });

    return {
        padNumber, padType, padShape, position, size, drill,
        layers, netIndex
    }
}


function parseModule(el: ListAtom): Module {
    const moduleName = el.getStringByIndex(1, { minLength: 1, maxLength: 150 });
    const layer = el.getStringByKey('layer', LAYER_VALIDATION);
    const position = el.getByKey('at', parsePosition);
    const text = el.getMany('fp_text', parseText(false));
    const lines = el.getMany('fp_line', parseLine);
    const arcs = el.getMany('fp_arc', parseArc);
    const circles = el.getMany('fp_circle', parseCircle);
    const rects = el.getMany('fp_rect', parseRect);
    const polys = el.getMany('fp_poly', parsePoly)
    const pads = el.getMany('pad', parsePad);

    return {
        moduleName, layer, position, pads,
        shapes: { ...text, ...lines, ...arcs, ...circles, ...rects, ...polys }
    }
}

function parseVia(el: ListAtom): Via {
    return {
        position: el.getByKey('at', parsePosition),
        size: el.getNumberByKey('size', { min: 0, max: 100 }),
        drill: el.getNumberByKey('drill', { min: 0, max: 100 }),
        layers: el.getByKey('layers', (el) => 
            el.value.slice(1).map(ly => {
                if (!(ly instanceof StringAtom)) {
                    throw new ParseError('Expected to find a layer name', el.pos);
                }
                return ly.value;
            })), // TODO: Parse layer names
        net: el.getNumberByKey('net', { min: 0, integer: true })
    }
}


export function kicadPCBParser(data: string): PCB {

    // Parse S-Expression into nested lists.
    const pcb = parse(data);

    // A PCB file starts with a "kicad_pcb" token, followed by a version
    // and a host description.
    // kicad_pcb (version 20171130) (host pcbnew 5.1.9-73d0e3b20d~88~ubuntu20.04.1)
    if (pcb.getStringByIndex(0) !== 'kicad_pcb')
        throw new ParseError('Not a PCB file', 0);

    const fileVersion = pcb.getStringByKey('version');
    if (!fileVersion) {
        throw new ParseError('Expected file version was not found', 0);
    }

    const layers = pcb.getByKey('layers', parseLayers);
    const nets = pcb.getMany('net', parseNet);
    const segments = pcb.getMany('segment', parseSegment);
    const vias = pcb.getMany('via', parseVia);
    const modules = pcb.getMany('module', parseModule);

    const fpText = pcb.getMany('fp_text', parseText(false));
    const fpCircle = pcb.getMany('fp_circle', parseCircle);
    const fpLine = pcb.getMany('fp_line', parseLine);
    const fpArc = pcb.getMany('fp_arc', parseArc);
    const fpPoly = pcb.getMany('fp_poly', parsePoly);
    const fpRect = pcb.getMany('fp_rect', parseRect);
    
    return {
        fileVersion,
        layers,
        nets,
        segments,
        vias,
        modules,
        shapes: { 
            ...fpText,
            ...fpCircle,
            ...fpLine,
            ...fpArc,
            ...fpPoly,
            ...fpRect
        }
    }

}