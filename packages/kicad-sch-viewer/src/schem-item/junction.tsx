import * as React from 'react';
import { JunctionItem } from '@kihub/kicad-sch-parser';
import { SchematicTheme } from '../theme';

export interface JunctionProps {
    item: JunctionItem;
    theme: SchematicTheme;
}

export function Junction(props: JunctionProps): JSX.Element {

    const { position } = props.item;

    return <circle 
        cx={position.x}
        cy={position.y}
        r={15}
        stroke='none'
        fill={props.theme.junction}
        />

}
