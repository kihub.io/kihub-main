'use strict'

const fsx = require('fs-extra')
const path = require('path')
const fs = require('fs')
const https = require('follow-redirects').https
const unzip = require('unzip')

const CACHE_DIRECTORY = '.kicad_libs'
const LIBRARY_URL = 'https://github.com/KiCad/kicad-library/archive/4.0.7.zip'
const LIST_FILENAME = 'libs.json'

function fetchFile(url, filename)
{
    return new Promise(resolve => {
        console.log(`Fetching from ${url}`)
        https.get(url, res => 
            res
                .pipe(fs.createWriteStream(filename))
                .on('finish', () => { 
                    console.log("Fetch done")
                    resolve()
                 })
            )

    })
}

function fetchKiCadLibraries(cacheDirectory = CACHE_DIRECTORY) {
    return new Promise(resolve => {
        const fileList = []

        // Make sure the destination directory exists
        fsx.ensureDirSync(cacheDirectory)
    
        // Check if a list file is available (meaning that the files have
        // already been downloaded)
        const listFilename = path.resolve(cacheDirectory, LIST_FILENAME)
        if (fs.existsSync(listFilename))
            // Read the file content and return it
            return fs.readFileSync(listFilename)
    
        // Check if the ZIP file has been downloaded
        const zipFilename = path.resolve(cacheDirectory, LIBRARY_URL.substr(LIBRARY_URL.lastIndexOf('/') + 1))

        if (!fs.existsSync(zipFilename)) {
            console.log()
            https.get(LIBRARY_URL, res => res.pipe(fs.createWriteStream(zipFilename)))
        }
        
        (fs.existsSync(zipFilename)? Promise.resolve() : fetchFile(LIBRARY_URL, zipFilename))
            .then(() => {
                const source = fs.createReadStream(zipFilename)
                source.pipe(unzip.Parse())
                .on('entry', entry => {
                    if (entry.type === 'File' && (entry.path.endsWith('.lib') || entry.path.endsWith('.dcm'))) {
                        const baseLibName = path.basename(entry.path)
                        const targetFilename = path.resolve(CACHE_DIRECTORY, baseLibName)
                        entry.pipe(fs.createWriteStream(targetFilename))
        
                        fileList.push(path.resolve(cacheDirectory, baseLibName))
                        console.log(`Found library: ${entry.path}`)
                    }
                    else           
                        entry.autodrain()
                })
                source.on('end', () => { 
                    resolve(fileList); 
                    source.close() 
                })
            })

    })
}

fetchKiCadLibraries().then(fl => console.log(fl))

