import stripCommentLines from './strip-comment-lines';
import { parse, SyntaxError } from './generated/lib-parser';
import { PartLibrary } from './types';

export function kicadLibraryParser(data: string): PartLibrary {

    // Strip comment lines
    const { stripped, translateLineNumber } = stripCommentLines(data);

    try {
        // Parse the library. On success, this will return the parsed
        // data. On failure, it will throw
        return parse(stripped);
    }
    catch(e) {
        // If we got a syntax error, adjust the line number to point
        // on the correct line in the source file (before comment stripping)
        if (e.location) {
            if (e.location.start)
                if (e.location.start.line)
                    e.location.start.line = translateLineNumber(e.location.start.line);

            if (e.location.end)
                if (e.location.end.line)
                    e.location.end.line = translateLineNumber(e.location.end.line);

            throw e;
        }
    }
    
}

export { parse as kicadSchParser } from './generated/sch-parser';
export * from './types';
export { PCB, kicadPCBParser } from './pcb-parse';
