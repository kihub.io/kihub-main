import LikeHelper from 'chai-like';
import { parse as schParse, SyntaxError } from '../src/generated/sch-parser';
import { parse as libParse } from '../src/generated/lib-parser';
import stripCommentLines from '../src/strip-comment-lines';

declare global {
    export namespace Chai {
        interface Assertion {
            schParseAs( schExpected: any ): Assertion;
            schFailParsingWith( schExpected: string ): Assertion;
            schParseWithoutError(): Assertion;
            libParseAs( libExpected: any ): Assertion;
            libFailParsingWith( expectedMessage: string ): Assertion;
            libParseWithoutError(): Assertion;
        }
    }
    
}

export default function ( chai, utils ) {

    chai.use( LikeHelper );

    const Assertion = chai.Assertion;

    Assertion.addMethod( "schParseAs", function ( schExpected: any ) {

        const schText = utils.flag(this, "object");
        const kiSch = schParse( schText );

        new Assertion( kiSch ).like( schExpected );

    } );

    Assertion.addMethod( "schFailParsingWith", function ( expectedMessage: string ) {

        const schText = utils.flag(this, "object");
        let pass = false;

        try {
            schParse( schText );
        }
        catch(e) {
            if ((e instanceof Error) && (e.message === expectedMessage)){
                pass = true;
            }
            else
                this.assert(false,
                    `expected #{this} to throw SyntaxError(${expectedMessage})`,
                    null,
                    null,
                    e.message + 'b'                   
                )
        }

        this.assert(
            pass,
            "expected #{this} to throw but it didn't",
            "expected #{this} to not report an error",
            null,
            null
        );

    } );

    Assertion.addMethod( "schParseWithoutError", function () {

        const schText = utils.flag(this, "object");

        try {

            schParse( schText );

        }
        catch(e) {
            const line = (<SyntaxError>e).location.start.line;
            this.assert(
                false,
                `expected to parse without error, but found error on line ${line} column ${e.location.start.column}`,
                "expected to fail parsing with error",
                null,
                null
            );

        }

    } );

    Assertion.addMethod( "libParseAs", function ( libExpected: any ) {

        const libText = utils.flag(this, "object");
        const kiLib = libParse( libText );

        new Assertion( kiLib ).like( libExpected );

    } );

    Assertion.addMethod( "libFailParsingWith", function ( expectedMessage: string ) {

        const libText = utils.flag(this, "object");
        let pass = false;

        try {
            const kiLib = libParse( libText );
        }
        catch(e) {
            if ((e instanceof Error) && (e.message === expectedMessage)){
                pass = true;
            }
            else
                this.assert(false,
                    `expected #{this} to throw SyntaxError(${expectedMessage})`,
                    null,
                    null,
                    e.message + 'b'                   
                )
        }

        this.assert(
            pass,
            "expected #{this} to throw but it didn't",
            "expected #{this} to not report an error",
            null,
            null
        );

    } );

    Assertion.addMethod( "libParseWithoutError", function () {

        const libText = utils.flag(this, "object");
        const { stripped, translateLineNumber } = stripCommentLines(libText);

        try {

            libParse( stripped );

        }
        catch(e) {
            const line = translateLineNumber((<SyntaxError>e).location.start.line);
            this.assert(
                false,
                `expected #{this} to parse without error, but found error on line ${line} column ${e.location.start.column}`,
                "expected #{this} to fail parsing with error",
                null,
                null
            );

        }

    } );

}

