
import { SchematicTheme } from './theme';
import * as KiColors from './kicad-colors';
import '@kihub/kicad-font/kicad-newstroke.css';

// KiCAD source reference: common/colors.cpp
const kicadTheme: SchematicTheme = {
    wire: KiColors.GREEN,
    bus: KiColors.BLUE,
    junction: KiColors.GREEN,
    localLabel: KiColors.BLACK,
    hierLabel: KiColors.BROWN,
    globalLabel: KiColors.RED,
    pinNumber: KiColors.RED,
    pinName: KiColors.CYAN,
    fields: KiColors.DARKGRAY,
    partReference: KiColors.CYAN,
    partValue: KiColors.CYAN,
    notes: KiColors.LIGHTBLUE,
    foreground: KiColors.RED,
    background: KiColors.LIGHTYELLOW,
    netName: KiColors.DARKGRAY,
    pin: KiColors.RED,
    sheet: KiColors.MAGENTA,
    sheetFileName: KiColors.BROWN,
    sheetName: KiColors.CYAN,
    sheetLabel: KiColors.BROWN,
    noConnect: KiColors.BLUE,
    grid: KiColors.DARKGRAY,
    schBackground: KiColors.WHITE,
    cursor: KiColors.BLACK,
    brightened: KiColors.PUREMAGENTA,
    hidden: KiColors.LIGHTGRAY,
    worksheet: KiColors.RED,
    fontName: 'kicad-newstroke',
    defaultThickness: 6,
    pageBorderColor: 'black'
};

export default kicadTheme;
