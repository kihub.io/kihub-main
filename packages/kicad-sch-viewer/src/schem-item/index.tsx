import * as React from 'react';
import { SchematicItem, ItemType, EntryItem, JunctionItem, LineItem, NoConnectionItem, 
    TextItem, SheetItem, PartDatabase, ComponentItem } from '@kihub/kicad-sch-parser';

import { BusEntry } from './bus-entry';
import { Junction } from './junction';
import { Line } from './line';
import { NoConnection } from './no-connect';
import { Text } from './text';
import { Sheet, HierPath, EnterSheetHandler } from './sheet';
import { PartItem } from './part';
import { SchematicTheme } from 'theme';

export { HierPathElement, HierPath, EnterSheetHandler } from './sheet';

export interface DrawSchematicItemProps {
    item: SchematicItem;
    partDb: PartDatabase;
    theme: SchematicTheme;
    onEnterSheet?: EnterSheetHandler;
    hierPath: HierPath;
}
export function DrawSchematicItem(props: DrawSchematicItemProps): JSX.Element {

    const { item, partDb, theme, onEnterSheet, hierPath } = props;

    switch(item.type) {

        case ItemType.Bitmap:
            return <></>;

        case ItemType.BusEntry:
            return <BusEntry item={item as EntryItem} theme={theme}/>;

        case ItemType.Component:
            // Find the part in the part library
            const compItem = item as ComponentItem;
            return (compItem.partUid && partDb[compItem.partUid])?
                <PartItem item={compItem} part={partDb[compItem.partUid]} theme={theme} hierPath={hierPath} /> :
                // TODO: Unknown part graphics
                <></>;
            
        case ItemType.Junction:
            return <Junction item={item as JunctionItem} theme={theme}/>;

        case ItemType.Line:
            return <Line item={item as LineItem} theme={theme}/>;

        case ItemType.NoConnect:
            return <NoConnection item={item as NoConnectionItem} theme={theme}/>;

        case ItemType.Sheet:
            return <Sheet item={item as SheetItem} theme={theme} hierPath={hierPath} onEnterSheet={onEnterSheet}/>;

        case ItemType.Text:
            return <Text item={item as TextItem} theme={theme}/>;

        default:
            throw Error('Unknown item type');

    }

}