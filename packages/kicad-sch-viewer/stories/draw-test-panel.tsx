import * as React from 'react';
//import { Component, ReactNode } from 'react';
import KiCADTheme from '../src/kicad-theme';

const TOP_LEFT = { x: -1000, y: -1000 };
const BOTTOM_RIGHT = { x: 1000, y: 1000 };

const axisStyle = {
    stroke: KiCADTheme.grid,
    strokeWidth: 1
}

export const DrawTestPanel = (props: { children: React.ReactNode }) => {
    return <svg width='600' height='600' 
        viewBox={`${TOP_LEFT.x} ${TOP_LEFT.y} ${BOTTOM_RIGHT.x-TOP_LEFT.x} ${BOTTOM_RIGHT.y-TOP_LEFT.y}`}>
        <defs>
            <pattern id="grid" width="100" height="100" patternUnits="userSpaceOnUse">
                <rect x="0" y="0" width="5" height="5" stroke="none" fill="gray"/>
            </pattern>
        </defs>

        <rect x={TOP_LEFT.x} y={TOP_LEFT.y} width="100%" height="100%" fill="url(#grid)"/>
        <line x1={TOP_LEFT.x} y1={0} x2={BOTTOM_RIGHT.x} y2={0} style={axisStyle}/>
        <line x1={0} y1={TOP_LEFT.y} x2={0} y2={BOTTOM_RIGHT.y} style={axisStyle}/>
        {props.children}
    </svg>

}
