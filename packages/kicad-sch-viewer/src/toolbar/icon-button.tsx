import * as React from 'react';

export function IconButton(props: { children: JSX.Element[] }) {

    const [ hover, setHover ] = React.useState(false);

    return <div onMouseEnter={() => setHover(true)} onMouseLeave={() => setHover(false)} >props.children</div>;
}