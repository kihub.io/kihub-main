import * as React from 'react';
import {
    DrawPin,
    PinShapeType,
    DrawOrientationType,
    ComponentTransform,
} from '@kihub/kicad-sch-parser';

import { PinText } from './pin-text';
import { SchematicTheme } from 'theme';

// Properties that come from the part (global)
export interface PartProps {
    showName: boolean; 
    showNumber: boolean;
    textOffset: number;
}

export interface PinProps {
    pinProps: DrawPin;
    partProps: PartProps;
    transform: ComponentTransform;
    theme: SchematicTheme;
}

const nullTransform = (dir: DrawOrientationType) => dir;
const mirrorXTransform = (dir: DrawOrientationType) => {
    if (dir === DrawOrientationType.Up) return DrawOrientationType.Down
    else if (dir === DrawOrientationType.Down) return DrawOrientationType.Up
    else return dir;
}

const mirrorYTransform = (dir: DrawOrientationType) => {
    if (dir === DrawOrientationType.Left) return DrawOrientationType.Right
    else if (dir === DrawOrientationType.Right) return DrawOrientationType.Left
    else return dir;
}

const rotateCCWTransform = (dir: DrawOrientationType) => {
    switch(dir) {
        case DrawOrientationType.Up: return DrawOrientationType.Left;
        case DrawOrientationType.Down: return DrawOrientationType.Right;
        case DrawOrientationType.Left: return DrawOrientationType.Down;
        case DrawOrientationType.Right: return DrawOrientationType.Up;
    }
}

const rotateCWTransform = (dir: DrawOrientationType) => {
    switch(dir) {
        case DrawOrientationType.Up: return DrawOrientationType.Right;
        case DrawOrientationType.Down: return DrawOrientationType.Left;
        case DrawOrientationType.Left: return DrawOrientationType.Up;
        case DrawOrientationType.Right: return DrawOrientationType.Down;
    }
}

/*
const transformPos = (p: Position, t: ComponentTransform) => ({
    x: t.x1*p.x + t.y1*p.y,
    y: t.x2*p.x + t.y2*p.y
})
*/

/**
 * Draw a library pin
 * @param pin
 */
export function Pin(props: PinProps): JSX.Element {

    const { name, pinNumber, position, pinLength, orientation, pinNumberSize, pinNameSize,
        shape } = props.pinProps;
    const { showName, showNumber, textOffset } = props.partProps;
    const { transform } = props;

    const x = position.x * transform.x1 + position.y * transform.y1;
    const y = position.x * transform.x2 + position.y * transform.y2;

    // Theses two functions calculate the size
    // of "internal" and "external" pin decorations
    // (such as clock, edge, etc.).
    const externalDecoSize = pinNumberSize;
    const internalDecoSize = pinNameSize ? pinNameSize / 2 : pinNumberSize / 2;

    const line: JSX.Element[] = [];

    // Line + External decor
    ////////////////////////
    if (shape === PinShapeType.Inverted || shape === PinShapeType.ClockInverted) {
        //             **   |
        //           **  ** |
        //  *********      *|
        //           **  ** |
        //             **   |
        line.push(<line x1={x} y1={y} x2={x + pinLength - externalDecoSize} y2={y} />);
        line.push(<circle cx={x + pinLength - externalDecoSize / 2} cy={y} r={externalDecoSize / 2} fill="none" />);
    } else if (shape === PinShapeType.FallingEdge) {
        //             *|
        //           *  |
        //  *********   |
        //           *  |
        //             *|
        const x1 = x - internalDecoSize + pinLength;
        const x2 = x + pinLength;
        line.push(
            <path
                d={`M${x} ${y} L${x1} ${y} L${x2} ${y + internalDecoSize} M${x1} ${y} L${x2} ${y - internalDecoSize}`}
                fill="none"
            />,
        );
    } else if (shape === PinShapeType.InputLow || shape === PinShapeType.ClockLow) {
        //          **   |
        //          * ** |
        //  *************|

        const x1 = x + pinLength - externalDecoSize;
        line.push(
            <path d={`M${x} ${y} L${x + pinLength} ${y} L${x1} ${y - externalDecoSize} L${x1} ${y}`} fill="none" />,
        );
    } else if (shape === PinShapeType.OutputLow) {
        //             **|
        //          **  *|
        //  *************|

        const x1 = x + pinLength - externalDecoSize;
        const y1 = y - externalDecoSize;
        line.push(<path d={`M${x} ${y} L${x + pinLength} ${y} L${x + pinLength} ${y1} L${x1} ${y}`} fill="none" />);
    } else {
        // Draw plain line
        line.push(<line x1={x} y1={y} x2={x + pinLength} y2={y} />);
    }

    // Internal decor
    /////////////////
    if (shape === PinShapeType.Clock || shape === PinShapeType.ClockInverted || shape === PinShapeType.ClockLow) {
        //  |**
        //  |  **
        //  |    *
        //  |  **
        //  |**

        const x1 = x + pinLength;
        line.push(
            <path
                d={`M${x1} ${y - internalDecoSize} L${x1 + internalDecoSize} ${y} L${x1} ${y + internalDecoSize}`}
                fill="none"
            />,
        );
    } else if (shape === PinShapeType.NonLogic) {
        //             *   *
        //              *|*
        //  *************|
        //              *|*
        //             *   *
        const x1 = x + pinLength + externalDecoSize / 2;
        const x2 = x + pinLength - externalDecoSize / 2;
        const y1 = y + externalDecoSize / 2;
        const y2 = y - externalDecoSize / 2;
        line.push(<path d={`M${x1} ${y1} L${x2} ${y2} M${x1} ${y2} L${x2} ${y1}`} fill="none" />);
    }

    // Orientation
    //////////////
    let svgTransform: { transform: string } | null;

    // Apply the transform to the pin orientation
    const transformedOrientation = (transform.x1 === -1 ? mirrorYTransform : nullTransform)(
        (transform.y2 === 1? mirrorXTransform : nullTransform)(
            (transform.y1 === 0? nullTransform :
            transform.y1 === 1? rotateCWTransform : rotateCCWTransform)(orientation)
        )
    );

    switch (transformedOrientation) {
        case DrawOrientationType.Right:
            svgTransform = null;
            break;
        case DrawOrientationType.Left:
            svgTransform = { transform: `rotate(180 ${x} ${y})` };
            break;
        case DrawOrientationType.Up:
            svgTransform = { transform: `rotate(-90 ${x} ${y})` };
            break;
        default:
            // DrawOrientationType.Down
            svgTransform = { transform: `rotate(90 ${x} ${y})` };
    }

    return (<>
            <g {...svgTransform} strokeWidth={6} stroke={props.theme.pin} >{line}</g>
           <PinText 
                pinName={name}
                pinNumber={pinNumber}
                pinNameSize={pinNameSize}
                pinNumberSize={pinNumberSize}
                position={{x, y}}
                pinLength={pinLength}
                orientation={transformedOrientation}
                textInside={textOffset}
                showName={showName}
                showNumber={showNumber}
                theme={props.theme}
       /> 
    </>);

}
