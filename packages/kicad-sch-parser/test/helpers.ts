/// <reference path="./helpers.d.ts"/>

import LikeHelper from 'chai-like';
import { parse, SyntaxError } from '../src/generated/parser';
import { PartLibrary } from '../src/kicad-library-types';
import stripCommentLines from '../src/strip-comment-lines';

export default function ( chai, utils ) {

    chai.use( LikeHelper );

    const Assertion = chai.Assertion;

    Assertion.addMethod( "parseAs", function ( libExpected: PartLibrary ) {

        const libText = utils.flag(this, "object");
        const kiLib = parse( libText );

        new Assertion( kiLib ).like( libExpected );

    } );

    Assertion.addMethod( "failParsingWith", function ( expectedMessage: string ) {

        const libText = utils.flag(this, "object");
        let pass = false;

        try {
            const kiLib = parse( libText );
        }
        catch(e) {
            if ((e instanceof Error) && (e.message === expectedMessage)){
                pass = true;
            }
            else
                this.assert(false,
                    `expected #{this} to throw SyntaxError(${expectedMessage})`,
                    null,
                    null,
                    e.message + 'b'                   
                )
        }

        this.assert(
            pass,
            "expected #{this} to throw but it didn't",
            "expected #{this} to not report an error",
            null,
            null
        );

    } );

    Assertion.addMethod( "parseWithoutError", function () {

        const libText = utils.flag(this, "object");
        const { stripped, translateLineNumber } = stripCommentLines(libText);

        try {

            parse( stripped );

        }
        catch(e) {
            const line = translateLineNumber((<SyntaxError>e).location.start.line);
            this.assert(
                false,
                `expected #{this} to parse without error, but found error on line ${line} column ${e.location.start.column}`,
                "expected #{this} to fail parsing with error",
                null,
                null
            );

        }

    } );

}

