import * as React from 'react';

import { storiesOf } from '@storybook/react';
import KiCADTheme from '../src/kicad-theme';


import { DrawTestPanel } from './draw-test-panel';
import { kicadLibraryParser, Part, DrawPrimitiveType, Convert,
   FillType, HorizontalAlignment, VerticalAlignment, PinShapeType, 
   DrawOrientationType, PinType, DrawArc, FieldOrientation, DrawPrimitive } from '@kihub/kicad-sch-parser';
import { PartDraw } from '../src/part';
import { Pin } from '../src/part/pin';
import testLibSource from './test-lib';

const manyPrimitives = (prim: DrawPrimitive[]): Part => ({
  referenceField: {
    index: 0, 
    value: '', 
    position: {x: 0, y: 0 }, 
    size: 0, 
    orientation: FieldOrientation.Horizontal, 
    visible: false, 
    halign: HorizontalAlignment.Center, 
    valign: VerticalAlignment.Center, 
    bold: true, 
    italic: true
  },
  valueField: {
    index: 0, 
    value: '', 
    position: {x: 0, y: 0 }, 
    size: 0, 
    orientation: FieldOrientation.Horizontal, 
    visible: false, 
    halign: HorizontalAlignment.Center, 
    valign: VerticalAlignment.Center, 
    bold: true, 
    italic: true
  },
  fields: [],
  partUid: '',
  hasConvert: false,
  name: '',
  ref: '',
  offset: 0,
  showPinNumber: false,
  showPinName: false,
  numOfParts: 0,
  locked: true,
  power: true,
  draw: {
    hash: '',
    details: prim
  }
});

const singlePrimitive = (p: DrawPrimitive) => manyPrimitives([ p ]);


const circle = {
  type: DrawPrimitiveType.Circle,
  part: 0,
  convert: Convert.All,
  center: { x: 100, y: -100 },
  radius: 250,
  pen: 1,
  fill: FillType.None
}

const rect = {
    type: DrawPrimitiveType.Rect,
    part: 0,
    convert: Convert.All,
    topLeft: { x: -150, y: -150 },
    bottomRight: { x: 200, y: 250 },
    pen: 15,
    fill: FillType.None
};

const polyline = {
    type: DrawPrimitiveType.Polyline,
    part: 0,
    convert: Convert.All,
    pen: 15,
    fill: FillType.None,
    vertices: [ 
      { x: -300, y: -300 },
      { x: 0, y: 0 },
      { x: 600, y: 200 }
    ]     
};

const text = {
  type: DrawPrimitiveType.Text,
  text: 'ABCdefxX',
  position: { x: 0, y: 0},
  size: 100,
  angle: 0,
  part: 0,
  convert: Convert.All,
  visible: true,
  italic: false,
  bold: false,
  halign: HorizontalAlignment.Center,
  valign: VerticalAlignment.Center
};

const pin = {
  type: DrawPrimitiveType.Pin,
  position: { x: 100, y: -300 }, 
  name: 'PINNAME',
  pinNumber: '1', 
  pinLength: 100,
  orientation: DrawOrientationType.Right,
  pinNumberSize: 50,
  pinNameSize: 50, 
  part: 0,
  convert: Convert.All,
  pinType: PinType.Input, 
  shape: PinShapeType.Line,
  visible: true
};

const drawPartSectionParams = {
  showPinNumber: true,
  showPinName: true,
  convert: true,
  subPart: 0,
  theme: KiCADTheme,
  textOffset: 40
}

storiesOf('Draw Primitives/Circle', module)
    .addDecorator(storyFn => <DrawTestPanel>{storyFn()}</DrawTestPanel>)
    .add('Circle Fg', () => <PartDraw part={singlePrimitive({ ...circle, fill: FillType.Foreground })} {...drawPartSectionParams}/>)
    .add('Circle Bg', () => <PartDraw part={singlePrimitive({ ...circle, fill: FillType.Background })} {...drawPartSectionParams}/>)
    .add('Circle Fg', () => <PartDraw part={singlePrimitive({ ...circle })} {...drawPartSectionParams}/>)
storiesOf('Draw Primitives/Rect', module)
  .addDecorator(storyFn => <DrawTestPanel>{storyFn()}</DrawTestPanel>)
  .add('Rect Fg', () => <PartDraw part={singlePrimitive({ ...rect, fill: FillType.Foreground })} {...drawPartSectionParams}/>)
  .add('Rect Bg', () => <PartDraw part={singlePrimitive({ ...rect, fill: FillType.Background })} {...drawPartSectionParams}/>)
  .add('Rect None', () => <PartDraw part={singlePrimitive({ ...rect })} {...drawPartSectionParams}/>)

  storiesOf('Draw Primitives/Polyline', module)
    .addDecorator(storyFn => <DrawTestPanel>{storyFn()}</DrawTestPanel>)
    .add('Polyline Fg', () => <PartDraw part={singlePrimitive({ ...polyline, fill: FillType.Foreground })} {...drawPartSectionParams}/>)
    .add('Polyline Bg', () => <PartDraw part={singlePrimitive({ ...polyline, fill: FillType.Background })} {...drawPartSectionParams}/>)
    .add('Polyline None', () => <PartDraw part={singlePrimitive({ ...polyline })} {...drawPartSectionParams}/>)

  const textStories = storiesOf('Draw Primitives/Text', module)
    .addDecorator(storyFn => <DrawTestPanel>{storyFn()}</DrawTestPanel>)

  for (const halign of [HorizontalAlignment.Center, HorizontalAlignment.Left, HorizontalAlignment.Right])
    for (const valign of [VerticalAlignment.Bottom, VerticalAlignment.Center, VerticalAlignment. Top]) {
      textStories.add(`${halign}/${valign}`, () => <PartDraw {...drawPartSectionParams} 
        part={singlePrimitive({ ...text, halign, valign })}/>)
    }
  textStories.add('Angle', () => <PartDraw {...drawPartSectionParams} part={manyPrimitives([
      { ...text, angle: 0 },
      { ...text, angle: 450, position: { x: 200, y: 0 }},
      { ...text, angle: 900, position: { x: 0, y: 200 }}
    ])}/>);

  textStories.add('Size', () => <PartDraw {...drawPartSectionParams} part={manyPrimitives([
    { ...text, size: 50, valign: VerticalAlignment.Bottom },
    { ...text, size: 100, position: { x: 0, y: 200 }, valign: VerticalAlignment.Bottom },
    { ...text, size: 200, position: { x: 0, y: -200 }, valign: VerticalAlignment.Bottom }
  ])}/>);

  textStories.add('Bold', () => <PartDraw {...drawPartSectionParams} part={singlePrimitive({ ...text, bold: true })}/>);
  textStories.add('Italic', () => <PartDraw {...drawPartSectionParams} part={singlePrimitive({ ...text, italic: true })}/>);
  textStories.add('Overline', () => <PartDraw {...drawPartSectionParams} part={singlePrimitive({ ...text, text: '~NEGATED' })}/>);


  const pinStories = storiesOf('Draw Primitives/Pin', module)
    .addDecorator(storyFn => <DrawTestPanel>{storyFn()}</DrawTestPanel>);

  for (const pinShape in PinShapeType) {
    pinStories.add(`Shape: ${pinShape}`, 
    () => <PartDraw 
    {...drawPartSectionParams} 
        part={singlePrimitive({ ...pin, shape: PinShapeType[pinShape] })}/>)
  }
  pinStories
    .add('Orientation', () => <PartDraw {...drawPartSectionParams} part={manyPrimitives([
      { ...pin, name: 'RIGHT', shape: PinShapeType.Inverted },
      { ...pin, name: 'DOWN', shape: PinShapeType.Inverted, orientation: DrawOrientationType.Down },
      { ...pin, name: 'LEFT', shape: PinShapeType.Inverted, orientation: DrawOrientationType.Left },
      { ...pin, name: 'UP', shape: PinShapeType.Inverted, orientation: DrawOrientationType.Up }
    ])}/>);

  const pin2 = {
    type: DrawPrimitiveType.Pin,
    position: { x: 100, y: 100 }, 
    name: 'PINNAMEX',
    pinNumber: '17', 
    pinLength: 100,
    orientation: DrawOrientationType.Right,
    pinNumberSize: 50,
    pinNameSize: 50, 
    part: 0,
    convert: Convert.All,
    pinType: PinType.Input, 
    shape: PinShapeType.Line,
    visible: true    
  }
  const pinPartPropsInside = {
    showName: true,
    showNumber: true,
    textOffset: 20
  }
  const pinPartPropsOutside = { ...pinPartPropsInside, textOffset: 0 };
  const transform = { x1: 1, y1: 0, x2: 0, y2: -1 }

  pinStories.add('Text Inside, Right', () => <Pin pinProps={{...pin2, orientation: DrawOrientationType.Right}} partProps={pinPartPropsInside} transform={transform} theme={KiCADTheme}/>)
  pinStories.add('Text Inside, Left', () => <Pin pinProps={{...pin2, orientation: DrawOrientationType.Left}} partProps={pinPartPropsInside} transform={transform} theme={KiCADTheme}/>)
  pinStories.add('Text Inside, Up', () => <Pin pinProps={{...pin2, orientation: DrawOrientationType.Up}} partProps={pinPartPropsInside} transform={transform} theme={KiCADTheme}/>)
  pinStories.add('Text Inside, Down', () => <Pin pinProps={{...pin2, orientation: DrawOrientationType.Down}} partProps={pinPartPropsInside} transform={transform} theme={KiCADTheme}/>)
  pinStories.add('Text Outside, Right', () => <Pin pinProps={{...pin2, orientation: DrawOrientationType.Right}} partProps={pinPartPropsOutside} transform={transform} theme={KiCADTheme}/>)
  pinStories.add('Text Outside, Left', () => <Pin pinProps={{...pin2, orientation: DrawOrientationType.Left}} partProps={pinPartPropsOutside} transform={transform} theme={KiCADTheme}/>)
  pinStories.add('Text Outside, Up', () => <Pin pinProps={{...pin2, orientation: DrawOrientationType.Up}} partProps={pinPartPropsOutside} transform={transform} theme={KiCADTheme}/>)
  pinStories.add('Text Outside, Down', () => <Pin pinProps={{...pin2, orientation: DrawOrientationType.Down}} partProps={pinPartPropsOutside} transform={transform} theme={KiCADTheme}/>)

    const testArcs = [
      'A 25 -24 126 1014 109 0 1 0 N 0 100 150 0',
      'A 100 -125 160 514 -1288 0 1 0 N 200 0 0 -250',
      'A 0 150 292 311 -1491 0 1 0 N 250 300 -250 0',
      'A 0 0 354 -451 -1349 0 1 0 N 250 -251 -250 -251',
      'A 0 0 354 1349 451 0 1 0 N -250 251 250 251',
      'A -250 -100 364 741 -159 0 1 0 N -150 250 100 -200',
      'A -275 -25 302 656 -244 0 1 0 N -150 250 0 -150'
    ];

    const parseArc = (arcLine: string): DrawArc => {
      const [ /* A */, centerX, centerY, radius, startAngle, 
        endAngle, /* part */, /* convert */, /* pen */,
        /* fill */, startX, startY, endX, endY ] = arcLine.split(' ');

        return {
          type: DrawPrimitiveType.Arc,
          center: { x: parseInt(centerX), y: parseInt(centerY) },
          radius: parseInt(radius),
          startAngle: parseInt(startAngle),
          endAngle: parseInt(endAngle),
          pen: 0, fill: FillType.None,
          start: { x: parseInt(startX), y: parseInt(startY) },
          end: { x: parseInt(endX), y: parseInt(endY) },
          part: 0, convert: Convert.All
        }
    }

  const arcStories = storiesOf('Draw Primitives/Arc', module)
    .addDecorator(storyFn => <DrawTestPanel>{storyFn()}</DrawTestPanel>)

  testArcs.forEach((testArc, testArcIndex) => {
    arcStories.add(`Arc ${testArcIndex}`,
      () => <PartDraw {...drawPartSectionParams} part={singlePrimitive(parseArc(testArc))}/>);
  });

arcStories
  .add('Arc Fg', () => <PartDraw {...drawPartSectionParams} part={singlePrimitive(
      {
        ...parseArc('A 25 -24 126 1014 109 0 1 0 N 0 100 150 0'),
        fill: FillType.Foreground
      }
    )}/>)
    .add('Arc Bg', () => <PartDraw {...drawPartSectionParams} part={singlePrimitive(
      {
        ...parseArc('A 25 -24 126 1014 109 0 1 0 N 0 100 150 0'),
        fill: FillType.Background
      }
    )}/>);


/////////////////////////////////
// Test Library
/////////////////////////////////

const lib = kicadLibraryParser(testLibSource);
const testLib = storiesOf('Test Library', module)
  .addDecorator(storyFn => <DrawTestPanel>{storyFn()}</DrawTestPanel>);

  
for (const part of lib.parts) {
  if (part.numOfParts > 1) {
    for(let i = 1; i < part.numOfParts; i++)
      testLib.add(`${part.name}_${i}`, () => <PartDraw part={part} subPart={i} convert={false} theme={KiCADTheme}/>);
  }
  else {
    testLib.add(`${part.name}`, () => <PartDraw part={part} subPart={1} convert={false} theme={KiCADTheme}/>);
  }

  if (part.hasConvert) {
    testLib.add(`${part.name}_Convert`, () => <PartDraw part={part} subPart={1} convert={true} theme={KiCADTheme}/>)
  }
}
