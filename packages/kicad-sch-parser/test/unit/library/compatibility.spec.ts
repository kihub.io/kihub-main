import { basename } from 'path';

import * as chai from 'chai';
import { readFile } from 'fs-extra';

import kiAssertions from '../../assertions';
import { schemLibs } from '@kihub/test-files';

chai.use(kiAssertions);

const { expect } = chai;

describe("Compatibility with existing libs", function() {

    schemLibs.forEach(libFile => {

        const displayName = basename(libFile);

        it(`Library ${displayName} parses`, async function() {

            const rawData = await readFile(libFile);
            expect(rawData.toString()).to.libParseWithoutError();

        });

    });

});

