
import * as chai from 'chai';
import kiAssertions from '../../assertions';
import { FieldOrientation, HorizontalAlignment, VerticalAlignment, DrawOrientationType, 
    DrawPrimitiveType, FillType, Convert, PinShapeType, PinType } from '../../../src/types';

chai.use(kiAssertions);

const { expect } = chai;

describe('Parse KiCAD library', function() {

    it('Version Line', function() {

        const lib = 'EESchema-LIBRARY Version 3.4\n';

        expect(lib).libParseAs({
            version: {
                major: 3,
                minor: 4
            },
            parts: []
        })
    })

    it('Minimal Part', function() {

        const lib = [
            'EESchema-LIBRARY Version 3.4',
            'DEF PART1 U 0 20 Y Y 1 F N',
            'F0 "U" -10 10 20 H V R CNN',
            'F1 "ABC" -100 100 200 H V R CNN',
            'DRAW',
            'ENDDRAW',
            'ENDDEF'
        ].join('\n') + '\n';

        expect(lib).to.libParseAs({
            version: {
                major: 3,
                minor: 4
            },
            parts: [
                {
                    name: 'PART1',
                    ref: 'U',
                    offset: 20,
                    showPinNumber: true,
                    showPinName: true,
                    numOfParts: 1,
                    locked: false,
                    power: false,
                    partUid: '4968b36c2bd693d2ab10a16ba0b158d9ffe39418665dae8f5d16ead0f0ebb0b7'
                }
            ]
        })

    });

    it('Two Parts', function() {

        const lib = [
            'EESchema-LIBRARY Version 3.4',
            'DEF PART1 U 0 20 Y Y 1 F N',
            'F0 "U" -10 10 20 H V R CNN',
            'F1 "ABC" -100 100 200 H V R CNN',
            'DRAW',
            'ENDDRAW',
            'ENDDEF',
            'DEF PART2 X 0 30 Y Y 1 F N',
            'F0 "U" -10 10 20 H V R CNN',
            'F1 "ABC" -100 100 200 H V R CNN',
            'DRAW',
            'ENDDRAW',
            'ENDDEF'
        ].join('\n') + '\n';

        expect(lib).to.libParseAs({
            version: {
                major: 3,
                minor: 4
            },
            parts: [
                {
                    name: 'PART1',
                    ref: 'U',
                    offset: 20,
                    showPinNumber: true,
                    showPinName: true,
                    numOfParts: 1,
                    locked: false,
                    power: false
                },
                {
                    name: 'PART2',
                    ref: 'X',
                    offset: 30,
                    showPinNumber: true,
                    showPinName: true,
                    numOfParts: 1,
                    locked: false,
                    power: false
                }

            ]
        })

    });

    it('DEF Line', function() {

        const libWithDEF = defLine => [
            'EESchema-LIBRARY Version 3.4',
            defLine,
            'F0 "U" -10 10 20 H V R CNN',
            'F1 "ABC" -100 100 200 H V R CNN',
            'DRAW',
            'ENDDRAW',
            'ENDDEF',
            ''
        ].join('\n')

        expect(libWithDEF('DEF PART1 U 0 20 Y Y 1 F N')).to.libParseAs({
            parts: [
                {
                    name: 'PART1',
                    ref: 'U',
                    offset: 20,
                    showPinNumber: true,
                    showPinName: true,
                    numOfParts: 1,
                    locked: false,
                    power: false
                }
            ]
        })  

        expect(libWithDEF('DEF PART2 Q 0 30 N N 5 L P')).to.libParseAs({
            parts: [
                {
                    name: 'PART2',
                    ref: 'Q',
                    offset: 30,
                    showPinNumber: false,
                    showPinName: false,
                    numOfParts: 5,
                    locked: true,
                    power: true
                }
            ]
        })  

    });

    describe('Fields', function() {

        const libWithFields = fields => [
            'EESchema-LIBRARY Version 3.4',
            'DEF PART1 U 0 20 Y Y 1 F N',
            ...fields,
            'DRAW',
            'ENDDRAW',
            'ENDDEF',
            ''
        ].join('\n');

        it('Two mandatory only', function() {
            // Two mandatory fields onlt
            expect(libWithFields([
                'F0 "U" -10 10 20 H V R CNN',
                'F1 "ABC" -100 100 200 V I L TIB'
            ])).to.libParseAs({
                parts: [
                    {
                        referenceField: {
                            index: 0,
                            value: 'U',
                            position: { x: -10, y: 10 },
                            size: 20,
                            orientation: FieldOrientation.Horizontal,
                            visible: true,
                            halign: HorizontalAlignment.Right,
                            valign: VerticalAlignment.Center,
                            italic: false,
                            bold: false
                        },
                        valueField: {
                            index: 1,
                            value: 'ABC',
                            position: { x: -100, y: 100 },
                            size: 200,
                            orientation: FieldOrientation.Vertical,
                            visible: false,
                            halign: HorizontalAlignment.Left,
                            valign: VerticalAlignment.Top,
                            italic: true,
                            bold: true
                        }
                    }
                ]
            });
        })

        it('Two mandatory + Footprint', function() {
            expect(libWithFields([
                'F0 "U" -10 10 20 H V R CNN',
                'F1 "ABC" -10 10 20 H V R CNN',
                'F2 "FP" -15 15 30 H V R CNN'
            ])).to.libParseAs({
                parts: [
                    {
                        referenceField: {
                            value: 'U'
                        },
                        valueField: {
                            value: 'ABC'
                        },
                        footprintField: {
                                index: 2,
                                value: "FP"
                        }
                    }
                ]
            });
        });

        it('Two mandatory + Doc', function() {
            expect(libWithFields([
                'F0 "U" -10 10 20 H V R CNN',
                'F1 "ABC" -10 10 20 H V R CNN',
                'F3 "http://xx.tt/abc?a=20&b=30" -15 15 30 H V R CNN'
            ])).to.libParseAs({
                parts: [
                    {
                        referenceField: {
                            value: 'U'
                        },
                        valueField: {
                            value: 'ABC'
                        },
                        documentField: {
                            value: 'http://xx.tt/abc?a=20&b=30'
                        }
                    }
                ]
            });
        });

        it('All std fields', function() {
            expect(libWithFields([
                'F0 "U" -10 10 20 H V R CNN',
                'F1 "NE555" -10 10 20 H V R CNN',
                'F2 "SOIC-8" 0 0 0 H V R CNN',
                'F3 "http://www.ti.com/lit/ds/symlink/ne555.pdf" -15 15 30 H V R CNN'
            ])).to.libParseAs({
                parts: [
                    {
                        referenceField: {
                            value: 'U'
                        },
                        valueField: {
                            value: 'NE555'
                        },
                        footprintField: {
                            value: "SOIC-8"
                        },
                        documentField: {
                            value: "http://www.ti.com/lit/ds/symlink/ne555.pdf"
                        }
                    }
                ]
            });
        });

        it('User fields', function() {
            expect(libWithFields([
                'F0 "REF" -10 10 20 H V R CNN',
                'F1 "VAL" -10 10 20 H V R CNN',
                'F4 "VALUE1" 0 0 0 H V R CNN "USER1"',
                'F5 "VALUE2" -15 15 30 H V R CNN "USER2"'
            ])).to.libParseAs({
                parts: [
                    {
                        fields: [
                            {
                                index: 4,
                                value: "VALUE1",
                                name: "USER1"
                            },
                            {
                                index: 5,
                                value: "VALUE2",
                                name: "USER2"
                            }
                        ]
                    }
                ]
            });
        });

        xit('Duplicate field numbers', function() {
            expect(libWithFields([
                'F0 "REF" -10 10 20 H V R CNN',
                'F1 "VAL" -10 10 20 H V R CNN',
                'F4 "VALUE1" 0 0 0 H V R CNN "USER1"',
                'F4 "VALUE2" 0 0 0 H V R CNN "USER2"'
            ])).to.libFailParsingWith("Duplicate field numbers are not allowed");
        });

        it('Missing refernce field', function() {
            expect(libWithFields([
                'F1 "VAL" -10 10 20 H V R CNN',
                'F4 "VALUE1" 0 0 0 H V R CNN "USER1"',
                'F4 "VALUE2" 0 0 0 H V R CNN "USER2"'
            ])).to.libFailParsingWith("Missing reference field in component");
        });

        it('Missing value field', function() {
            expect(libWithFields([
                'F0 "REF" -10 10 20 H V R CNN',
                'F4 "VALUE1" 0 0 0 H V R CNN "USER1"',
                'F4 "VALUE2" 0 0 0 H V R CNN "USER2"'
            ])).to.libFailParsingWith("Missing value field in component");
        });

        it('Invalid field index', function() {
            expect(libWithFields([
                'F0 "REF" -10 10 20 H V R CNN',
                'F1 "VAL" -10 10 20 H V R CNN',
                'F4 "VALUE1" 0 0 0 H V R CNN "USER1"',
                'F110 "VALUE2" 0 0 0 H V R CNN "USER2"'
            ])).to.libFailParsingWith("Illegal field index");
        });

    });

    describe("Alias List", function() {

        it("Basic", function() {
            expect([
                'EESchema-LIBRARY Version 3.4',
                'DEF PART1 U 0 20 Y Y 1 F N',
                'F0 "U" -10 10 20 H V R CNN',
                'F1 "ABC" -100 100 200 H V R CNN',
                'ALIAS ABC D13F 1~2',
                'DRAW',
                'ENDDRAW',
                'ENDDEF',
                ''
            ].join('\n'))
            .to.libParseAs({
                parts: [
                    {
                        aliases: ['ABC', 'D13F', '1~2']
                    }
                ]
            });    
        });

        it("After FPLIST", function() {
            expect([
                'EESchema-LIBRARY Version 3.4',
                'DEF PART1 U 0 20 Y Y 1 F N',
                'F0 "U" -10 10 20 H V R CNN',
                'F1 "ABC" -100 100 200 H V R CNN',
                '$FPLIST',
                '$ENDFPLIST',
                'ALIAS ABC D13F 1~2',
                'DRAW',
                'ENDDRAW',
                'ENDDEF',
                ''
            ].join('\n'))
            .to.libParseAs({
                parts: [
                    {
                        aliases: ['ABC', 'D13F', '1~2']
                    }
                ]
            });    
        });

        it("Only one allowed", function() {
            expect([
                'EESchema-LIBRARY Version 3.4',
                'DEF PART1 U 0 20 Y Y 1 F N',
                'F0 "U" -10 10 20 H V R CNN',
                'F1 "ABC" -100 100 200 H V R CNN',
                'ALIAS A',
                '$FPLIST',
                '$ENDFPLIST',
                'ALIAS B',
                'DRAW',
                'ENDDRAW',
                'ENDDEF',
                ''
            ].join('\n'))
            .to.libFailParsingWith("Only one ALIAS section is allowed");
        });

    });

    it("FPLIST", function() {
        expect([
            'EESchema-LIBRARY Version 3.4',
            'DEF PART1 U 0 20 Y Y 1 F N',
            'F0 "U" -10 10 20 H V R CNN',
            'F1 "ABC" -100 100 200 H V R CNN',
            '$FPLIST',
            'ENTRYA',
            'ENTRYB',
            '$ENDFPLIST',
            'DRAW',
            'ENDDRAW',
            'ENDDEF',
            ''
        ].join('\n'))
        .to.libParseAs({
            parts: [
                {
                    fpFilter: ['ENTRYA', 'ENTRYB']
                }
            ]
        })

    });

    describe('DRAW Section', function() {

        const libWithDraw = draw => [
            'EESchema-LIBRARY Version 3.4',
            'DEF PART1 U 0 20 Y Y 1 F N',
            'F0 "U" -10 10 20 H V R CNN',
            'F1 "ABC" -100 100 200 H V R CNN',
            'DRAW',
            draw,
            'ENDDRAW',
            'ENDDEF',
            ''
        ].join('\n');

        const partWithDrawPrimitive = pmt => ({
            parts: [{
                draw: {
                    details: [ pmt ]
                }
            }]
        });


        describe('Arc', function() {

            it('Minimal', function() {
                expect(libWithDraw('A 10 20 30 0 900 1 2 15'))
                .to.libParseAs(partWithDrawPrimitive({
                    type: DrawPrimitiveType.Arc,
                    center: { x: 10, y: 20 },
                    radius: 30,
                    startAngle: 0,
                    endAngle: 900,
                    part: 1,
                    convert: Convert.Convert,
                    pen: 15,
                    fill: FillType.None,
                    start: { x:40, y: 20 },
                    end: { x:10, y: 50 }
                }));
            });
    
            it('With Fill', function() {
                expect(libWithDraw('A 10 20 30 0 900 1 2 15 f'))
                .to.libParseAs(partWithDrawPrimitive({
                    type: DrawPrimitiveType.Arc,
                    fill: FillType.Background
                }));
    
                expect(libWithDraw('A 10 20 30 0 900 1 2 15 F'))
                .to.libParseAs(partWithDrawPrimitive({
                    type: DrawPrimitiveType.Arc,
                    fill: FillType.Foreground
                }));
    
                expect(libWithDraw('A 10 20 30 0 900 1 2 15 N'))
                .to.libParseAs(partWithDrawPrimitive({
                    type: DrawPrimitiveType.Arc,
                    fill: FillType.None
                }));
    
            });
    
            it('With Start and Stop', function() {
                expect(libWithDraw('A 10 20 30 0 900 1 2 15 f 100 200 300 500'))
                .to.libParseAs(partWithDrawPrimitive({
                    type: DrawPrimitiveType.Arc,
                    fill: FillType.Background,
                    start: { x: 100, y: 200 },
                    end: { x: 300, y: 500 }
                }));
    
            });
    
        });


        it('Circle', function() {
            expect(libWithDraw('C 100 200 300 1 2 15 N'))
            .to.libParseAs(partWithDrawPrimitive({
                type: DrawPrimitiveType.Circle,
                center: { x: 100, y: 200 },
                radius: 300,
                part: 1,
                convert: Convert.Convert,
                pen: 15,
                fill: FillType.None
            }));
        });

        describe('Polyline', function() {
            it('Basic', function() {
                expect(libWithDraw('P 3 1 0 15 10 20 30 40 50 60'))
                .to.libParseAs(partWithDrawPrimitive({
                    type: DrawPrimitiveType.Polyline,
                    part: 1,
                    convert: Convert.All,
                    pen: 15,
                    vertices: [
                        { x: 10, y: 20 },
                        { x: 30, y: 40 },
                        { x: 50, y: 60 }
                    ],
                    fill: FillType.None
                }));
            });   

            it('With fill', function() {
                expect(libWithDraw('P 1 1 1 15 10 20 f'))
                .to.libParseAs(partWithDrawPrimitive({
                    type: DrawPrimitiveType.Polyline,
                    part: 1,
                    convert: Convert.Regular,
                    pen: 15,
                    vertices: [
                        { x: 10, y: 20 },
                    ],
                    fill: FillType.Background
                }));

                expect(libWithDraw('P 1 1 2 15 10 20 F'))
                .to.libParseAs(partWithDrawPrimitive({
                    type: DrawPrimitiveType.Polyline,
                    part: 1,
                    convert: Convert.Convert,
                    pen: 15,
                    vertices: [
                        { x: 10, y: 20 },
                    ],
                    fill: FillType.Foreground
                }));
            });   

            it('Wrong vertex count', function() {
                expect(libWithDraw('P 2 1 0 15 10 20 30 40 50 60'))
                .to.libParseAs(partWithDrawPrimitive({
                    type: DrawPrimitiveType.Polyline,
                    vertices: [
                        { x: 10, y: 20 },
                        { x: 30, y: 40 },
                    ],
                }));

            });

        });

        describe('Rect', function() {

            it("No fill", function() {
                expect(libWithDraw('S 10 20 30 40 1 0 15'))
                .to.libParseAs(partWithDrawPrimitive({
                    type: DrawPrimitiveType.Rect,
                    topLeft: { x: 10, y: 20 },
                    bottomRight: { x: 30, y: 40 },
                    part: 1,
                    convert: Convert.All,
                    pen: 15
                }));
            });

            it("With fill", function() {
                expect(libWithDraw('S 10 20 30 40 1 0 15 f'))
                .to.libParseAs(partWithDrawPrimitive({
                    type: DrawPrimitiveType.Rect,
                    topLeft: { x: 10, y: 20 },
                    bottomRight: { x: 30, y: 40 },
                    part: 1,
                    convert: Convert.All,
                    pen: 15,
                    fill: FillType.Background
                }));

            });

        });

        describe('Text', function() {
            it('No modifiers', function() {
                expect(libWithDraw('T 10 20 30 40 0 1 0 abc'))
                .to.libParseAs(partWithDrawPrimitive({
                   type: DrawPrimitiveType.Text,
                    angle: 10,
                    position: { x: 20, y: 30 },
                    size: 40,
                    visible: true,
                    part: 1,
                    convert: Convert.All,
                    text: "abc",
                    italic: false,
                    bold: false,
                    halign: HorizontalAlignment.Left,
                    valign: VerticalAlignment.Center
                }))
            });

            it('Italic and Bold modifiers', function() {
                expect(libWithDraw('T 10 20 30 40 0 1 0 abc Normal 0'))
                .to.libParseAs(partWithDrawPrimitive({
                   type: DrawPrimitiveType.Text,
                    angle: 10,
                    position: { x: 20, y: 30 },
                    size: 40,
                    visible: true,
                    part: 1,
                    convert: Convert.All,
                    text: "abc",
                    italic: false,
                    bold: false,
                    halign: HorizontalAlignment.Left,
                    valign: VerticalAlignment.Center
                }));

                expect(libWithDraw('T 10 20 30 40 0 1 0 abc Italic 1'))
                .to.libParseAs(partWithDrawPrimitive({
                   type: DrawPrimitiveType.Text,
                    angle: 10,
                    position: { x: 20, y: 30 },
                    size: 40,
                    visible: true,
                    part: 1,
                    convert: Convert.All,
                    text: 'abc',
                    italic: true,
                    bold: true,
                    halign: HorizontalAlignment.Left,
                    valign: VerticalAlignment.Center
                }));

            });

            it('All modifiers', function() {
                expect(libWithDraw('T 10 20 30 40 0 1 0 abc Normal 0 C T'))
                .to.libParseAs(partWithDrawPrimitive({
                   type: DrawPrimitiveType.Text,
                    angle: 10,
                    position: { x: 20, y: 30 },
                    size: 40,
                    visible: true,
                    part: 1,
                    convert: Convert.All,
                    text: 'abc',
                    italic: false,
                    bold: false,
                    halign: HorizontalAlignment.Center,
                    valign: VerticalAlignment.Top
                }));
            });

            it('Escaped Text', function() {
                expect(libWithDraw('T 10 20 30 40 0 1 0 abc~d"ef'))
                .to.libParseAs(partWithDrawPrimitive({
                   type: DrawPrimitiveType.Text,
                    text: 'abc d"ef',
                }));
            });

            it('Quoted Text', function() {
                expect(libWithDraw('T 10 20 30 40 0 1 0 "abc~ def"'))
                .to.libParseAs(partWithDrawPrimitive({
                   type: DrawPrimitiveType.Text,
                    text: 'abc~ def',
                }));
            });

        });

        describe('Pin', function() {

            it("No shape specified", function() {
                expect(libWithDraw('X abc a1 10 20 30 U 40 50 0 0 I'))
                .to.libParseAs(partWithDrawPrimitive({
                   type: DrawPrimitiveType.Pin,
                   name: 'abc',
                   pinNumber: 'a1',
                   position: { x: 10, y: 20 },
                   pinLength: 30,
                   orientation: DrawOrientationType.Up,
                   pinNumberSize: 40,
                   pinNameSize: 50,
                   part: 0,
                   convert: Convert.All,
                   shape: PinShapeType.Line,
                   visible: true,
                   pinType: PinType.Input
                }));
            });

            it("Invisible Line", function() {
                expect(libWithDraw('X abc a1 10 20 30 U 40 50 0 0 I N'))
                .to.libParseAs(partWithDrawPrimitive({
                   type: DrawPrimitiveType.Pin,
                   shape: PinShapeType.Line,
                   visible: false
                }));
            });

            const pinShapes = { 
                'I': PinShapeType.Inverted,
                'C': PinShapeType.Clock,
                'CI': PinShapeType.ClockInverted,
                'L': PinShapeType.InputLow,
                'CL': PinShapeType.ClockLow,
                'V': PinShapeType.OutputLow,
                'F': PinShapeType.FallingEdge,
                'X': PinShapeType.NonLogic
            };
            for (const shapeLetter in pinShapes) {

                const pinShape = pinShapes[shapeLetter];
                it(`Shape ${shapeLetter}`, function() {
                    expect(libWithDraw(`X abc a1 10 20 30 U 40 50 0 0 I ${shapeLetter}`))
                    .to.libParseAs(partWithDrawPrimitive({
                       type: DrawPrimitiveType.Pin,
                       shape: pinShape,
                       visible: true
                    }));
                    expect(libWithDraw(`X abc a1 10 20 30 U 40 50 0 0 I N${shapeLetter}`))
                    .to.libParseAs(partWithDrawPrimitive({
                       type: DrawPrimitiveType.Pin,
                       shape: pinShape,
                       visible: false
                    }));
    
                });

            }

            const pinTypes = {
                'I': PinType.Input,
                'O': PinType.Output,
                'B': PinType.Bidir,
                'T': PinType.Tristate,
                'P': PinType.Passive,
                'U': PinType.Unspecified,
                'W': PinType.PowerInput,
                'w': PinType.PowerOutput,
                'C': PinType.OpenCollector,
                'E': PinType.OpenEmitter,
                'N': PinType.NotConnected
            }

            for (const typeLetter in pinTypes) {

                const pinType = pinTypes[typeLetter];

                it(`Type ${typeLetter}`, function() {
                    expect(libWithDraw(`X abc a1 10 20 30 U 40 50 0 0 ${typeLetter}`))
                    .to.libParseAs(partWithDrawPrimitive({
                       type: DrawPrimitiveType.Pin,
                       pinType: pinType,
                       visible: true
                    }));
                });

            }

        });



    });
    


})