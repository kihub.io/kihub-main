import * as React from 'react';
import { Position, HorizontalAlignment, VerticalAlignment } from '@kihub/kicad-sch-parser';

const DEFAULT_HORIZONTAL_ALIGNMENT = 'middle';
const DEFAULT_VERTICAL_ALIGNMENT = 'middle';

function horizAlign(ka?: HorizontalAlignment): string {
    if (!ka) return DEFAULT_HORIZONTAL_ALIGNMENT;

    switch (ka) {
        case HorizontalAlignment.Center:
            return 'middle';
        case HorizontalAlignment.Left:
            return 'start';
        case HorizontalAlignment.Right:
            return 'end';
        default:
            throw Error('Invalid horizontal alignment');
    }
}

function vertAlign(va?: VerticalAlignment): string {
    if (!va) return DEFAULT_VERTICAL_ALIGNMENT;

    switch (va) {
        case VerticalAlignment.Bottom:
            return 'baseline';
        case VerticalAlignment.Center:
            return 'middle';
        case VerticalAlignment.Top:
            return 'hanging';
        default:
            throw Error('Invalid vertical alignment');
    }
}

export interface TextProps {
    text: string;
    position: Position;
    size: number;
    bold?: boolean;
    italic?: boolean;
    valign?: VerticalAlignment;
    halign?: HorizontalAlignment;
    angle?: number;
    fontName: string;
    color: string;
}

export function Text(props: TextProps): JSX.Element {
    const { text, position, size, bold, italic, valign, halign, angle, fontName, color } = props;
    const decoration = text.startsWith('~') ? 'overline' : 'none';

    return (
        <text
            x={position.x}
            y={position.y}
            fontSize={size*1.5}
            fontWeight={bold ? 'bold' : 'normal'}
            fontStyle={italic ? 'italic' : 'normal'}
            dominantBaseline={vertAlign(valign)}
            textAnchor={horizAlign(halign)}
            textDecoration={decoration}
            transform={`rotate(${(angle || 0) / 10} ${position.x} ${position.y})`}
            style={{
                fontFamily: fontName,
                stroke: color,
                fill: color,
                strokeWidth: 0,
            }}
        >
            {text.startsWith('~') ? text.substring(1) : text}
        </text>
    );
}
