import { expect } from 'chai';
import { parseRefDes, parseLibraryItem, calcPageSize } from '../../../src/helpers';

describe('parseLibraryItem', function() {

    it('Old format version', function() {

        const nameOnly = name => ({
            name,
            libNickname: null,
            revision: null
        });

        expect(parseLibraryItem('abc', 2)).to.deep.equal(nameOnly('abc'));
        expect(parseLibraryItem('abc:def', 2)).to.deep.equal(nameOnly('abc:def'));
        expect(parseLibraryItem('abc:def/2', 2)).to.deep.equal(nameOnly('abc:def/2'));
    });

    it('Only item name', function() {
        expect(parseLibraryItem('abc', 3)).to.deep.equal({
            name: 'abc',
            libNickname: null,
            revision: null
        });
    });

    it('Item name and library nick', function() {
        expect(parseLibraryItem('abc:def', 4)).to.deep.equal({
            name: 'def',
            libNickname: 'abc',
            revision: null
        });
    });

    it('Item name, library nick, revision', function() {
        expect(parseLibraryItem('abc:def/a10', 4)).to.deep.equal({
            name: 'def',
            libNickname: 'abc',
            revision: 'a10'
        });

    });

    it('Replace illegal chars', function() {
        expect(parseLibraryItem('abc:de:f', 4)).to.deep.equal({
            name: 'de_f',
            libNickname: 'abc',
            revision: null
        });

        expect(parseLibraryItem('ab~cd', 4)).to.deep.equal({
            name: 'ab_cd',
            libNickname: null,
            revision: null
        });


    })


});


describe('parseRefDes', function() {

    it('Remove numeric suffix', function() {

        expect(parseRefDes('AB123')).to.equal('AB');

    });

    it('Remove question mark suffix', function() {

        expect(parseRefDes('J?')).to.equal('J');

    });

    it('No change if no suffix', function() {

        expect(parseRefDes('Q')).to.equal('Q');

    });

    it('Replace spaces', function() {

        expect(parseRefDes('Z~?')).to.equal('Z ');

    })

});

describe("calcPageSize", function() {

    it("Predefined size - mil, portrait", function() {

        expect(calcPageSize("A", 100, 200, true)).to.deep.equal({
            pageSize: "A",
            width: 8500,
            height: 11000,
            portrait: true
        });
    });

    it("Predefined size - mm, landscape", function() {

        expect(calcPageSize("A4", 100, 200, false)).to.deep.equal({
            pageSize: "A4",
            width: 8268,
            height: 11693,
            portrait: false
        });

    });

    it("User defined size", function() {

        expect(calcPageSize("User", 100, 200, false)).to.deep.equal({
            pageSize: "User",
            width: 100,
            height: 200
        });

    });

    it("Invalid page size", function() {

        expect(() => calcPageSize("Q", 100, 200, true)).to.throw();

    });

})